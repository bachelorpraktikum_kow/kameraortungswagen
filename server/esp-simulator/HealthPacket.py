"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

import Packet
import random
class HealthPacket(Packet.Packet):
    def __init__(self, esps = [("Thomas", "10.8.0.2"), ("Charles", "192.168.2.12"), ("Toby", "192.168.178.137")]) -> None:
        self.esps = esps
        super().__init__()

    def compress(self, packet) -> dict:
        """
        Compresses a packet to its minified state.
        :param packet: Human-Readable packet as a dict
        :returns: Minified/Compressed packet as a dict
        """
        translated = dict()
        translated[0] = packet["ip"]
        translated[1] = packet["name"]
        translated[2] = packet["type"]

        wifi = dict()
        wifi[0] = packet["wifi"]["ssid"]
        translated[3] = wifi

        config = dict()
        config[0] = packet["config"]["readInterval"]
        config[1] = packet["config"]["flashlight"]
        config[2] = packet["config"]["camera"]
        config[3] = packet["config"]["cameraQuality"]
        config[4] = packet["config"]["cameraFps"]

        translated[4] = config
        
        return translated

    def random(self) -> dict:
        """
        The random method generates a random health packet with the following structure:

        ip: IP address of the selected ESP.
        name: Name of the selected ESP.
        type: Type of packet, set to "health".
        wifi: Dictionary containing the following wifi configuration:
        - ssid: SSID of the currently connected wifi network
        pins: Dictionary containing the following pin configuration:
        - steering: A random integer between 0 and 10.
        - hall: A random integer between 0 and 10.
        - tie: A random integer between 0 and 10.
        - odometer: A random integer between 0 and 10.
        config: Dictionary containing the following pin configuration:
        - readInterval: A random integer between 1000 and 5000.
        

        :returns: a random data packet as a dict
        """
        selected_esp = random.choice(self.esps)

        #quality_options = ["UXGA", "SXGA", "XGA", "SVGA", "VGA", "CIF", "QQVGA"]
        quality_options = ["XGA", "SVGA", "VGA", "CIF", "QQVGA"]
        # select a random quality option
        quality = random.choice(quality_options)

        fps_options = [1,5,10]
        # select a random fps option
        fps = random.choice(fps_options)

        return {
                    "ip": selected_esp[1],
                    "name": selected_esp[0],
                    "type": "health",
                    "wifi": {
                        "ssid": "FreeWifi"
                    },
                    "config": {
                        "readInterval": random.randint(1000, 5000),
                        "flashlight": random.randint(0, 10),
                        "camera": random.randint(0, 1),
                        "cameraQuality": quality,
                        "cameraFps": fps,
                    }
                }