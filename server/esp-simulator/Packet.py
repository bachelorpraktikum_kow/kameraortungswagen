"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

import xxhash, json, random
class Packet():
    packet = dict()
    def __init__(self) -> None:
        """
        Initializes a random packet and compresses it
        """
        packet = self.random()
        self._packet = self.compress(packet)
        
    def get(self) -> str:
        """
        Returns the randomly generated packet as a json string
        :returns: str in json of the packet (minified)
        """
        jsonString = json.dumps(self._packet, separators=(',', ':'))
        knownHash = xxhash.xxh32_hexdigest(jsonString)
        return knownHash + ":" + jsonString

    def compress(self, packet) -> dict:
        raise NotImplementedError("Must override compress")

    def random(self, esps = [("Thomas", "10.8.0.2"), ("Charles", "192.168.2.12"), ("Toby", "192.168.178.137")]) -> dict:
        raise NotImplementedError("Must override random")

