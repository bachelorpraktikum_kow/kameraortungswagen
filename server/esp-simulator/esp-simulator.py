"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

import socket, random, time
from DataPacket import DataPacket
from HealthPacket import HealthPacket

# config
UDP_IP = "127.0.0.1"
UDP_PORT = 4242

SIMULATION_SPEED = 0 # in seconds
MAX_DELAY = 3 # in seconds
ESPs = [("Thomas", "10.8.0.2")]
LIMIT_OUTPUT = False

while True:
    packet = None
    if random.randint(0, 100) <= 80:
        packet = DataPacket(ESPs)
    else:
        packet = HealthPacket(ESPs)
    packet = packet.get().encode("utf-8")
    
    # delay sending (for simulating time difference)
    time.sleep(random.randint(0, MAX_DELAY))

    # send to socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    number_bytes_sent = sock.sendto(packet, (UDP_IP, UDP_PORT))

    if LIMIT_OUTPUT:
        print("[+] Sent packet " + packet.decode()[0:50] + "...")
    else:
        print("[+] Sent packet " + packet.decode())

    # TODO: send image to server
    
    
    time.sleep(SIMULATION_SPEED) 



