"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

import time, random
import Packet
class DataPacket(Packet.Packet):
    static_last_odometer = 0
    static_last_tie = 0
    static_last_speed = 0

    def __init__(self, esps = [("Thomas", "10.8.0.2"), ("Charles", "192.168.2.12"), ("Toby", "192.168.178.137")]) -> None:
        self.esps = esps
        super().__init__()

    def compress(self, packet) -> dict:
        """
        Compresses a packet to its minified state.
        :param packet: Human-Readable packet as a dict
        :returns: Minified/Compressed packet as a dict
        """
        translated = dict()
        translated[0] = packet["ip"]
        translated[1] = packet["name"]
        translated[2] = packet["type"]

        sensors = dict()
        sensors[0] = packet["sensors"]["steering"]
        sensors[1] = packet["sensors"]["hall"]
        sensors[2] = packet["sensors"]["tie"]
        sensors[3] = packet["sensors"]["tieCount"]
        sensors[4] = packet["sensors"]["odometer"]
        sensors[5] = packet["sensors"]["odometerCount"]
        sensors[6] = packet["sensors"]["wifi"]
        sensors[7] = packet["sensors"]["rtc"]
        sensors[8] = packet["sensors"]["battery"]
        translated[3] = sensors

        return translated

    def random(self) -> dict:
        """
        The random method generates a random data packet with the following structure:

        ip: IP address of the selected ESP.
        name: Name of the selected ESP.
        type: Type of packet, set to "data".
        sensors: Dictionary containing the following sensor data:
        - steering: A random integer between 0 and 90, representing the steering angle in degrees.
        - hall: A random integer between 0 and 1, representing whether the hall sensor is active or not.
        - tie: A random float between 0 and 170, representing the tie sensor value in millimeters per second.
        - tieCount: A integer, representing the number of tie sensor hits.
        - odometer: A random float between 0 and 170, representing the odometer sensor value in millimeters per second.
        - odometerCount: A random integer, representing the number of odometer sensor hits.
        - wifi: A random float between -100 and 0, representing the WiFi signal strength in decibels.
        - rtc: Timestamp of when the packet was generated, in UNIX format (time in milli-seconds since the UNIX epoch).

        :returns: a random data packet as a dict
        """
        selected_esp = random.choice(self.esps)

        # generate realistic speeds (only works if using one board)
        odometer_speed = random.uniform(DataPacket.static_last_speed - 10, DataPacket.static_last_speed + 30)
        if odometer_speed < 0: odometer_speed = 15
        if odometer_speed > 170: odometer_speed = random.uniform(150, 170)
        DataPacket.static_last_speed = odometer_speed
        
        tie_speed = random.uniform(odometer_speed - (0.1 * odometer_speed), odometer_speed + (0.1 * odometer_speed)) 
        if tie_speed < 0: tie_speed = 0
        if tie_speed > 170: tie_speed = 170

        # set hall sensor 1 if static_last_odometer is >=80 and by random 80%, then reset static_last_odometer
        hall = "0"
        if DataPacket.static_last_odometer >= 80 and random.randint(0, 100) > 80:
            DataPacket.static_last_odometer = 0
            DataPacket.static_last_tie = 0
            hall = "1"
        else:
            DataPacket.static_last_odometer += random.randint(1, 10)
            DataPacket.static_last_tie += random.randint(1, 10)


        return {
                    "ip": selected_esp[1],
                    "name": selected_esp[0],
                    "type": "data",
                    "sensors": {
                        "steering": random.randint(1400, 2100), # in mV (1500-2000)
                        "hall": hall, # hall active or not
                        "tie": round(tie_speed, 2) if random.uniform(0, 1) <= 0.95 else 0, # mm/s
                        "tieCount": DataPacket.static_last_tie, # count of tie hits
                        "odometer": round(odometer_speed, 2) if random.uniform(0, 1) <= 0.95 else 0, # mm/s
                        "odometerCount": DataPacket.static_last_odometer, # count of odometer hits
                        "wifi": -1 * round(random.random() * 100, 2), # wifi strength in db
                        "rtc": int(time.time() * 1000), # timestamp
                        "battery": random.randint(3590, 4200) # battery voltage in mV
                    }
                }