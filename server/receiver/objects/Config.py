"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

class Config:

    def __init__(self, board_id, wifi, config) -> None:
        """
        Initialize the Config class with given parameters

        :param board_id: the id of the board
        :type board_id: int
        :param wifi: wifi information
        :type wifi: dict
        :param config: config information
        :type config: dict
        """
        self._board_id = board_id
        self._wifi = wifi
        self._config = config

    def save(self, db):
        """
        Saves the configuration to the database
        :param db: sqlite3.Cursor The database cursor to use for the operation
        """
        configuration_id = self._id(db)
        if configuration_id < 0:
            # Configuration does not exists
            self._add(db)
            return

        self._configuration_id = configuration_id
        self._update(db)

    def _add(self, db):
        """
        Add a new configuration to the database.
        :param db: database connection object
        """
        db.execute("""
        INSERT INTO configuration (
        configuration_wifi_ssid,
        configuration_read_interval,
        configuration_flashlight,
        configuration_camera,
        configuration_camera_quality,
        configuration_camera_fps,
        board_id) VALUES (?, ?, ?, ?, ?, ?, ?)""", (
            self._wifi["ssid"], 
            self._config["readInterval"],
            self._config["flashlight"],
            self._config["camera"],
            self._config["cameraQuality"],
            self._config["cameraFps"],
            self._board_id))

        self._configuration_id = self._id(db)

    def _update(self, db):
        """
        Update the configuration in the database
        :param db: database object
        """
        db.execute("""
        UPDATE configuration SET 
        configuration_wifi_ssid = ?, 
        configuration_read_interval = ?,
        configuration_flashlight = ?,
        configuration_camera = ?,
        configuration_camera_quality = ?,
        configuration_camera_fps = ?
        WHERE board_id = ?""", (
            self._wifi["ssid"], 
            self._config["readInterval"], 
            self._config["flashlight"],
            self._config["camera"],
            self._config["cameraQuality"],
            self._config["cameraFps"],
            self._board_id))

    def _id(self, db):
        """
        Retrieve the ID of the configuration object from the database.
        :param db: The database object to interact with.
        :returns: The ID of the configuration object, or -1 if it does not exist.
        """
        current_configuration = db.query("SELECT configuration_id FROM configuration WHERE board_id = ?", (self._board_id,))

        if len(current_configuration) == 0:
            return -1
        return current_configuration[0][0]