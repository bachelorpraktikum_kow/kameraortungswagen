"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

from packets.Packet import Packet
from packets.DataPacket import DataPacket
from packets.HealthPacket import HealthPacket
import json

class PacketFactory:
    @staticmethod
    def createPacket(data) -> Packet:
        """
        Given a data packet, the method will split the packet into the integrity hash and the raw packet data.
        It then decodes the packet and checks its type, creating the appropriate DataPacket or HealthPacket object if it is a valid packet.
        
        :return: Packet or None
        """

        data_split = data.decode().split(":", 1)
        # decode packet
        integrity = data_split[0]
        packet_raw = data_split[1]

        packet = {}
        try:
            packet = json.loads(packet_raw)
        except:
            return None
        
        # check type of packet
        if not "2" in packet.keys():
            return None
        packet_type = packet["2"]
         # if data packet, handle data packet
        if packet_type == "data":
            print("[+] Data packet received, analyzing...")
            return DataPacket(packet, packet_raw, integrity)        
        # if health packet, handle health packet
        elif packet_type == "health":
            print("[+] Health packet received, analyzing...")
            return HealthPacket(packet, packet_raw, integrity)
        else:
            print("[-] Invalid packet type received, ignoring...")
            return None
