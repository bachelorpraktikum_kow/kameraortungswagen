"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

import time
from packets.Packet import Packet

class DataPacket(Packet):
    """
    This class DataPacket is a subclass of Packet and represents a data packet sent by the ESP.
    """

    def __init__(self, packet, packet_raw, integrity) -> None:
        """
        Initalizes a DataPacket

        :param packet: Packet sent by the ESP
        :param packet_raw: Raw packet data (still json)
        :param integrity: Integrity prepended to the packet (xxhash32)
        """
        super().__init__(packet, packet_raw, integrity)

    def translate(self, packet) -> dict:
        """
        Translates the packet data into a human-readable dictionary

        :param packet: packet data
        :return: dictionary containing translated packet data
        """
        translated = dict()
        translated["ip"] = packet["0"]
        translated["name"] = packet["1"]
        translated["type"] = packet["2"]

        sensors = dict()
        sensors["steering"] = packet["3"]["0"]
        sensors["hall"] = packet["3"]["1"]
        sensors["tie"] = packet["3"]["2"]
        sensors["tieCount"] = packet["3"]["3"]
        sensors["odometer"] = packet["3"]["4"]
        sensors["odometerCount"] = packet["3"]["5"]
        sensors["wifi"] = packet["3"]["6"]
        sensors["rtc"] = packet["3"]["7"]
        sensors["battery"] = packet["3"]["8"]
        translated["sensors"] = sensors

        return translated


    # write data to database
    def save(self, db) -> bool:
        """
        Save the data from the packet to the database
        :param db: Database connection
        :return: whether the operation was successful or not
        """

        # get board id from database
        board_id = self.board(db)
        
        # ignore packet if board is not in database
        if board_id < 0:
            return False

        # prepare sql statement
        params = (board_id, self.packet["sensors"]["steering"], self.packet["sensors"]["hall"], self.packet["sensors"]["tie"], self.packet["sensors"]["tieCount"], self.packet["sensors"]["odometer"], self.packet["sensors"]["odometerCount"], self.packet["sensors"]["wifi"], int(self.packet["sensors"]["rtc"]), self.packet["sensors"]["battery"], int(time.time() * 1000))

        # execute statement on database
        result = db.execute("INSERT INTO `sensor_data` (board_id, sensor_data_steering, sensor_data_hall, sensor_data_tie, sensor_data_tie_count, sensor_data_odometer, sensor_data_odometer_count, sensor_data_wifistrength, sensor_data_rtc, sensor_data_battery, sensor_data_local_rtc) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", params)

        return True