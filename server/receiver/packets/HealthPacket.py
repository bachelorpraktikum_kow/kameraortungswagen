"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

from packets.Packet import Packet
from objects.Config import Config

class HealthPacket(Packet):
    """
    This class HealthPacket is a subclass of Packet and represents a health packet sent by the ESP.
    """

    def __init__(self, packet, packet_raw, integrity) -> None:
        """
        Initalizes a HealthPacket

        :param:packet Packet sent by the ESP
        :param:packet_raw Raw packet data (still json)
        :param:integrity Integrity prepended to the packet (xxhash32)
        """
        super().__init__(packet, packet_raw, integrity)

    def translate(self, packet) -> dict:
        """
        Translates the packet data into a human-readable dictionary

        :param packet: packet data
        :return: dictionary containing translated packet data
        """

        translated = dict()
        translated["ip"] = packet["0"]
        translated["name"] = packet["1"]
        translated["type"] = packet["2"]

        wifi = dict()
        wifi["ssid"] = packet["3"]["0"]
        translated["wifi"] = wifi

        config = dict()
        config["readInterval"] = packet["4"]["0"]
        config["flashlight"] = packet["4"]["1"]
        config["camera"] = packet["4"]["2"]
        config["cameraQuality"] = packet["4"]["3"]
        config["cameraFps"] = packet["4"]["4"]
        translated["config"] = config
        
        return translated

    def save(self, db) -> bool:
        """
        Save the data from the packet to the database
        :param db: Database connection
        :return: whether the operation was successful or not
        """

        # write health to database

        # get board id from database
        board_id = self.board(db)

        # add board if board info in health packet not in database
        if board_id < 0:
            db.execute("INSERT INTO board (board_ip, board_name) VALUES (?, ?)", (self.packet["ip"], self.packet["name"]))
            board_id = self.board(db)

        # add config & pins or update config & pins
        config = Config(board_id, self.packet["wifi"], self.packet["config"])
        config.save(db)