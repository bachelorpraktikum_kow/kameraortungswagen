"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""
import xxhash

class Packet:
    """
    The Packet class is the base class for handling and validating packets that are received from the connected boards.
    It contains methods for validating the integrity of the packet, finding the associated board in the database, and translating the raw packet data into a more usable format.
    
    Attributes:
    packet (dict): A dictionary containing the translated packet data.
    packet_raw (str): A string containing the raw packet data.
    integrity (str): A string containing the integrity hash of the packet.
    """

    packet = dict()
    def __init__(self, packet, packet_raw, integrity) -> None:
        """
        Initializes the packet class with the given packet, raw packet and integrity hash

        :param packet: the packet in its parsed form
        :param packet_raw: the raw packet as it was received
        :param integrity: the hash of the packet_raw
        """
        self.packet = self.translate(packet)
        self.packet_raw = packet_raw
        self.integrity = integrity

    def validate(self) -> bool:
        """
        Validates the integrity of the packet by checking the hash of the raw packet
        :return: True if the packet is valid, else False
        """
        knownHash = xxhash.xxh32_hexdigest(self.packet_raw)
        if knownHash == self.integrity:
            return True
        return False

    def board(self, db) -> int:
        """
        Get the id of the board that sent the packet
        :param db: the database connection
        :return: the id of the board if found, else -1
        """
        # get board_id from database
        row = db.query("SELECT board_id FROM board WHERE board_name = ? AND board_ip = ?", (self.packet["name"], self.packet["ip"]))

        # return board_id or -1 if not exists
        if len(row) == 0:
            return -1

        board_id = row[0][0]
        return board_id


    def translate(self, packet) -> dict:
        """
        Translates the packet data into a human-readable dictionary

        Note: Needs to be implemented in sub-classes

        :param packet: Raw Packet as dict to be translated in human-readable format
        :return: dictionary containing translated packet data
        """
        pass

    def save(self, db) -> bool:
        """
        Save the data from the packet to the database

        Note: Needs to be implemented in sub-classes

        :param db: Database connection
        :return: whether the operation was successful or not
        """
        pass