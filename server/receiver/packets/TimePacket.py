import json
import xxhash
import socket
import time

class TimePacket:

    @staticmethod
    def send(ip, esp_name):
        # build configuration packet
        configuration_packet = {
            "name": esp_name,
            "type": "configuration",
            "config": {
                "time": str(int(time.time() * 1000))
            }
        }

        encoded_configuration_packet = json.dumps(configuration_packet)
        integrity = xxhash.xxh32_hexdigest(encoded_configuration_packet.encode('utf-8'))
        packet = integrity + ":" + encoded_configuration_packet

        # send packet to esp
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(packet.encode("utf-8"), (ip, 4242))