"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

import socket
import packets.PacketFactory as PacketFactory
from packets.TimePacket import TimePacket
import database.SQLiteDatabase as SQLiteDatabase

UDP_IP = "0.0.0.0"
UDP_PORT = 4242

print("[+] Starting packet receiver on port " + str(UDP_PORT) + "...")

# create udp socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# bind socket to UDP_IP and UDP_PORT
sock.bind((UDP_IP, UDP_PORT))

print("[+] Receiver started! Waiting for packets...")

# connect to database
db = SQLiteDatabase.SQLiteDatabase()

print("[+] Connected to database!")

# wait and receive packets
while True:
    # receive 8192 bytes from socket (large buffer size to handle different sized packets)
    data, addr = sock.recvfrom(8192)
    
    # TODO: Check addr of sender to validate esp against data (security)

    try:
        # get packet as object
        packet = PacketFactory.PacketFactory.createPacket(data)
        if packet == None:
            print("[-] Error: Packet is not of any known type!")
            continue

        # validate packet
        if not packet.validate():
            print("[-] Error: Integrity of packet could not be validated!")
            continue

        # check if packet time is way below current time
        if "sensors" in packet.packet.keys() and packet.packet["sensors"]["rtc"] < 10000000000:
            # send correcting packet
            print("[#] Info: Packet time is way below current time, sending correcting packet!")
            TimePacket.send(addr[0], packet.packet["name"])
    
        # save packet to database
        packet.save(db)
    except Exception as e:
        print("[-] Error: Unexpected error occured while processing packet! Message: " + str(e))
        continue
    

   
