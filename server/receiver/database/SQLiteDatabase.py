"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

import sqlite3, sys, os
class SQLiteDatabase:

    def __init__(self) -> None:
        """
        Creates a connection to a SQLite database by establishing a connection to the database using the path to the database file.
        In case of an sqlite3.Error exception, it prints out the error message and exits the program.
        """
        try:
            db_path = os.path.dirname(os.path.realpath(__file__)) + "/../../database/database.db"
            self._connection = sqlite3.connect(db_path)
            self._cursor = self._connection.cursor()
        except sqlite3.Error as error:
            print("Error while connecting to sqlite: ", error)
            sys.exit(-1)

    def execute(self, query, params) -> sqlite3.Cursor:
        """
        Executes a query on a SQLite database, and commits the changes.

        :param query: string that contains the parameterlized sql query
        :param params: tuple/list of parameters to be passed in the query
        :returns: the cursor object of the executed query, that allows to fetch the results, if there's any.
        """
        result = self._cursor.execute(query, params)        
        self._connection.commit()
        return result

    def query(self, query, params) -> sqlite3.Cursor:
        """
        Executes a query on a SQLite database, and retrieve the results as a list of named tuples.

        :param query: string that contains the parameterlized sql query
        :param params: tuple/list of parameters to be passed in the query
        :returns: It returns the rows as a list of named tuples.
        """
        self._cursor.execute(query, params)
        rows = self._cursor.fetchall()
        return rows
