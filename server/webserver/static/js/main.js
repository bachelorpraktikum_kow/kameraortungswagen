/**
* This file is part of the project "Kameraortungswagen" licensed under GPLv3.
* It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
* On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
*/


/**
 * Synonym for getElementById
 * @param {string}  id  ID of the html object
 */
function $(id) {
    return document.getElementById(id);
}

/**
 * This function is an asynchronous function that makes a request to a specified API endpoint and calls a callback function with the decoded json of the response when it is received.
 * 
 * It takes two arguments, 'api' and 'func', where 'api' is a string that specifies the API endpoint to be requested, and 'func' is a callback function that will be called when the response is received.
 * If the request failed, the function returns false.
 * 
 * @param {string} api API-url excluding /api/ (example: boards, data/1/1)
 * @param {function} func Callback-function after successfull request
 * @returns true if request was successfull, false if not
 */
async function request(api, func) {
    const response = await fetch("/api/" + api);
    if (!response.ok) {
        return false;
    }
    const json = await response.json();
    func(json);
    return true;
}

/**
 * This function is an asynchronous function that makes a request to a specified API endpoint and calls a callback function with the decoded json of the response when it is received.
 * 
 * It takes two arguments, 'api' and 'func', where 'api' is a string that specifies the API endpoint to be requested, and 'func' is a callback function that will be called when the response is received.
 * If the request failed, the function returns false.
 * 
 * @param {string} api API-url excluding /api/ (example: boards, data/1/1)
 * @param {array} data Data to be JSON encoded and sent to the server
 * @param {function} func Callback-function after successfull request
 * @returns true if request was successfull, false if not
 */
async function post(api, data, func) {
  const response = await fetch("/api/" + api, {method: 'POST',  mode: 'cors', cache: 'no-cache', credentials: 'same-origin', headers: {'Content-Type': 'application/json', 'X-CSRFToken': CSRF}, body: JSON.stringify(data)});
  if (!response.ok) {
      return false;
  }
  const json = await response.json();
  func(json);
  return true;
}


/**
 * This function formats a given JavaScript Date object into a string in the format of "HH:MM:SS, DD.MM.YYYY" and returns it.
 * It takes one argument, 'datetime' which is a JavaScript Date object.
 * 
 * @param {DataTime} datetime DateTime-object to be formatted
 * @returns Formatted String of the DateTime-object in the format "HH:MM:SS, DD.MM.YYYY"
 */

function formattime(datetime){
    var year = datetime.getFullYear();
    var month = datetime.getMonth() + 1;
    if (month < 10) 
        month = "0" + month;

    var date = datetime.getDate();
    if (date < 10) 
        date = "0" + date;
    var hour = datetime.getHours();
    if (hour < 10) 
        hour = "0" + hour;
    var min = datetime.getMinutes();
    if (min < 10) 
        min = "0" + min;
    var sec = datetime.getSeconds();
    if (sec < 10) 
        sec = "0" + sec;
    var time =  date + '.' + month + '.' + year + ", " +  hour + ':' + min + ':' + sec;
    return time;
}

/**
 * This function calculates the time elapsed since a given date and returns it in a human-readable format.
 * It takes one argument, 'date', which is a JavaScript Date object representing the starting point of the elapsed time.
 * 
 * @param {DateTime} date DateTime-object to be converted to "... ago"
 * @returns Human-readable format of the DateTime-object in the style of "... ago"
*/
function timeago(date) {
    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = seconds / 31536000;
    if (interval > 1)
      return Math.floor(interval) + " year(s) ago";
    interval = seconds / 2592000;
    if (interval > 1) 
      return Math.floor(interval) + " month(s) ago";
    
    interval = seconds / 86400;
    if (interval > 1) 
      return Math.floor(interval) + " day(s) ago";
    
    interval = seconds / 3600;
    if (interval > 1) 
      return Math.floor(interval) + " hour(s) ago";
    
    interval = seconds / 60;
    if (interval > 1) 
      return Math.floor(interval) + " minute(s) ago";
    
    return Math.floor(seconds) + " second(s) ago";
}

/**
 * Maps a number ranging from inMin to inMax, to a number in the range outMin to outMax
 * 
 * @param {float} number Number to be mapped
 * @param {float} inMin Original min range of number
 * @param {float} inMax Original max range of number
 * @param {float} outMin Mapped min range of number
 * @param {float} outMax Mapped max range of number
 * @returns Mapped number in the range outMin to outMax
 */
function mapRange(number, inMin, inMax, outMin, outMax) {
  return (number - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}
