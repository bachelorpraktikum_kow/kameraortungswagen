/**
* This file is part of the project "Kameraortungswagen" licensed under GPLv3.
* It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
* On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
*/

let modal_firmware_update = $("modal-firmware-update");
let modal_firmware_update_btn = $("btn-firmware-update");
let modal_close = document.getElementsByClassName("modal-close")[0];

modal_firmware_update_btn.onclick = function () {
  modal_firmware_update.style.display = "block";
}
modal_close.onclick = function () {
  modal_firmware_update.style.display = "none";
}
window.onclick = function (event) {
  if (event.target == modal_firmware_update) {
    modal_firmware_update.style.display = "none";
  }
}
