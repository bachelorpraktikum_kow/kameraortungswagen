/**
 * This function makes periodic API requests to retrieve new boards and updates the UI with the received data.
 * It makes a request to the API endpoint of "boards/".
 * It schedules another call to this function every second.
 */
function BoardsLoop() {

    request("boards/", function (array) {
        if (array.length == 0) return;
        if (tour_running) return;

        let list = document.getElementById("list");
        list.innerHTML = "";

        for (let index = 0; index < array.length; index++) {
            const element = array[index];
        
            if (element["updated"] > 0) {
                element["updated"] = formattime(new Date(element["updated"]));
            } else {
                element["updated"] = "Never";
            }

            let board = document.createElement("div");
            board.classList.add("list-entry");
            board.classList.add("board");

            board.dataset.id = element["id"];

            // set tour data
            let tour_name, tour_ip, tour_updated, tour_active = "";
            if (index == 0) {
                tour_name = " data-tour='board-element-name' ";
                tour_ip = " data-tour='board-element-ip' ";
                tour_updated = " data-tour='board-element-updated' ";
                tour_active = " data-tour='board-element-active' ";
                board.dataset.tour = 'board-element';
            }

            board.innerHTML = 
            '<div class="board-entry-first" ' + tour_name  + '><div class="board-entry-first-icon">' +
                '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M15 9H9v6h6V9zm-2 4h-2v-2h2v2zm8-2V9h-2V7c0-1.1-.9-2-2-2h-2V3h-2v2h-2V3H9v2H7c-1.1 0-2 .9-2 2v2H3v2h2v2H3v2h2v2c0 1.1.9 2 2 2h2v2h2v-2h2v2h2v-2h2c1.1 0 2-.9 2-2v-2h2v-2h-2v-2h2zm-4 6H7V7h10v10z"/></svg></div>' +
                '<div class="board-entry-first-name">' +
                    '<span class="board-entry-content">' + escapeHTML(element["name"]) + '</span>' +
                    '<span class="board-entry-desc">Name</span>' +
                '</div>' +
            '</div>' +
            '<div class="board-entry board-entry-ip" ' + tour_ip  + '>' +
                '<span class="board-entry-content">' + escapeHTML(element["ip"]) + '</span>' +
                '<span class="board-entry-desc">IP</span>' +
            '</div>' +
            '<div class="board-entry board-entry-updated" ' + tour_updated  + '>' +
                '<span class="board-entry-content">' + escapeHTML(element["updated"]) + '</span>' +
                '<span class="board-entry-desc">Last update</span>' +
            '</div>' +
            '<div class="board-entry board-entry-status" ' + tour_active  + '>' +
                '<span class="board-entry-content dot ' + (element["active"] == '1' ? "green" : "red") + '"></span>' +
                '<span class="board-entry-desc">' + (element["active"] == '1' ? "Online" : "Offline") + '</span>' +
            '</div>' +
            '<div class="board-entry-arrow">' +
                '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16.01 11H4v2h12.01v3L20 12l-3.99-4v3z"/></svg>' +
            '</div>';

            board.addEventListener("click", function (board) {
                document.location = "/dashboard/" + board.currentTarget.dataset.id;
            })

            list.appendChild(board);
        }

    });

    setTimeout(() => {
        BoardsLoop();
    }, 1000)
}

/**
 * Escapes a string html-safe
 * @param {string} unsafeText 
 * @returns html-escaped text
 */
function escapeHTML(unsafeText) {
    let div = document.createElement('div');
    div.innerText = unsafeText;
    return div.innerHTML;
}


BoardsLoop();