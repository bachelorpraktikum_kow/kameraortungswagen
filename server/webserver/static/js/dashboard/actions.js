/**
* This file is part of the project "Kameraortungswagen" licensed under GPLv3.
* It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
* On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
*/


/**
 * This function is called when the user clicks on the "Export CSV" button.
 * It opens a new tab with the exported CSV file.
 */
document.getElementById("export-csv").addEventListener("click", function () {
    var count = prompt("How many rounds should be downloaded as a CSV? (beginning with the lastest round) Please note: The steering angle calibration is not included, make sure to write down the current calibration values.", "5");
    if (count == null || isNaN(count)) {
        return;
    }
    let url = "/api/csv/" + BOARD_ID + "/" + count;

    // create a new link element to open a new tab to url
    var a = document.createElement('a');
    a.href = url;
    a.target = '_blank';
    a.click();

    //window.open("/api/csv/" + BOARD_ID + "/" + count, '_blank').focus();
})

document.getElementById("new-round").addEventListener("click", function () {
    // ask for confirmation using a message box
    if (confirm("Do you really want to start a new round?")) {
        post("update/" + BOARD_ID, { "round": "1" }, function (response) {
            if (!response["success"]) {
                alert(response["msg"]);
            }
        });
    }
});

document.getElementById("restart-esp").addEventListener("click", function () {
    // ask for confirmation using a message box
    if (confirm("Do you really want to restart the ESP?")) {
        post("update/" + BOARD_ID, { "restart": "1" }, function (response) {
            if (!response["success"]) {
                alert(response["msg"]);
            }
        });
    }
});


document.getElementById("tab-speeddiagram-btn").addEventListener("click", function () {
    document.getElementById("tab-speeddiagram").classList.add("active");
    document.getElementById("tab-polardiagram").classList.remove("active");
    document.getElementById("tab-speeddiagram-btn").classList.add("active");
    document.getElementById("tab-polardiagram-btn").classList.remove("active");
});

document.getElementById("tab-polardiagram-btn").addEventListener("click", function () {
    document.getElementById("tab-polardiagram").classList.add("active");
    document.getElementById("tab-speeddiagram").classList.remove("active");
    document.getElementById("tab-polardiagram-btn").classList.add("active");
    document.getElementById("tab-speeddiagram-btn").classList.remove("active");
});

$("show-graph").addEventListener("click", function () {
    let graph_fullscreen = $("graph-fullscreen");
    let graph_fullscreen_exit = $("graph-fullscreen-exit");
    if (graph_fullscreen.style.display == "none") {
        $('tabs').classList.remove('graph-fullscreen');
        graph_fullscreen_exit.style.display = "none";
        graph_fullscreen.style.display = "block";
    } else {
        $('tabs').classList.add('graph-fullscreen');
        graph_fullscreen.style.display = "none";
        graph_fullscreen_exit.style.display = "block";
    }
});

$("btn-layout").addEventListener("click", function () {
    let btn_layout_with_camera = $("btn-layout-with-camera");
    let btn_layout_without_camera = $("btn-layout-without-camera");

    if (btn_layout_without_camera.style.display == "none") {
        $('content-grid').classList.remove('without-camera');
        btn_layout_with_camera.style.display = "none";
        btn_layout_without_camera.style.display = "block";
    } else {
        $('content-grid').classList.add('without-camera');
        btn_layout_without_camera.style.display = "none";
        btn_layout_with_camera.style.display = "block";
    }
});

function refreshStreamBoards() {
    let container = $("stream-boards");
    container.innerHTML = "";

    request("boards/", function (array) {
        if (array.length == 0) return;

        for (let index = 0; index < array.length; index++) {
            const element = array[index];

            let board = document.createElement("div");
            board.classList.add("stream-board");

            board.dataset.id = element["id"];

            board.innerHTML = `
                <span>` + element["name"] + `</span>
                <span>` + element["ip"] + `</span>
            `;

            board.addEventListener("click", function (board_clicked) {
                setVideoStream(board_clicked.currentTarget.dataset.id);
            })

            container.appendChild(board);

        }
    });
}

function setVideoStream(id) {
    CAMERA_ID = id;
    window.stop();
    $("stream-image").src = "";
    $("stream-image").src = "/stream/" + CAMERA_ID + "/force";

    $("stream-chooser").style.display = "none";
}

$("stream-refresh-boards").addEventListener("click", function () {
    refreshStreamBoards();
});
refreshStreamBoards();