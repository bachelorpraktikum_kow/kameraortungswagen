
/**
* This file is part of the project "Kameraortungswagen" licensed under GPLv3.
* It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
* On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
*/

/**
 * Sets the speed in the speedometer element and its color according to the value of speed in the range of 0 - 170.
 * @param {HTMLElement} element The speedometer div (with class speedometer)
 * @param {float} speed The speed of the train in the range 0 - 170
*/
function speedometer(element, speed) {
    if (speed < 0) speed = 0;
    if (speed > 170) speed = 170;

    element.getElementsByTagName("span").item(0).innerText = Math.round(speed);
    let deg = mapRange(speed, 0, 170, -146, 146);
    element.getElementsByTagName("svg").item(0).style.transform = "translate(-50%, -100%) rotate(" + deg + "deg)";

    if (speed >= 120) {
        // color from gradient
        let percent = mapRange(speed, 120, 170, 0, 100) / 100;
        element.getElementsByClassName("speedometer-ball").item(0).style.backgroundColor = pickHex([255, 0, 0], [255, 165, 0], percent);
        element.getElementsByTagName("path").item(0).style.fill = pickHex([255, 0, 0], [255, 165, 0], percent);
        element.getElementsByTagName("span").item(0).style.color = "#fff";
    } else if (speed > 60.0) {
        element.getElementsByClassName("speedometer-ball").item(0).style.backgroundColor = "#fff";
        element.getElementsByTagName("path").item(0).style.fill = "#fff";
        element.getElementsByTagName("span").item(0).style.color = "#000";
    } else {
        element.getElementsByClassName("speedometer-ball").item(0).style.backgroundColor = "#d2d2d2";
        element.getElementsByTagName("path").item(0).style.fill = "#d2d2d2";
        element.getElementsByTagName("span").item(0).style.color = "#000";
    }
}

/**
 * This function interpolates between two colors, specified by RGB values and returns the interpolated color.
 * It takes three arguments, 'color1' and 'color2' which are arrays representing the red, green, and blue values of the two colors, and 'weight' which is a value between 0 and 1 representing the interpolation weight.
 * It returns the interpolated color in the format of "rgb(R, G, B)" where R, G, B are the interpolated red, green, and blue values.
 * 
 * @param {array} color1 Starting color as a rgb array [r, g, b]
 * @param {array} color2 Ending color as a rgb array [r, g, b]
 * @param {float} weight Weight in the range 0-1 for the color
 * @returns The interpolated color as a string in the format "rgb(R, G, B)"
 */
function pickHex(color1, color2, weight) {
    var w1 = weight;
    var w2 = 1 - w1;
    var rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
    Math.round(color1[1] * w1 + color2[1] * w2),
    Math.round(color1[2] * w1 + color2[2] * w2)];
    return "rgb(" + rgb.join(", ") + ")";
}