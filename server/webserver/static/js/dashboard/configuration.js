/**
* This file is part of the project "Kameraortungswagen" licensed under GPLv3.
* It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
* On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
*/


document.getElementById("camera-brightness-light").addEventListener("change", function () {
    if (CAMERA_ID == undefined) return;
    let brightness = document.getElementById("camera-brightness-light").value;
    post("update/" + CAMERA_ID, { "flashlight": brightness }, function (response) {
        if (!response["success"]) {
            alert(response["msg"]);
        }
    });
})


document.getElementById("camera-quality").addEventListener("change", function () {
    if (CAMERA_ID == undefined) return;
    let quality = document.getElementById("camera-quality").value;
    post("update/" + CAMERA_ID, { "quality": quality }, function (response) {
        if (!response["success"]) {
            alert(response["msg"]);
        }
    });
})

document.getElementById("camera-fps").addEventListener("change", function () {
    if (CAMERA_ID == undefined) return;
    let fps = document.getElementById("camera-fps").value;
    post("update/" + CAMERA_ID, { "fps": fps }, function (response) {
        if (!response["success"]) {
            alert(response["msg"]);
        }
    });
})

document.getElementById("camera-off").addEventListener("click", function () {
    if (CAMERA_ID == undefined) return;
    document.getElementById("camera-off").style.display = "none";
    document.getElementById("camera-on").style.display = "block";
    post("update/" + CAMERA_ID, { "camera": "0" }, function (response) {
        if (!response["success"]) {
            alert(response["msg"]);
        }
    });
})

document.getElementById("camera-on").addEventListener("click", function () {
    if (CAMERA_ID == undefined) return;
    document.getElementById("camera-on").style.display = "none";
    document.getElementById("camera-off").style.display = "block";
    post("update/" + CAMERA_ID, { "camera": "1" }, function (response) {
        if (!response["success"]) {
            alert(response["msg"]);
        }
    });
})

document.getElementById("read-interval-save").addEventListener("click", function () {
    let interval = document.getElementById("new-read-interval").value;
    post("update/" + BOARD_ID, { "readInterval": interval }, function (response) {
        if (!response["success"]) {
            alert(response["msg"]);
        } else {
            alert("Read interval saved successfully!");
        }
    });
})

document.getElementById("camera-rotate").addEventListener("click", function () {
    if (CAMERA_ID == undefined) return;
    let stream_img = document.getElementById("stream-image");
    var style = window.getComputedStyle(stream_img);
    if (style.transform == "matrix(-1, 0, 0, -1, 0, 0)") {
        document.getElementById("camera-rotate").style.transform = "rotate(0deg)";
        stream_img.style.transform = "rotate(0deg)";
    } else {
        document.getElementById("camera-rotate").style.transform = "rotate(180deg)";
        stream_img.style.transform = "rotate(180deg)";
    }
});