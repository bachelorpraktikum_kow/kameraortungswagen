/**
* This file is part of the project "Kameraortungswagen" licensed under GPLv3.
* It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
* On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
*/


// Updates the UI with the lasted data from the board.

let steering_angle = $("steering-angle");
let steering_angle_text = $("steering-angle-text");
let steering_angle_reading = $("steering-angle-reading");
let steering_angle_reading_calibration = $("steering-angle-reading-calibration");
let odometer = $("odometer-speed");
let speedometer_odometer = $("speedometer-odometer");
let tie = $("tie-speed");
let speedometer_tie = $("speedometer-tie");
let fixpoint_odometer = $("hall-odometer");
let fixpoint_time = $("hall-time");
let last_update = $("last-update-data");
let last_update_full = $("last-update-full-data");
let wifi_bar = $("wifi-bar");
let wifi_disconnected = $("wifi-disconnected");
let wifi_empty = $("wifi-empty")
let wifi_ssid = $("data-status-wifi");
let battery_full = $("battery-full");
let battery_unknown = $("battery-unknown");
let battery_empty = $("battery-empty");
let battery_content = $("data-status-battery");

let behind_real_time = $("behind-real-time");

let readInterval = $("read-interval");

let stream_image = $("stream-image");
let stream_offline = $("stream-offline");

let chartjs_speeddiagram = $("chartjs-speeddiagram");
let chartjs_polardiagram = $("chartjs-polardiagram");

let ssid = undefined;
let active = false;
let data_loop_timeout = 250;
let configuration_loop_timeout = 10000;

let odometer_background = $("odometer_background");
let tie_background = $("tie_background");
let default_background_color = "rgb(0, 86, 157)";
let warning_background_color = "rgb(255, 126, 0)";

/**
 * This function makes periodic API requests to retrieve data and updates the UI with the received data.
 * It calls the request function with an API endpoint of "data/" + the result of GetCurrentBoard() function + "/1" and a callback function as arguments.
 * It schedules another call to this function every 3 seconds.
*/
function DataLoop() {

    request("rounds/" + GetCurrentBoard() + "/2/", function (array) {
        if (array[0] == undefined) {
            offline();
            return;
        }

        let newest = array[0];
        active = newest["active"] == 1 ? true : false;

        SetWifiData(newest);
        SetBatteryData(newest);

        if (!active) {
            offline();
        } else {
            stream_image.style.filter = "";
            stream_offline.style.display = "none";

            // set steering angle
            steering_angle_reading.innerText = newest["steering"] + " mV"; // on dashboard
            steering_angle_reading_calibration.innerText = newest["steering"]; // on calibration
            steering_angle.innerText = convertMVtoAngle(newest["steering"])["angle"] + " °";
            steering_angle_text.innerText = convertMVtoAngle(newest["steering"])["angleText"];

            // set odometer and tie speed
            odometer.innerText = newest["odometer"] + " mm/s";
            speedometer(speedometer_odometer, newest["odometer"]);
            tie.innerText = newest["tie"] + " mm/s";
            speedometer(speedometer_tie, newest["tie"]);

            // set fixpoint
            fixpoint_odometer.innerText = newest["fixpoint"]["odometer"] + " odometer steps ago";
            fixpoint_time.innerText = (newest["fixpoint"]["time"] / 1000) + "s from last packet";

            // set last update
            last_update_full.innerText = formattime(new Date(newest["rtc"]["server"]));
            last_update.innerText = timeago(new Date(newest["rtc"]["server"]));

            // set latency
            behind_real_time.innerText = ((newest["rtc"]["server"] - newest["rtc"]["board"]) / 1000) + " seconds";
        }

        SetSpeedGraphData(array);
    });


    setTimeout(() => {
        DataLoop();
    }, data_loop_timeout)
}

/**
 * This function makes periodic API requests to retrieve configuration data of the current board and camera board and updates the UI with the received data.
 * It calls the request function with an API endpoint of "configuration/" + the result of GetCurrentBoard() function and a callback function as arguments.
 * It schedules another call to this function every 10 seconds.
 */
function ConfigurationDataLoop() {

    // reloadImage();

    request("configuration/" + GetCurrentBoard() + "/", function (array) {
        if (array["wifi"] == undefined) return;
        ssid = array["wifi"]["ssid"];
        readInterval.innerText = array["config"]["read_interval"];
    });

    if (CAMERA_ID != undefined) {
        request("configuration/" + CAMERA_ID + "/", function (array) {
            if (array["wifi"] == undefined) return;

            $("camera-brightness-light").value = array["config"]["flashlight"];
            $("camera-fps").value = array["config"]["camera_fps"];
            $("camera-quality").value = array["config"]["camera_quality"];

            if (array["config"]["camera"] == "1") {
                $("camera-off").style.display = "block";
                $("camera-on").style.display = "none";
            } else {
                $("camera-off").style.display = "none";
                $("camera-on").style.display = "block";
            }
        });
    }

    setTimeout(() => {
        ConfigurationDataLoop();
    }, configuration_loop_timeout)
}

/**
 * This function sets the board status to offline.
 * It sets the stream image to grayscale and displays the offline text.
 * It sets the steering angle, odometer, tie speed, fixpoint and last update to "?"
 * It is called when the board is not active.
 */
function offline() {
    stream_image.style.filter = "grayscale(100%) brightness(50%)";
    stream_offline.style.display = "block";

    // set steering angle
    steering_angle_reading.innerText = "? mV"; // on dashboard
    steering_angle_reading_calibration.innerText = "?"; // on calibration
    steering_angle.innerText = "? °";
    steering_angle_text.innerText = "?";

    // set odometer and tie speed
    odometer.innerText = "? mm/s";
    tie.innerText = "? mm/s";

    // set fixpoint
    fixpoint_odometer.innerText = "? odometer steps ago";
    fixpoint_time.innerText = "? s from last packet";

    // set last update
    last_update_full.innerText = "?";
    last_update.innerText = "?";

}

// function reloadImage() {
//     src = stream_image.src.split("#")[0];
//     src += "#" + new Date().getTime();
//     stream_image.src = src;
// }

let chart_speeddiagram = undefined;
let chart_polardiagram = undefined;
let last_combined_data = "";
/**
 * Sets the speed diagram data.
 * It takes an array of data entries as an argument.
 * 
 * @param {*} array Array of data entries
 */
function SetSpeedGraphData(array) {

    if (array.length == 0) return;

    let dataset_odometer_previous = [];
    let dataset_odometer_current = [];
    let dataset_tie_previous = [];
    let dataset_tie_current = [];

    let dataset_steering = [];
    let dataset_diagram_angles = [];

    let current_dataset = 0;

    let highest_odometer = 0;
    let highest_steering = 0;

    let counter = 0;

    let actual_odometer_speed = 0;
    let actual_tie_speed = 0;

    // go through all entries backwards
    for (let i = array.length - 1; i >= 0; i--) {
        // get current entry
        let entry = array[i];

        // if entry is a fixpoint increase current dataset
        if (entry["fixpoint"]["hall"] == "1") {
            current_dataset++;
        }

        // create x and y pair from entry at fixpoint.odometer and odometer.speed
        let x = entry["fixpoint"]["odometer"];
        let yOdometer = entry["odometer"];
        let yTie = entry["tie"];
        let y_steering_value = convertMVtoAngle(entry["steering"])["angle"];

        // get present speeds
        actual_odometer_speed = yOdometer;
        actual_tie_speed = yTie;

        // set x as highest odometer if it is higher
        if (x > highest_odometer) {
            highest_odometer = x;
        }

        // set x as highest odometer if it is higher


        // add to dataset
        if (current_dataset == 1) {
            dataset_odometer_previous.push({ x: x, y: yOdometer });
            dataset_tie_previous.push({ x: x, y: yTie });
        } else if (current_dataset == 2) {
            dataset_odometer_current.push({ x: x, y: yOdometer });
            dataset_tie_current.push({ x: x, y: yTie });
            // add to dataset
            dataset_steering.push(y_steering_value);
        }

    }

    for (let i = 0; i < dataset_steering.length; i++) {
        let radial_value = i * ((2 * Math.PI) / dataset_steering.length);
        let angle = Math.round((radial_value / Math.PI) * 180);
        dataset_diagram_angles.push(angle);
    }

    // check if any speed sensor is zero if so give out warning
    if (actual_odometer_speed == 0) {
        odometer_background.style.animation = "warning 0.75s ease-in-out infinite";
    } else {
        odometer_background.style.animation = "none";
        odometer_background.style.backgroundColor = default_background_color;
    }

    if (actual_tie_speed == 0) {
        tie_background.style.animation = "warning 0.75s ease-in-out infinite";
    } else {
        tie_background.style.animation = "none";
        tie_background.style.backgroundColor = default_background_color;
    }

    // skip if data is the same
    let current_combined_data = [dataset_odometer_previous, dataset_odometer_current, dataset_tie_previous, dataset_tie_current].join(",");
    if (last_combined_data == current_combined_data) {
        return;
    }
    last_combined_data = current_combined_data;


    const color_odometer_current = "#e99100";
    const color_tie_current = "#ffffff";

    const color_odometer_previous = "#9c6100";
    const color_tie_previous = "#858585";

    const data = {
        labels: [...Array(highest_odometer + 1).keys()],
        datasets: [
            {
                label: 'Current Round (Odometer)',
                data: dataset_odometer_current,
                borderColor: color_odometer_current,
                backgroundColor: color_odometer_current,
                pointRadius: 3,
            },
            {
                label: 'Current Round (Tie)',
                data: dataset_tie_current,
                borderColor: color_tie_current,
                backgroundColor: color_tie_current,
                pointRadius: 3,
            },
            {
                label: 'Previous Round (Odometer)',
                data: dataset_odometer_previous,
                borderColor: color_odometer_previous,
                backgroundColor: color_odometer_previous,
                pointRadius: 0,
            },
            {
                label: 'Previous Round (Tie)',
                data: dataset_tie_previous,
                borderColor: color_tie_previous,
                backgroundColor: color_tie_previous,
                pointRadius: 0,
            }
        ]
    };

    const config = {
        type: 'line',
        data: data,
        plugins: [htmlLegendPlugin],
        options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                title: {
                    display: true,
                    text: 'Speed-Diagram',
                    padding: {
                        top: 10,
                        bottom: 30
                    },
                    font: {
                        size: 24
                    }
                },
                legend: {
                    display: false,
                },
                htmlLegend: {
                    containerID: 'chartjs-speeddiagram-legend',
                },
            },

            scales: {
                x: {
                    display: true,
                    type: 'linear',
                    min: 0,
                    title: {
                        display: true,
                        text: 'Odometer Steps'
                    },
                },
                y: {
                    display: true,
                    type: 'linear',
                    min: 0,
                    title: {
                        display: true,
                        text: 'Speed (mm/s)'
                    },
                }
            },

            animation: {
                duration: 0
            }
        },
    };

    if (chart_speeddiagram != undefined) {
        chart_speeddiagram.destroy();
    }
    Chart.defaults.color = "#fff";
    chart_speeddiagram = new Chart(chartjs_speeddiagram, config);

    if (chart_polardiagram != undefined) {
        chart_polardiagram.destroy();
    }
    chart_polardiagram = SetUpPolarDiagram(dataset_diagram_angles, dataset_steering);

    //document.getElementById("chartjs-speeddiagram-legend").innerHTML = chart.generateLegend();

}

/**
 * Set up the polar diagram
 * @param {*} angles array of angles
 * @param {*} steering_values array of steering values
 * @returns 
 */
function SetUpPolarDiagram(angles, steering_values) {
    const color_steering_current = "#e99100";
    const minAngle = parseInt(getMinMaxAngle()["minAngle"]);
    const maxAngle = parseInt(getMinMaxAngle()["maxAngle"]);

    const data = {
        labels: angles,
        datasets: [
            {
                label: 'Current Round (Steering)',
                data: steering_values,
                fill: false,
                borderColor: color_steering_current,
                backgroundColor: color_steering_current,
            }
        ]
    };

    const grid_color = "e99100";

    const config = {
        type: 'radar',
        data: data,
        plugins: [htmlLegendPlugin],
        options: {
            responsive: true,
            maintainAspectRatio: false,

            plugins: {
                title: {
                    display: false,
                    text: 'Polardiagram',
                    padding: {
                        top: 10,
                        bottom: 30
                    },
                    font: {
                        size: 24
                    }
                },
                legend: {
                    display: false,
                },
                htmlLegend: {
                    containerID: 'chartjs-polardiagram-legend',
                },
            },

            scales: {
                r: {
                    min: minAngle - 10,
                    max: maxAngle + 10,
                    angleLines: {
                        color: grid_color,
                        display: false,
                        reverse: true
                    },
                    grid: {
                        color: grid_color,
                        circular: true,
                    },
                    pointLabels: {
                        display: false,
                        color: grid_color
                    },
                    ticks: {
                        color: 'white',
                        showLabelBackdrop: false,
                        minRotation: 90
                    }
                },
            },

            animation: {
                duration: 0
            }
        }
    };

    Chart.defaults.color = "#fff";
    return new Chart(chartjs_polardiagram, config);
    //document.getElementById("chartjs-speeddiagram-legend").innerHTML = chart.generateLegend();
}



/**
 * Returns the id of the current board
 * 
 * @returns id of the current board
 */
function GetCurrentBoard() {
    return BOARD_ID;
}


/**
 * This function sets the wifi data in the UI.
 * @param {*} data The data object containing the wifi data.
 */
function SetWifiData(data) {
    // North of -30 dBm: Too good to be true, or signal saturation (not good).
    // -30 dBm: The best possible.
    // -50 dBm: Excellent signals.
    // -60 dBm: Very good signals.
    // -65 dBm: Good, reliable signals. Up to now, you’ve always had full bars.
    // -70 dBm: This is the threshold where you might have lost a signal bar and are about to lose another, if not already. But the connection is still solid.
    // -75 dBm: This is where things start getting problematic, but the connection might still be usable.
    // -80 dBm: Borderline useless — you barely have just one bar.
    // -90 dBm: The signal is really weak, (almost) impossible to connect to.
    // South of -90 dBm: Forget about it.
    let wifitext = " (" + data["wifi"] + "db)";
    if (data["wifi"] > -50) { // or 30 - 50 extra?
        wifi_bar.style.clipPath = "inset(0% 0px 0px 0px)"; // -> Excellent
        wifitext = "Excellent" + wifitext;
    } else if (data["wifi"] > -60) {
        wifi_bar.style.clipPath = "inset(25% 0px 0px 0px)"; // -> Very Good
        wifitext = "Very Good" + wifitext;
    } else if (data["wifi"] > -65) {
        wifi_bar.style.clipPath = "inset(30% 0px 0px 0px)"; // -> Good
        wifitext = "Good" + wifitext;
    } else if (data["wifi"] > -70) {
        wifi_bar.style.clipPath = "inset(40% 0px 0px 0px)"; // -> Stable
        wifitext = "Stable" + wifitext;
    } else if (data["wifi"] > -80) { // or 75 ?
        wifi_bar.style.clipPath = "inset(40% 0px 0px 0px)"; // -> Unstable
        wifitext = "Unstable" + wifitext;
    } else {
        wifi_bar.style.clipPath = "inset(100% 0px 0px 0px)"; // -> Disconnected
        wifitext = "Disconnected" + wifitext;
    }

    if (ssid != undefined) {
        wifitext += " | " + ssid;
    }

    // if disconnected hide show disconnected
    if (!active) {
        wifitext = "Disconnected";
        wifi_disconnected.style.display = "block";
        wifi_bar.style.display = "none";
        wifi_empty.style.display = "none";
    } else {
        wifi_disconnected.style.display = "none";
        wifi_bar.style.display = "block";
        wifi_empty.style.display = "block";
    }

    wifi_ssid.dataset.text = wifitext;
}

/**
 * This function analyzes the battery data, calculates the battery percentage and updates the UI with the received data.
 * @param {*} data The data object containing the battery data.
 */
function SetBatteryData(data) {

    let battery_mV = data["battery"];
    let battery_percent = 0;

    battery_percent = -0.0000076611819 * (battery_mV * battery_mV) + 0.2232061133327 * battery_mV - 704.9567994939885

    if (battery_percent > 100) {
        battery_percent = 100;
    }
    if (battery_percent < 0) {
        battery_percent = 0;
    }

    let clippath = mapRange(battery_percent, 0, 100, 87, 24);
    //console.log(battery_mV + "mV : " + battery_percent + "% : " + clippath);

    battery_full.style.clipPath = "inset(0px " + Math.round(clippath) + "% 0px 0px)";

    // if disconnected hide show disconnected
    if (!active) {
        battery_unknown.style.display = "block";
        battery_full.style.display = "none";
        battery_empty.style.display = "none";
        battery_content.dataset.text = "?";
    } else {
        battery_unknown.style.display = "none";
        battery_full.style.display = "block";
        battery_empty.style.display = "block";
        battery_content.dataset.text = battery_mV + "mV | " + Math.round(battery_percent) + "%";
    }
}

// Start the loop for the api requests
ConfigurationDataLoop();
DataLoop();