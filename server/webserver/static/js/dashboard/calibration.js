/**
* This file is part of the project "Kameraortungswagen" licensed under GPLv3.
* It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
* On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
*/

let modal_configuration = $("modal-configuration");
let modal_configuration_btn = $("btn-configuration");
let modal_close = document.getElementsByClassName("modal-close")[0];

let calibration_add = $("calibration-add");
let calibration_save = $("calibration-save");

modal_configuration_btn.onclick = function () {
    modal_configuration.style.display = "block";
}
modal_close.onclick = function () {
    // confirm before closing
    if (confirm("Are you sure you want to close the configuration? Please save your changes before closing.")) {
        modal_configuration.style.display = "none";
    }
}


request("steering/" + BOARD_ID, function (steering_table) {
    steering_table.forEach(steering_entry => {
        addSteeringCalibration(steering_entry.min, steering_entry.max, steering_entry.angle, steering_entry.text);
    });
});

calibration_add.addEventListener("click", function () {
    addSteeringCalibration();
});
calibration_save.addEventListener("click", function () {
    post("steering/" + BOARD_ID, getSteeringTable(), function (response) {
        if (!response["success"]) {
            alert(response["msg"]);
        } else {
            alert("Calibration saved successfully!");
        }
        modal_configuration.style.display = "none";
    });
});


function addSteeringCalibration(min = "", max = "", angle = "", text = "") {
    let list = $("steering-list");

    // create new steering entry
    let new_steering_entry = document.createElement("div");
    new_steering_entry.classList.add("steering-entry");

    new_steering_entry.innerHTML = `
        <div class="steering-entry-value">
            <input data-steering-id="" data-steering-type="min" type="number" name="min" value="` + min + `">
            <span>mV</span>
            <span>Min</span>
        </div>
        <div class="steering-entry-value">
            <input data-steering-id="" data-steering-type="max" type="number" name="max" value="` + max + `">
            <span>mV</span>
            <span></span>
            <span>Max</span>
        </div>
        <div class="steering-entry-value">
            <input data-steering-id="" data-steering-type="angle" type="number" name="low" value="` + angle + `">
            <input data-steering-id="" data-steering-type="text" type="text" name="text" value="` + text + `">
            <span>Angle (in °)</span>
            <span>Description</span>
        </div>
        <div class="steering-entry-value" onclick="removeSteeringCalibration(this)">
            <span>-</span>
        </div>
    `;

    list.appendChild(new_steering_entry);
}

function removeSteeringCalibration(element) {
    element.parentNode.remove();
}

function convertMVtoAngle(angle_mV) {
    let steering_table = getSteeringTable();

    let angle = 0;
    let angleText = "Undefined";

    for (let i = 0; i < steering_table.length; i++) {
        let steering_entry = steering_table[i];

        if (angle_mV >= steering_entry.min && angle_mV <= steering_entry.max) {
            angle = steering_entry.angle;
            angleText = steering_entry.text;
            break;
        }
    }

    return {
        angle: parseInt(angle),
        angleText: angleText
    };
}


function getSteeringTable() {
    let steering_list = $("steering-list");
    let steering_entries = steering_list.getElementsByClassName("steering-entry");

    let steering_table = [];

    for (let i = 0; i < steering_entries.length; i++) {
        let steering_entry = steering_entries[i];

        let min = steering_entry.getElementsByClassName("steering-entry-value")[0].getElementsByTagName("input")[0];
        let max = steering_entry.getElementsByClassName("steering-entry-value")[1].getElementsByTagName("input")[0];
        let angle = steering_entry.getElementsByClassName("steering-entry-value")[2].getElementsByTagName("input")[0];
        let text = steering_entry.getElementsByClassName("steering-entry-value")[2].getElementsByTagName("input")[1];

        let steering_entry_object = {
            min: min.value,
            max: max.value,
            angle: angle.value,
            text: text.value
        };

        steering_table.push(steering_entry_object);
    }

    return steering_table;
}

function getMinMaxAngle() {
    let steering_table = getSteeringTable();
    let minAngle = Number.MAX_SAFE_INTEGER;
    let maxAngle = Number.MIN_SAFE_INTEGER;
    for (let i = 0; i < steering_table.length; i++) {
        if (parseInt(steering_table[i]["angle"]) < minAngle) {
            minAngle = steering_table[i]["angle"];
        }
        if (parseInt(steering_table[i]["angle"]) > maxAngle) {
            maxAngle = steering_table[i]["angle"];
        }
    }
    return {
        minAngle: minAngle,
        maxAngle: maxAngle
    };
}