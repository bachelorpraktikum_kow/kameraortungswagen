function start_dashboard_tour() {
    let driver = new Driver({
        allowClose: false, closeBtnText: 'Abort tour', onNext: function (el) {
            if (!driver.hasNextStep()) {
                continue_dashboard_tour();
            }
        }
    });

    driver.defineSteps([
        {
            element: '[data-tour="data"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the data section. Here you can see the current sensor reads from the trolley.',
                position: 'bottom'
            }
        },
        {
            element: '[data-tour="data-steering"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the data from the steering sensor. It shows the current steering angle in ° and in text. The steering angle is calculated from the steering sensor voltage.<br>The calibration table for the mV to angle conversion can be changed in the settings',
                position: 'bottom'
            }
        },
        {
            element: '[data-tour="data-odometer"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the data from the odometer sensor. It shows the current speed of the trolley in mm/s.',
                position: 'bottom'
            }
        },
        {
            element: '[data-tour="data-tie"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the data from the tie sensor. It also shows the current speed in mm/s, but is calculated from the number of ties the trolley has counted.',
                position: 'bottom'
            }
        },
        {
            element: '[data-tour="data-hall"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the data from the hall sensor. It shows the last time the trolley passed the hall sensor (fixpoint on the track) in odometer steps and in seconds.',
                position: 'bottom'
            }
        },
        {
            element: '[data-tour="data-updated"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the time the data was last updated. When hovering over the time, the exact time is shown.',
                position: 'top'
            }
        },
        {
            element: '[data-tour="status"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the status section. The status is updated with every data packet.',
                position: 'left'
            }
        },
        {
            element: '[data-tour="status-latency"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the latency of the data packets. The latency is the time between the trolley sending the data and the webserver receiving the data.',
                position: 'left'
            }
        },
        {
            element: '[data-tour="status-wifi"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the WiFi signal strength of the trolley. When hovering over the symbol, the currently connected wifi network as well as the the signal strength is shown in words and dB.',
                position: 'left'
            }
        },
        {
            element: '[data-tour="status-battery"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'This is the battery percentage of the trolley. When hovering over the symbol, the battery voltage is shown in milli-volts (mV) and percent.',
                position: 'left'
            }
        },
        {
            element: '[data-tour="status-layout"]',
            popover: {
                title: 'Quick Tour: Dashboard',
                description: 'Clicking this icon toggles the layout to the camera layout (or back to this layout). The camera layout shows the camera feed from a selected trolley.<br><br><strong>The tour will continue in the camera layout.</strong>',
                position: 'left'
            }
        }
    ]);
    driver.start();

}

function continue_dashboard_tour() {
    // change layout to camera layout
    $("btn-layout-with-camera").click();

    // wait 1s for layout to change
    setTimeout(() => {
        // Continue tour in camera layout
        driver = new Driver({ allowClose: false, closeBtnText: 'Abort tour', });
        driver.defineSteps([
            {
                element: '[data-tour="status-settings"]',
                popover: {
                    title: 'Quick Tour: Dashboard',
                    description: 'Clicking on this button opens the settings page. The settings page allows you to edit the <strong>calibration table for the steering sensor</strong>. The calibration table is used to convert the voltage of the steering sensor to an angle in degrees.',
                    position: 'left'
                }
            },
            {
                element: '[data-tour="stream"]',
                popover: {
                    title: 'Quick Tour: Dashboard',
                    description: 'This is the stream section. Select a trolley from the menu to see the camera feed from that trolley. The camera feed can be that of a different trolley than the one you are currently viewing the dashboard of.',
                    position: 'right'
                }
            },
            {
                element: '[data-tour="stream-controls"]',
                popover: {
                    title: 'Quick Tour: Dashboard',
                    description: 'Here you can control the camera settings. You can change the brightness for the onboard flashlight, as well as toggle the camera on or off, and change the resolution and frames per seconds (FPS) of the camera. The camera settings are saved on the ESP32 and will be loaded on startup.',
                    position: 'top'
                }
            },
            {
                element: '[data-tour="actions"]',
                popover: {
                    title: 'Quick Tour: Dashboard',
                    description: 'This is the quick actions section. Here you can quickly perform tasks.',
                    position: 'left'
                }
            },
            {
                element: '[data-tour="actions-new-round"]',
                popover: {
                    title: 'Quick Tour: Dashboard',
                    description: 'This button starts a new round. It simulates the trolley passing the hall sensor, which resets the odometer and tie counter, and starts a new round.',
                    position: 'left'
                }
            },
            {
                element: '[data-tour="actions-export"]',
                popover: {
                    title: 'Quick Tour: Dashboard',
                    description: 'This button exports the current data in rounds to a CSV file. The CSV file can be opened in Excel or any other spreadsheet program.',
                    position: 'left'
                }
            },
            {
                element: '[data-tour="actions-restart"]',
                popover: {
                    title: 'Quick Tour: Dashboard',
                    description: 'This button restarts the trolley. Sometimes the trolley has to be restarted to fix issues.',
                    position: 'left'
                }
            },
            {
                element: '[data-tour="diagrams-chooser"]',
                popover: {
                    title: 'Quick Tour: Dashboard',
                    description: 'This is the diagram chooser. Here you can choose between a speed diagram and a polardiagram.<br><br> The speed diagram below shows the (odometer & tie) speed of the trolley over odometer steps for the last two rounds. The polardiagram shows the direction / steering angle of the trolley over time.',
                    position: 'left'
                }
            },
            {
                element: '[data-tour="board-name"]',
                popover: {
                    title: 'Quick Tour: Dashboard',
                    description: 'The name and ip address of the currently shown dashboard.<br><br>That concludes the quick tour. You can always start the tour again by clicking the "Let\'s tour the website" button on the landing page.',
                    position: 'bottom'
                }
            },
        ]);
        driver.start();
    }, 1000);

}

function start_navigation_tour() {
    const driver = new Driver({ allowClose: false, closeBtnText: 'Abort tour', });
    driver.defineSteps([
        {
            element: '[data-tour="header"]',
            popover: {
                title: 'Quick Tour: Navigation',
                description: 'This is the header. Here you can find the name of the website and the navigation menu. Clicking the name will bring you back to this page.',
                position: 'bottom'
            }
        },
        {
            element: '[data-tour="navigation"]',
            popover: {
                title: 'Quick Tour: Navigation',
                description: 'This is the navigation menu. Here you can find buttons to navigate to this page (\'Home\'), the board overview (\'Boards\') or the login page (\'Login\').<br><br>To continue the tour, <strong>click on the \'Boards\'</strong> button.',
                position: 'bottom'
            }
        },
    ]);
    driver.start();

}

function start_connect_tour() {
    const driver = new Driver({ allowClose: false, closeBtnText: 'Abort tour', showButtons: true, });
    driver.defineSteps([
        {
            element: 'section:nth-of-type(1)',
            popover: {
                title: 'Quick Tour: Boards',
                description: 'Here you can see the ip address of the webserver. This is the ip address you need to connect to the webserver from the trolley.<br><br>If the ip address is not numeric (e.g. localhost) or 127.0.0.1, you need to find the ip address of the webserver (e.g. 192.168.178.30) manually. You can do this by checking the ip address of the computer running the webserver or in the settings of your router.',
                position: 'bottom'
            }
        },
        {
            element: '#list',
            popover: {
                title: 'Quick Tour: Boards',
                description: 'Currently there are no boards connected. To continue the tour, <strong>connect a board by configuring it over bluetooth</strong> (see documentation).<br><i>Note:</i>This process can take up to 60 seconds. The board will show up here when it is connected.',
                position: 'bottom'
            }
        },
    ]);
    driver.start();


    // start loop until a board is connected
    let interval = setInterval(function () {
        if (document.getElementById("list").getElementsByClassName('board').length > 0) {
            clearInterval(interval);
            tour_running = true;
            driver.reset();
            start_board_tour();
        }
    }, 1000);
}

function start_board_tour() {
    // wait for 1 second
    setTimeout(() => {
        const driver = new Driver({ allowClose: false, closeBtnText: 'Abort tour', });
        driver.defineSteps([
            {
                element: '[data-tour="board-element"]',
                popover: {
                    title: 'Quick Tour: Boards',
                    description: 'Now that a board is connected, you can see it here. It represents a trolley. Clicking on a board will bring you to the dashboard of the board.<br><br><i>Please note: While in the tour, the board overview will not be automatically updated. Please finish the tour first.</i>',
                    position: 'bottom'
                }
            },
            {
                element: '[data-tour="board-element-name"]',
                popover: {
                    title: 'Quick Tour: Boards',
                    description: 'This is the name of the trolley. You can change it using bluetooth.',
                    position: 'bottom'
                }
            },
            {
                element: '[data-tour="board-element-ip"]',
                popover: {
                    title: 'Quick Tour: Boards',
                    description: 'This is the ip address of the trolley.',
                    position: 'bottom'
                }
            },
            {
                element: '[data-tour="board-element-updated"]',
                popover: {
                    title: 'Quick Tour: Boards',
                    description: 'This is the last time the trolley sent data to the server.',
                    position: 'bottom'
                }
            },
            {
                element: '[data-tour="board-element-active"]',
                popover: {
                    title: 'Quick Tour: Boards',
                    description: 'This is the status of the trolley. It can be \'Online\' (green dot) or \'Offline\' (red dot). The board is considered offline when it has not sent data to the server for more than 30 seconds.',
                    position: 'bottom'
                }
            },
            {
                element: '[data-tour="board-element"]',
                popover: {
                    title: 'Quick Tour: Boards',
                    description: 'To continue the tour, <strong>click on this board</strong>.',
                    position: 'bottom'
                }
            },
        ]);
        driver.start();

    }, 1000);
}


// incept the close button, so that the cookie is removed when the tour is closed
// this is needed, because driver.js does not provide a callback for when the tour is closed
setInterval(() => {
    try {
        let close_btns = document.getElementsByClassName("driver-close-btn");
        for (let i = 0; i < close_btns.length; i++) {
            close_btns[i].addEventListener("click", function () {
                // remove the cookie named tour
                document.cookie = 'tour=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                tour_running = false;
            });
        }
    } catch (e) { }
}, 1000);



// check if tour is currently running for the boards page
let tour_running = false;
let tour_page = undefined;

// check if on the landing page
let tour_button = document.getElementById("tour-start");
// check if the button exists, and if so, add an event listener to it
if (tour_button != null) {
    // remove the cookie named tour
    document.cookie = 'tour=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';

    tour_page = "landing";
    // add an event listener to the button, which sets a cookie named "tour" to true
    tour_button.addEventListener("click", function () {
        document.cookie = 'tour=true; path=/';
        start_navigation_tour();
    });
}

// check if on the boards page
let board_list = document.getElementById("list");
// check if the boards element exists, and if so, add an event listener to it
if (board_list != null) {
    tour_page = "boards";
    if (document.cookie.split('; ').find(row => row.startsWith('tour=true'))) {
        console.log("Starting tour on boards page");
        /* check if boards exist, and if so, start the board tour, otherwise start the connect tour
        if (board_list.getElementsByClassName('board').length > 0) {
            start_board_tour();
        } else {*/
        start_connect_tour();
        //}
    }
}

// check if on the dashboard page
let dashboard = document.querySelectorAll('[data-tour="data"]');
if (dashboard.length > 0) {
    tour_page = "dashboard";
    if (document.cookie.split('; ').find(row => row.startsWith('tour=true'))) {
        console.log("Starting tour on dashboard page");
        // remove the cookie named tour
        document.cookie = 'tour=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        start_dashboard_tour();
    }
}







