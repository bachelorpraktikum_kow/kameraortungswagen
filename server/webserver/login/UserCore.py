"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

import re
from datetime import datetime, timezone, timedelta
from enum import Enum

import jwt
from argon2 import PasswordHasher, DEFAULT_TIME_COST, DEFAULT_MEMORY_COST, DEFAULT_PARALLELISM, DEFAULT_HASH_LENGTH, DEFAULT_RANDOM_SALT_LENGTH, Type

from database.SQLiteDatabase import SQLiteDatabase
from config import AUTH_TIME_HOURS


# set explicit arguments for PasswordHasher
password_hasher_args = {
    'time_cost': DEFAULT_TIME_COST,
    'memory_cost': DEFAULT_MEMORY_COST,
    'parallelism': DEFAULT_PARALLELISM,
    'hash_len': DEFAULT_HASH_LENGTH,
    'salt_len': DEFAULT_RANDOM_SALT_LENGTH,
    'encoding': 'utf-8',
    'type': Type.ID
}

# set maximum login age
MAX_AGE = AUTH_TIME_HOURS * 60 * 60


class Role(Enum):
    """
    This class is an enum that contains the roles of the users.
    """
    ADMIN = 'admin'
    USER = 'user'


class Time():
    """
    This class is a wrapper for the datetime class. It is used to compare two Time objects and to create a Time object from a timestamp.
    """
    @staticmethod
    def from_timestamp(timestamp: int):
        """
        This function creates a Time object from a timestamp.

        :param timestamp: Timestamp to create the Time object from
        :type timestamp: int
        :return: Time object
        :rtype: Time
        """
        obj = Time()
        obj.__overwrite_datetime_obj(datetime.fromtimestamp(timestamp, timezone.utc))
        return obj


    @staticmethod
    def from_datetime_utc(datetime_obj: datetime):
        """
        This function creates a Time object from a datetime object with UTC timezone.

        :param datetime_obj: Datetime object to create the Time object from
        :type datetime_obj: datetime
        :return: Time object
        :rtype: Time
        """
        if datetime_obj.tzinfo != timezone.utc:
            raise ValueError('Timezone is not UTC.')

        obj = Time()
        obj.__overwrite_datetime_obj(datetime(year = datetime_obj.year, month = datetime_obj.month, day = datetime_obj.day, hour = datetime_obj.hour, minute = datetime_obj.minute, second = datetime_obj.second, microsecond = datetime_obj.microsecond, tzinfo = datetime_obj.tzinfo))
        return obj


    def __init__(self) -> None:
        """
        This function creates a Time object with the current time.
        """
        self.__datetime_obj = datetime.now(timezone.utc)


    def __overwrite_datetime_obj(self, datetime_obj: datetime):
        """
        This function overwrites the datetime object of the Time object.

        :param datetime_obj: Datetime object to overwrite the datetime object of the Time object with
        :type datetime_obj: datetime
        :return: None
        :rtype: None"""
        self.__datetime_obj = datetime_obj


    def compare(self, other) -> int:
        """
        This function compares two Time objects.

        :param other: Time object to compare with
        :type other: Time
        :return: -1 if self is smaller than other, 0 if self is equal to other, 1 if self is greater than other
        :rtype: int
        """
        if type(other) != Time:
            raise ValueError('parameter other is not of type \'Time\'')

        if self.__datetime_obj.year < other.__datetime_obj.year:
            return -1
        elif self.__datetime_obj.year > other.__datetime_obj.year:
            return 1

        if self.__datetime_obj.month < other.__datetime_obj.month:
            return -1
        elif self.__datetime_obj.month > other.__datetime_obj.month:
            return 1

        if self.__datetime_obj.day < other.__datetime_obj.day:
            return -1
        elif self.__datetime_obj.day > other.__datetime_obj.day:
            return 1

        if self.__datetime_obj.hour < other.__datetime_obj.hour:
            return -1
        elif self.__datetime_obj.hour > other.__datetime_obj.hour:
            return 1

        if self.__datetime_obj.minute < other.__datetime_obj.minute:
            return -1
        elif self.__datetime_obj.minute > other.__datetime_obj.minute:
            return 1

        if self.__datetime_obj.second < other.__datetime_obj.second:
            return -1
        elif self.__datetime_obj.second > other.__datetime_obj.second:
            return 1

        if self.__datetime_obj.microsecond < other.__datetime_obj.microsecond:
            return -1
        elif self.__datetime_obj.microsecond > other.__datetime_obj.microsecond:
            return 1

        return 0


    def add_seconds(self, seconds: int) -> None:
        delta = timedelta(0, float(seconds))
        self.__datetime_obj = self.__datetime_obj + delta


    def to_timestamp(self) -> int:
        return int(self.__datetime_obj.timestamp())


class User():
    """
    This class is a wrapper for the user table in the database.
    """
    @staticmethod
    def __create_user_from(column_name: str, value):
        value = str(value)

        db = SQLiteDatabase()
        rows = db.query(f'SELECT * FROM user WHERE {column_name} = ?;', (value,))

        if len(rows) > 1:
            raise LookupError('Multiple users were found for the given query.')
        elif len(rows) < 1:
            raise LookupError('No user was found for the given query.')

        user_obj = rows[0]
        return User(user_obj['user_id'], user_obj['user_name'], user_obj['user_password'], Role(user_obj['user_role']))


    @staticmethod
    def create_from_uid(uid: int):
        return User.__create_user_from('user_id', uid)


    @staticmethod
    def create_from_username(username: str):
        return User.__create_user_from('user_name', username)


    @staticmethod
    def get_uid_by_username(username: str) -> int:
        db = SQLiteDatabase()
        rows = db.query('SELECT * FROM user WHERE user_name = ?;', (username,))

        if len(rows) != 1:
            return None

        user_obj = rows[0]
        return int(user_obj['user_id'])


    @staticmethod
    def split_subject(subject: str) -> tuple:
        """
        This function splits a subject string into the uid and the username.

        :param subject: Subject string to split
        :type subject: str
        :return: Tuple of uid and username
        :rtype: tuple[int, str]
        """

        if not User.is_valid_subject(subject):
            raise ValueError(f'Invalid subject string.')

        (uid_str, username) = subject.split('@', 1)
        return (int(uid_str), username)


    @staticmethod
    def is_valid_subject(subject: str) -> bool:
        """
        This function checks if a subject string is valid.

        :param subject: Subject string to check
        :type subject: str
        :return: True if the subject string is valid, False otherwise
        :rtype: bool
        """
        (uid_str, username) = subject.split('@', 1)
        return User.is_valid_uid_str(uid_str) and User.is_valid_username(username)


    @staticmethod
    def is_valid_uid_str(uid_str: str) -> bool:
        """
        This function checks if a uid string is valid.

        :param uid_str: Uid string to check
        :type uid_str: str
        :return: True if the uid string is valid, False otherwise
        :rtype: bool
        """
        return bool(re.fullmatch(r'^\d+$', uid_str))


    @staticmethod
    def is_valid_username(username: str) -> bool:
        """
        This function checks if a username is valid.

        :param username: Username to check
        :type username: str
        :return: True if the username is valid, False otherwise
        :rtype: bool
        """
        return bool(re.fullmatch(r'^\w+$', username))


    @staticmethod
    def create_subject(uid_str: str, username: str) -> str:
        return f'{uid_str}@{username}'


    @staticmethod
    def does_username_exist(username: str) -> bool:
        db = SQLiteDatabase()
        rows = db.query('SELECT user_name FROM user WHERE user_name = ?;', (username,))

        return len(rows) >= 1


    @staticmethod
    def get_all_users():
        db = SQLiteDatabase()
        rows = db.query('SELECT * FROM user;', ())

        users = list()

        for row in rows:
            users.append(User(row['user_id'], row['user_name'], row['user_password'], Role(row['user_role'])))

        return users


    @staticmethod
    def add_new_user(username: str, password: str, role: Role):
        if User.does_username_exist(username):
            raise ValueError('The username does already exist.')

        ph = PasswordHasher(**password_hasher_args)

        db = SQLiteDatabase()
        db.execute('INSERT INTO user (user_name, user_password, user_role) VALUES (?, ?, ?);', (username, ph.hash(password), role.value))

        return User.create_from_username(username)


    def __init__(self, uid: int, username: str, password_hash: str, role: Role) -> None:
        """
        This function creates a User object.

        :param uid: Uid of the user
        :type uid: int
        :param username: Username of the user
        :type username: str
        :param password_hash: Password hash of the user
        :type password_hash: str
        :param role: Role of the user
        :type role: Role
        :return: None
        :rtype: None
        """
        self.__uid = uid
        self.__username = username
        self.__password_hash = password_hash
        self.__role = role

        self.__shadow_username = None
        self.__shadow_password_hash = None
        self.__shadow_role = None

        self.__ph = PasswordHasher(**password_hasher_args)


    def persist_changes(self) -> bool:
        """
        This function persists the changes made to the user object in the database.

        :return: True if actual changes were persisted. False otherwise.
        :rtype: bool
        """
        if self.__shadow_username == self.__username:
            self.__shadow_username = None

        if self.__shadow_password_hash == self.__password_hash:
            self.__shadow_password_hash = None

        if self.__shadow_role == self.__role:
            self.__shadow_role = None

        if self.__shadow_username == None and self.__shadow_password_hash == None and self.__shadow_role == None:
            return False

        if self.__shadow_username != None:
            if User.does_username_exist(self.__shadow_username):
                raise ValueError(f'The user {self.__shadow_username} does already exist. No shadow values / chnages were persisted.')

        query = 'UPDATE user SET '
        args = list()

        if self.__shadow_username != None:
            query = query + 'user_name = ?, '
            args.append(self.__shadow_username)
        if self.__shadow_password_hash != None:
            query = query + 'user_password = ?, '
            args.append(self.__shadow_password_hash)
        if self.__shadow_role != None:
            query = query + 'user_role = ?, '
            args.append(self.__shadow_role.value)

        # remove prepended comma and space from last column update
        query = query[:-2]

        # restrict query to correct uid
        query = query + ' WHERE user_id = ?;'
        args.append(self.__uid)

        db = SQLiteDatabase()
        db.execute(query, tuple(args))

        if self.__shadow_username != None:
            self.__username = self.__shadow_username
            self.__shadow_username = None
        if self.__shadow_password_hash != None:
            self.__password_hash = self.__shadow_password_hash
            self.__shadow_password_hash = None
        if self.__shadow_role != None:
            self.__role = self.__shadow_role
            self.__shadow_role = None

        return True


    def uid(self) -> int:
        """
        This function returns the uid of the user.

        :return: Uid of the user
        :rtype: int
        """
        return self.__uid


    def username(self, new_username: str = None) -> str:
        """
        This function returns the username of the user.

        :param new_username: New username to set
        :type new_username: str
        :return: Username of the user
        :rtype: str
        """
        if new_username != None:
            self.__shadow_username = new_username

        return self.__username


    def check_password(self, password: str) -> bool:
        """
        This function checks if the given password matches the password hash of the user.

        :param password: Password to check
        :type password: str
        :return: True if the password matches, False otherwise
        :rtype: bool
        """
        result = self.__ph.verify(self.__password_hash, password)

        if result and self.__ph.check_needs_rehash(self.__password_hash):
            self.__set_password(password, False)

        return result


    def __set_password(self, password: str, as_shadow: bool = True) -> None:
        """
        This function sets the password hash of the user.

        :param password: Password to set
        :type password: str
        :param as_shadow: If True, the password hash will be set as shadow password hash, otherwise it will be set as password hash
        :type as_shadow: bool
        :return: None
        :rtype: None
        """
        hash = self.__ph.hash(password)

        if as_shadow:
            self.__shadow_password_hash = hash
        else:
            db = SQLiteDatabase()
            db.execute('UPDATE user SET user_password = ? WHERE user_id = ?;', (hash, self.__uid))
            self.__password_hash = hash


    def set_password(self, password: str) -> None:
        """
        This function sets the password hash of the user.

        :param password: Password to set
        :type password: str
        :return: None
        :rtype: None
        """
        self.__set_password(password, True)


    def role(self, new_role: Role = None) -> Role:
        """
        This function returns the role of the user.

        :param new_role: New role to set
        :type new_role: Role
        :return: Role of the user
        :rtype: Role
        """
        if new_role != None:
            self.__shadow_role = new_role

        return self.__role


    def delete(self) -> None:
        db = SQLiteDatabase()
        db.execute('DELETE FROM user WHERE user_id = ?;', (self.__uid,))


class JWT():
    """
    This class represents a JWT token.
    """
    ISSUER_KEY = 'iss'
    EXPIRE_KEY = 'exp'
    SUBJECT_KEY = 'sub'

    ISSUER_NAME = 'jk'

    def __init__(self, jwt_token: str, secret: str, algorithm: str = 'HS256') -> None:
        """
        This function creates a JWT object.

        :param jwt_token: JWT token
        :type jwt_token: str
        :param secret: Secret to decode the JWT token
        :type secret: str
        :param algorithm: Algorithm to use for decoding the JWT token
        :type algorithm: str
        :return: None
        :rtype: None
        """
        self.__jwt_token = jwt_token
        payload = jwt.decode(jwt_token, secret, algorithm)

        if not JWT.ISSUER_KEY in payload.keys():
            raise KeyError(f'Invalid JWT token: {JWT.ISSUER_KEY} key is missing.')
        if not JWT.EXPIRE_KEY in payload.keys():
            raise KeyError(f'Invalid JWT token: {JWT.EXPIRE_KEY} key is missing.')
        if not JWT.SUBJECT_KEY in payload.keys():
            raise KeyError(f'Invalid JWT token: {JWT.SUBJECT_KEY} key is missing.')

        self.__issuer = payload.get(JWT.ISSUER_KEY)

        expire = -1
        try:
            expire = int(payload.get(JWT.EXPIRE_KEY))
            if expire < 0:
                raise ValueError()
        except ValueError:
            raise ValueError(f'Invalid JWT token: {JWT.EXPIRE_KEY} value is invalid.')
        self.__expire = Time.from_timestamp(expire)

        subject = payload.get(JWT.SUBJECT_KEY)
        if not User.is_valid_subject(subject):
            raise ValueError(f'Invalid JWT token: {JWT.SUBJECT_KEY} value is invalid.')
        self.__subject = subject

        self.__cache_user = None


    def is_expired(self) -> bool:
        """
        This function checks if the JWT token is expired.

        :return: True if the JWT token is expired, False otherwise
        :rtype: bool
        """
        now = Time()
        return now.compare(self.__expire) != -1


    def is_valid(self) -> bool:
        """
        This function checks if the JWT token is valid.

        :return: True if the JWT token is valid, False otherwise
        :rtype: bool
        """
        # reset cached user
        self.__cache_user = None

        if self.__issuer != JWT.ISSUER_NAME:
            return False

        if self.is_expired():
            return False

        (uid, username) = User.split_subject(self.__subject)
        try:
            self.__cache_user = User.create_from_uid(uid)
        except:
            self.__cache_user = None

        if self.__cache_user == None:
            return False

        return username == self.__cache_user.username()


    def get_user(self) -> User:
        """
        This function returns the user object of the JWT token.

        :return: User object if the JWT token is valid, None otherwise
        :rtype: User | None
        """
        return self.__cache_user


    def get_jwt_token(self) -> str:
        return self.__jwt_token


class JWTCreator():
    def __init__(self, secret: str, algorithm: str = 'HS256') -> None:
        self.__secret = secret
        self.__algorithm = algorithm


    def create_token_from_subject(self, subject: str, max_age = MAX_AGE) -> JWT:
        expire_time = Time()
        expire_time.add_seconds(max_age)

        jwt_str = jwt.encode({JWT.ISSUER_KEY: JWT.ISSUER_NAME, JWT.EXPIRE_KEY: expire_time.to_timestamp(), JWT.SUBJECT_KEY: subject}, self.__secret, self.__algorithm)

        return JWT(jwt_str, self.__secret, self.__algorithm)


    def create_token_from_uid_username(self, uid: int, username: str, max_age = MAX_AGE) -> JWT:
        return self.create_token_from_subject(User.create_subject(str(uid), username), max_age)
