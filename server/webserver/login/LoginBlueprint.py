"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""
# Standard library imports
from enum import Enum

# Flask imports
from flask import Blueprint
from flask import render_template
from flask import request
from flask import redirect
from flask import make_response
from flask import jsonify

# Project imports
from config import SECRET_KEY
from login.LoginForms import LoginForm, UserEditForm, AdminEditUserForm, AdminUserQuery
from login.UserCore import User, Role, JWT, JWTCreator, MAX_AGE


login_blueprint = Blueprint('login', __name__, template_folder='templates')
jwt_creator = JWTCreator(SECRET_KEY)


class SpecialUID(Enum):
    NOOP = 0
    CREATE = -1


@login_blueprint.route('/login', methods = ['GET', 'POST'])
def login():
    """
    This function handles the login page.

    GET: Renders the login page.
    POST: Checks the login credentials and sets the cookie.

    :return: The rendered login page or an error message.
    :rtype: str
    """
    if is_logged_in():
        return redirect('/', code = 303)

    template_params = {
            'notification': '',
            'notification_type': 'none',
            'user': '',
            'password': ''
        }

    if request.method == 'POST':
        # handle user login
        form = LoginForm(request.form)

        if not form.validate():
            template_params['notification'] = '\n'.join(form.username.errors + form.password.errors)
            template_params['notification_type'] = 'error'
            return render_template('login.html', **template_params)

        user_obj = None
        password_str = form.password.data

        try:
            user_obj = User.create_from_username(form.username.data)
        except Exception as ex:
            template_params['notification'] = 'Username does not exist.'
            template_params['notification_type'] = 'error'
            return render_template('login.html', **template_params)

        try:
            if not user_obj.check_password(password_str):
                raise Exception('Invalid password.')
        except:
            template_params['notification'] = 'Password is incorrect.'
            template_params['notification_type'] = 'error'
            template_params['user'] = user_obj.username()
            template_params['password'] = password_str
            return render_template('login.html', **template_params)

        jwt = jwt_creator.create_token_from_uid_username(user_obj.uid(), user_obj.username())
        jwt_str = jwt.get_jwt_token()

        response = make_response(redirect('/', code = 303))
        response.set_cookie('User', jwt_str, max_age = MAX_AGE)
        return response

    elif request.method == 'GET':
        # display login page
        return render_template('login.html', **template_params)


@login_blueprint.route('/user', methods = ['GET', 'POST'])
def user():
    """
    This function handles the user page.

    GET: Renders the user page.
    POST: Changes the password of the user.

    :return: The rendered user page or an error message.
    :rtype: str
    """
    user_obj = get_current_login()

    if not user_obj:
        return 'You are not logged in.', 403

    template_params = {
            'notification': '',
            'notification_type': 'none',

            # user data fields
            'username': user_obj.username(),
            'role': user_obj.role().value.capitalize(),
            'password_current': '',
            'password_future': '',
            'password_future_confirm': ''
        }

    if request.method == 'POST':
        # handle user data change
        form = UserEditForm(request.form)

        # keep entered data in form
        template_params['password_future'] = form.password_future.data
        template_params['password_future_confirm'] = form.password_future_confirm.data

        if not form.validate():
            template_params['notification'] = '\n'.join(form.password_current.errors + form.password_future.errors)
            template_params['notification_type'] = 'error'
            return render_template('user.html', **template_params)

        # perform password change if necessary
        password_current_str = form.password_current.data
        password_future_str = form.password_future.data

        if password_current_str and password_future_str:
            try:
                if not user_obj.check_password(password_current_str):
                    raise Exception('Invalid password.')
            except:
                template_params['notification'] = 'The current password was wrong.'
                template_params['notification_type'] = 'error'
                return render_template('user.html', **template_params)

            user_obj.set_password(password_future_str)
            user_obj.persist_changes()
            template_params['notification'] = 'The new password was set.'
            template_params['notification_type'] = 'success'
            template_params['password_future'] = ''
            template_params['password_future_confirm'] = ''

        return render_template('user.html', **template_params)

    elif request.method == 'GET':
        # display user page
        return render_template('user.html', **template_params)


@login_blueprint.route('/logout', methods = ['POST'])
def logout():
    """
    This function handles the logout process.

    POST: User confirms logout

    :return: Rendered template or redirect
    :rtype: str
    """
    if not is_logged_in():
        return redirect('/', code = 303)

    # user is logged out
    response = make_response(redirect('/', code = 303))
    response.set_cookie('User', '', max_age = 0)
    return response


@login_blueprint.route('/admin', methods = ['GET', 'POST'])
def admin():
    """
    Admin interface for user management

    GET: Render admin interface
    POST: Handle user management requests

    :return: Rendered template or error message
    :rtype: str
    """
    admin_obj = get_current_login()

    if not admin_obj:
        return 'You are not logged in.', 403

    if admin_obj.role() != Role.ADMIN:
        return 'You do not have admin rights.', 403

    template_params = {
        'notification': '',
        'notification_type': 'none',

        # user settings
        'edit_user_id': '',
        'edit_user_name': '',
        'edit_user_role': '',
        'edit_user_password': ''
    }

    if request.method == 'POST':
        # process settings change
        form = AdminEditUserForm(request.form)

        template_params['edit_user_id'] = form.uid.data
        template_params['edit_user_name'] = form.username.data
        template_params['edit_user_role'] = form.role.data
        template_params['edit_user_password'] = form.password.data

        if not form.validate():
            if form.uid.data == None:
                template_params['edit_user_id'] = 0
            template_params['notification'] = '\n'.join(form.uid.errors + form.username.errors + form.role.errors + form.password.errors + form.delete.errors)
            template_params['notification_type'] = 'error'
            return render_template('admin.html', **template_params)

        user_name_str =  form.username.data
        user_role_enm = Role(form.role.data)
        user_password_str = form.password.data

        if form.uid.data == SpecialUID.NOOP.value:
            template_params['notification'] = 'User ID must be present to change a user.'
            template_params['notification_type'] = 'error'
            return render_template('admin.html', **template_params)

        elif form.uid.data == SpecialUID.CREATE.value:
            # create new user
            if not form.password.data:
                template_params['notification'] = 'The creation of a new user requires a password.'
                template_params['notification_type'] = 'error'
                return render_template('admin.html', **template_params)

            try:
                User.add_new_user(user_name_str, user_password_str, user_role_enm)
            except:
                template_params['notification'] = 'The new user could not be created.'
                template_params['notification_type'] = 'error'
                return render_template('admin.html', **template_params)

            template_params['notification'] = 'The new user was created.'
            template_params['notification_type'] = 'success'
            return render_template('admin.html', **template_params)

        user_obj = None

        try:
            user_obj = User.create_from_uid(form.uid.data)
        except:
            template_params['notification'] = 'The user ID does not exist.'
            template_params['notification_type'] = 'error'
            return render_template('admin.html', **template_params)

        if admin_obj.uid() == user_obj.uid():
            template_params['notification'] = 'Admin users cannot edit their own accounts.'
            template_params['notification_type'] = 'error'
            return render_template('admin.html', **template_params)

        if form.delete.data == 'Delete':
            template_params['edit_user_id'] = 0
            template_params['edit_user_name'] = ''
            template_params['edit_user_role'] = ''
            template_params['edit_user_password'] = ''

            template_params['notification'] = f'Successfully deleted user {user_obj.username()}.'
            template_params['notification_type'] = 'success'

            try:
                user_obj.delete()
            except:
                template_params['notification'] = f'Failed to delete user {user_obj.username()}.'
                template_params['notification_type'] = 'error'

            return render_template('admin.html', **template_params)

        user_obj.username(user_name_str)
        user_obj.role(user_role_enm)

        if user_password_str:
            user_obj.set_password(user_password_str)

        try:
            if user_obj.persist_changes():
                # successfully applied changes
                template_params['notification'] = 'All changes were applied.'
            else:
                # no changes detected
                template_params['notification'] = 'No changes detected. Request changed nothing.'

            template_params['notification_type'] = 'success'
            template_params['edit_user_password'] = ''

        except:
            # persisting changes failed
            template_params['notification'] = 'An internal error occured when persisting changes.'
            template_params['notification_type'] = 'error'

        return render_template('admin.html', **template_params)

    elif request.method == 'GET':
        # display settings page
        return render_template('admin.html', **template_params)


@login_blueprint.route('/admin/query', methods = ['POST'])
def admin_query():
    admin_obj = get_current_login()

    if not admin_obj:
        return 'You are not logged in.', 403

    if admin_obj.role() != Role.ADMIN:
        return 'You do not have admin rights.', 403

    form = AdminUserQuery(request.form)

    if not form.validate():
        return 'Invalid request.', 400

    user_obj = None

    try:
        user_obj = User.create_from_uid(form.uid.data)
    except:
        return 'Invalid request.', 400

    return jsonify({'uid': user_obj.uid(), 'username': user_obj.username(), 'role': user_obj.role().value})


def get_current_login() -> User:
    cookie = request.cookies.get('User')

    if not cookie:
        return None

    jwt = None

    try: jwt = JWT(cookie, SECRET_KEY)
    except: return None

    if not jwt.is_valid():
        return None

    return jwt.get_user()


def is_logged_in() -> bool:
    user_obj = get_current_login()

    return user_obj != None


def is_logged_in_as(role: Role) -> bool:
    user_obj = get_current_login()

    if user_obj == None:
        return False

    return user_obj.role() == role
