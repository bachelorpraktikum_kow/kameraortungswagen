"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

from wtforms import Form, IntegerField, StringField, PasswordField, SelectField
from wtforms.validators import InputRequired, Optional, Length, Regexp, EqualTo, ValidationError, AnyOf, NumberRange

from login.UserCore import Role


class UnequalTo:
    """
    Compares the values of two fields.

    :param fieldname:
        The name of the other field to compare to.
    :param message:
        Error message to raise in case of a validation error. Can be
        interpolated with `%(other_label)s` and `%(other_name)s` to provide a
        more helpful error.
    """

    def __init__(self, fieldname, message = None):
        self.fieldname = fieldname
        self.message = message

    def __call__(self, form, field):
        try:
            other = form[self.fieldname]
        except KeyError as exc:
            raise ValidationError(
                field.gettext("Invalid field name '%s'.") % self.fieldname
            ) from exc
        if field.data != other.data:
            return

        d = {
            "other_label": hasattr(other, "label")
            and other.label.text
            or self.fieldname,
            "other_name": self.fieldname,
        }
        message = self.message
        if message is None:
            message = field.gettext("Field must be equal to %(other_name)s.")

        raise ValidationError(message % d)


roles = [(role.value, str(role.value).capitalize()) for role in Role]
roles.sort()


vldr_login_username = [
    InputRequired(message = 'Username is required.'),
    Length(min = 1, message = 'Username is too short.'),
    Regexp(r'^[0-9A-Za-z_]+$', message = 'Username contains invalid characters.')
]

vldr_login_password = [
    InputRequired(message = 'Password is required.'),
    Length(min = 1, message = 'Password is too short.'),
    Regexp(r'^[A-Za-z0-9!@#$%^&*()+\-_={}\[\];:\"\'\\|?/.,<>~]+$', message = 'Password contains invalid characters.')
]

vldr_user_edit_password_current = [
    Optional(strip_whitespace = True),
    Length(min = 1, message = 'Current password is too short.'),
    Regexp(r'^[A-Za-z0-9!@#$%^&*()+\-_={}\[\];:\"\'\\|?/.,<>~]+$', message = 'Current password contains invalid characters.')
]

vldr_user_edit_password_future = [
    Optional(strip_whitespace = True),
    Length(min = 6, max = 128, message = 'New password must be between 6 and 128 characters long.'),                           # at least 6 characters, at maximum 128 characters
    Regexp(r'^.*[a-z].*$', message = 'New password must contain at least one lower case character.'),                          # at least 1 lower-case letter
    Regexp(r'^.*[A-Z].*$', message = 'New password must contain at least one upper case character.'),                          # at least 1 upper-case letter
    Regexp(r'^.*[0-9].*$', message = 'New password must contain at least one digit.'),                                         # at least 1 digit
    Regexp(r'^[A-Za-z0-9!@#$%^&*()+\-_={}\[\];:\"\'\\|?/.,<>~]+$', message = 'Current password contains invalid characters.'), # only characters, numbers and standard special characters
                                                                                                                               # ! @ # $ % ^ & * ( ) + - _ = { } [ ] ; : " ' \ | ? / . , < > ~
    EqualTo('password_future_confirm', message = 'New password does not match with confirmation.'),
    UnequalTo('password_current', message = 'New password must not be equal to current password.')
]

vldr_user_edit_password_future_confirm = [
    Optional(strip_whitespace = True)
]

vldr_admin_edit_user_uid = [
    InputRequired(message = 'User ID must be present to change a user.'),
    NumberRange(min = -1)
]

vldr_admin_edit_user_username = [
    InputRequired(message = 'Username must be present to change a user.'),
    Length(min = 4, max = 16, message = 'Username must be between 4 and 16 characters long.'),
    Regexp(r'^[0-9A-Za-z_]+$', message = 'Username contains invalid characters.')
]

vldr_admin_edit_user_role = [
    InputRequired(message = 'Role must be present to change a user.'),
    AnyOf([role for (role, _) in roles], message = 'Role has an invalid value.')
]

vldr_admin_edit_user_password = [
    Optional(strip_whitespace = True),
    Length(min = 1, message = 'New password must be at least 1 character long.'),
    Regexp(r'^[A-Za-z0-9!@#$%^&*()+\-_={}\[\];:\"\'\\|?/.,<>~]+$', message = 'Password contains invalid characters.')
]

vldr_admin_edit_user_delete = [
    Optional(strip_whitespace = True),
    AnyOf(['Delete'], message = 'Delete confirmation is invalid.')
]

vldr_admin_query_user = [
    NumberRange(min = 1)
]


class LoginForm(Form):
    username = StringField(None, validators = vldr_login_username, filters = [str.strip])
    password = PasswordField(None, validators = vldr_login_password)


class UserEditForm(Form):
    password_current = PasswordField(None, validators = vldr_user_edit_password_current)
    password_future = PasswordField(None, validators = vldr_user_edit_password_future)
    password_future_confirm = PasswordField(None, validators = vldr_user_edit_password_future_confirm)


class AdminEditUserForm(Form):
    uid = IntegerField(None, validators = vldr_admin_edit_user_uid)
    username = StringField(None, validators = vldr_admin_edit_user_username, filters = [str.strip])
    role = SelectField(None, choices = roles, validate_choice = False, validators = vldr_admin_edit_user_role)
    password = PasswordField(None, validators = vldr_admin_edit_user_password)
    delete = PasswordField(None, validators = vldr_admin_edit_user_delete, filters = [str.strip])


class AdminUserQuery(Form):
    uid = IntegerField(None, validators = vldr_admin_query_user)
