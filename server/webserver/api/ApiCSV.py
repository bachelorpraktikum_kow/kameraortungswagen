"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

from api.ApiRounds import ApiRounds
from database.Database import Database

from flask import Response
import csv
import io
import time, sys

class ApiCSV(ApiRounds):

    def __init__(self, db) -> None:
        super().__init__(db)

    def request(self, data):
        """
        Retrieve the sensor_data information of a board from the database.

        :param data: board_id as a tuple of the board to retrieve the sensor_data information from and the number of rounds to be returned.
        :type data: tuple
        :return: A CSV containing the sensor information of the board, including steering, hall, tie, odometer, odometer_count, wifi strength, local and remote time and active status.
        :rtype: dict
        """

        sensor_data = self.fetch_rounds(data[0], data[1])

        csv_table = io.StringIO()
        csv_writer = csv.writer(csv_table, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)

        csv_writer.writerow(["board_time", "server_time", "steering", "odometer_speed", "tie", "fixpoint_hall", "fixpoint_odometer_steps", "fixpoint_tie_steps", "fixpoint_time", "wifi", "battery"]) # "id",
        for unformatted_entry in sensor_data:

            csv_writer.writerow([
                #unformatted_entry["id"],
                unformatted_entry["rtc"], 
                unformatted_entry["local_rtc"], 
                unformatted_entry["steering"],
                unformatted_entry["odometer"],
                unformatted_entry["tie"],
                unformatted_entry["hall"],
                unformatted_entry["fixpoint_odometer"] if unformatted_entry["fixpoint_odometer"] >= 0 else "n/a",
                unformatted_entry["fixpoint_tie"] if unformatted_entry["fixpoint_tie"] >= 0 else "n/a",
                unformatted_entry["fixpoint_time"] if unformatted_entry["fixpoint_time"] >= 0 else "n/a",
                unformatted_entry["wifi"],
                unformatted_entry["battery"]
            ])

        return Response(csv_table.getvalue(), mimetype='text/csv', headers={'Content-Disposition':'attachment;filename=datapoints-' + str(round(time.time())) + '-' + str(data[1]) + '.csv'})
