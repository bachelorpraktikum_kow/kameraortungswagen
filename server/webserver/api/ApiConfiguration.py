"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

from database.Database import Database
from api.Api import Api
class ApiConfiguration(Api):

    def __init__(self, db) -> None:
        super().__init__(db)


    def request(self, data):
        """
        Retrieve the configuration information of a board from the database.

        :param data: board_id as a tuple of the board to retrieve the configuration information from.
        :type data: tuple
        :return: A dictionary containing the configuration information of the board, including wifi, pins, and config. If no configuration information is found, an empty dictionary is returned.
        :rtype: dict
        """
        result = self._db.query("""
            SELECT 
            configuration_wifi_ssid wifi_ssid, 
            configuration_read_interval readInterval,
            configuration_wifi_ssid wifi_ssid,
            configuration_flashlight flashlight,
            configuration_camera camera,
            configuration_camera_quality camera_quality,
            configuration_camera_fps camera_fps
            FROM configuration 
            WHERE board_id = ?
            """, data)

        configuration_raw = Database.dict_factory(result)
        if len(configuration_raw) == 0:
            return {}
        
        configuration_raw = configuration_raw[0]
        configuration = {}
        wifi = {}
        wifi["ssid"] = configuration_raw["wifi_ssid"]
        configuration["wifi"] = wifi

        config = {}
        config["read_interval"] = configuration_raw["readInterval"]
        config["flashlight"] = configuration_raw["flashlight"]
        config["camera"] = configuration_raw["camera"]
        config["camera_quality"] = configuration_raw["camera_quality"]
        config["camera_fps"] = configuration_raw["camera_fps"]

        configuration["config"] = config

        return configuration