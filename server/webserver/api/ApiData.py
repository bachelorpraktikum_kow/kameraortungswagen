"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

from api.Api import Api
from database.Database import Database
class ApiData(Api):

    def __init__(self, db) -> None:
        super().__init__(db)

    def request(self, data):
        """
        Retrieve the sensor_data information of a board from the database.

        :param data: board_id as a tuple of the board to retrieve the sensor_data information from and the number of rows to be returned.
        :type data: tuple
        :return: A dictionary containing the sensor information of the board, including steering, hall, tie, odometer, odometer_count, wifi strength, local and remote time and active status.
        :rtype: dict
        """
        # Board active? (timeout = 30 seconds)
        timeout = 30

        result = self._db.query("""
        SELECT sensor_data_steering steering, sensor_data_hall hall, sensor_data_tie tie, sensor_data_tie_count fixpoint_tie, sensor_data_odometer odometer, sensor_data_odometer_count fixpoint_odometer, sensor_data_wifistrength wifi, sensor_data_rtc rtc, sensor_data_battery battery, sensor_data_local_rtc local_rtc, 
        (SELECT COUNT(*) > 0 FROM sensor_data WHERE datetime(sensor_data_local_rtc / 1000, 'unixepoch', 'localtime') > datetime('now', '-""" + str(timeout) + """ seconds', 'localtime') AND board_id = ?1) active,
        
        (sensor_data_rtc - (SELECT sensor_data_rtc FROM sensor_data WHERE board_id = ?1 AND sensor_data_hall = '1' ORDER BY sensor_data.sensor_data_id DESC)) fixpoint_time
        
        FROM sensor_data 
        WHERE board_id = ?1
        ORDER BY sensor_data.sensor_data_id DESC 
        LIMIT ?2;
        """, data)
        sensor_data = Database.dict_factory(result)

        # If no data is available, return an empty list
        if len(sensor_data) == 0:
            return []

        # Exclude the first entry in the fixpoint calculation
        first = sensor_data[0].copy()
        # if more than one entry is requested, calculate the fixpoint for all values except the first one
        if data[1] > 1:
            sensor_data = self.fixpoint(sensor_data)
        # Replace the first entry with the original one
        sensor_data[0] = first

        # Format the data
        formatted_data = []
        for unformatted_entry in sensor_data:
            formatted_entry = {
                "active": unformatted_entry["active"],
                "steering": unformatted_entry["steering"],
                "odometer": unformatted_entry["odometer"],
                "tie": unformatted_entry["tie"],
                "fixpoint": {
                    "hall": unformatted_entry["hall"],
                    "odometer": unformatted_entry["fixpoint_odometer"] if unformatted_entry["fixpoint_odometer"] is not None and unformatted_entry["fixpoint_odometer"] >= 0 else "n/a",
                    "tie": unformatted_entry["fixpoint_tie"] if unformatted_entry["fixpoint_tie"] is not None and unformatted_entry["fixpoint_tie"] >= 0 else "n/a",
                    "time": unformatted_entry["fixpoint_time"] if unformatted_entry["fixpoint_time"] is not None and unformatted_entry["fixpoint_time"] >= 0 else "n/a"
                },
                "wifi": unformatted_entry["wifi"],
                "rtc": {
                    "board": unformatted_entry["rtc"],
                    "server": unformatted_entry["local_rtc"],
                },
                "battery": unformatted_entry["battery"]
            }
            formatted_data.append(formatted_entry)

        # return the formatted data
        return formatted_data


    def fixpoint(self, round_result):
        result = []
        # reverse the round_result
        round_result.reverse()
        last_hall = {}
        for entry in round_result:
            if entry["hall"] == 1:               
                # set last hall to current entry
                last_hall = entry
            
            # calculate fixpoint values
            entry["fixpoint_time"] = self._calculate_fixpoint(entry, last_hall)

            # add entry to result
            result.insert(0, entry)
    
        return result


    def _calculate_fixpoint(self, entry, last_hall, reset_time = 30 * 60 * 1000):
        if last_hall == {}:
            return -1

        #fixpoint_odometer = entry["odometerCount"] - last_hall["odometerCount"]
        fixpoint_time = entry["rtc"] - last_hall["rtc"]

        # if the fixpoint time is greater than 30 minutes the board was probably reset
        if fixpoint_time > reset_time:
            fixpoint_time = -1

        return fixpoint_time
        #return fixpoint_odometer, fixpoint_time