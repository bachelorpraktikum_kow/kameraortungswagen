"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""
import socket, json
import xxhash

from api.Api import Api
from database.Database import Database
from login.UserCore import Role
from login.LoginBlueprint import is_logged_in_as


class ApiUpdate(Api):

    def __init__(self, db) -> None:
        super().__init__(db)


    def request(self, data):
        """
        Update the board using a configuration packet.

        :param data: list of the board_id and the requested function to be configure.
        :type data: list
        :param req: The request object
        :type req: flask.Request
        :return: A dictionary containing a success or error message
        :rtype: dict
        """

        # Authenticate if configuration packet is privileged 
        if not is_logged_in_as(Role.USER) and not is_logged_in_as(Role.ADMIN):
            return {"success": False, "msg": "Error: Not logged in!"}

        # retrieve current configuration
        board_id = data[0]
        requested_updates = data[1]

        result = self._db.query("""
            SELECT 
            configuration_wifi_ssid wifi_ssid, 
            configuration_read_interval readInterval, 
            configuration_flashlight flashlight,
            configuration_camera camera,
            configuration_camera_quality quality,
            configuration_camera_fps fps,
            board.board_name name,  
            board.board_ip ip
            FROM configuration 
            INNER JOIN board ON board.board_id = configuration.board_id
            WHERE configuration.board_id = ?
            """, (board_id,))

        currrent_configuration = Database.dict_factory(result)
        if len(currrent_configuration) == 0:
            return {}
        currrent_configuration = currrent_configuration[0]

        # set current configuration
        ip = currrent_configuration["ip"]
        esp_name = currrent_configuration["name"]
        readInterval = currrent_configuration["readInterval"]
        flashlight = currrent_configuration["flashlight"]
        camera = currrent_configuration["camera"]
        quality = currrent_configuration["quality"]
        fps = currrent_configuration["fps"]

        round_reset = 0
        restart = 0

        # update configuration
        if "readInterval" in requested_updates.keys():
            if int(requested_updates["readInterval"]) >= 0:
                readInterval = int(requested_updates["readInterval"])
            else:
                return {"success": False, "msg": "Error: Invalid read interval!"}

        if "flashlight" in requested_updates.keys():
            if int(requested_updates["flashlight"]) >= 0 and int(requested_updates["flashlight"]) <= 10:
                flashlight = int(requested_updates["flashlight"])
            else:
                return {"success": False, "msg": "Error: Invalid flashlight brightness!"}
        if "camera" in requested_updates.keys():
            if int(requested_updates["camera"]) == 0:
                camera = 0
            else:
                camera = 1

        if "quality" in requested_updates.keys():
            valid_quality = ["UXGA", "SXGA", "XGA", "SVGA", "VGA", "CIF", "QQVGA"]
            if requested_updates["quality"] in valid_quality:
                quality = requested_updates["quality"]
            else:
                return {"success": False, "msg": "Error: Invalid camera quality!"}

        if "fps" in requested_updates.keys():
            valid_fps = [1, 5, 10]
            if int(requested_updates["fps"]) in valid_fps:
                fps = requested_updates["fps"]
            else:
                return {"success": False, "msg": "Error: Invalid framerate (fps)!"}

        if "round" in requested_updates.keys():
            round_reset = 1

        if "restart" in requested_updates.keys():
            restart = 1

        # build configuration packet
        configuration_packet = {
            "name": esp_name,
            "type": "configuration",
            "config": {
                "readInterval": int(readInterval),
                "flashlight": int(flashlight),
                "camera": int(camera),
                "cameraQuality": quality,
                "cameraFps": int(fps),
                "round": int(round_reset),
                "restart": int(restart)
            }
        }

        encoded_configuration_packet = json.dumps(configuration_packet)
        integrity = xxhash.xxh32_hexdigest(encoded_configuration_packet.encode('utf-8'))
        packet = integrity + ":" + encoded_configuration_packet

        # send packet to esp
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(packet.encode("utf-8"), (ip, 4242))

        return {"success": True, "msg": "Success: Configuration packet sent!", "configuration_packet": encoded_configuration_packet}






        