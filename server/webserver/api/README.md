# API Documentation v0.1-alpha

## Endpoints
### [GET] /api/boards
Returns all boards.

### [GET] /api/data/<board_id>/<count>
Returns the last <count> sensor data from the database.

#### Parameters
| Parameter          | Description                                             | Sample |
| ------------------ | ------------------------------------------------------- | ------ |
| <board_id>         | Board id of the requested board, as seen in /api/boards | 1      |
| <count> (optional) | Number of database entries that should be returned      | 10     |

#### Sample Request
```
/api/data/1/2
```

#### Sample Response
```json
[
  {
    "active": 0,
    "fixpoint": {
      "hall": 1,
      "odometer": 0,
      "time": 0
    },
    "odometer": {
      "speed": 146.24742,
      "steps": 2656
    },
    "rtc": {
      "board": 1674425891,
      "server": 1674425891
    },
    "steering": 10,
    "tie": 114.61294,
    "wifi": -44.52
  },
  {
    "active": 0,
    "fixpoint": {
      "hall": 1,
      "odometer": "n/a",
      "time": "n/a"
    },
    "odometer": {
      "speed": 106.35129,
      "steps": 1248
    },
    "rtc": {
      "board": 1674425881,
      "server": 1674425881
    },
    "steering": 29,
    "tie": 158.69638,
    "wifi": -66.23
  }
]
```

### [GET] /api/configuration/<board_id>
Returns current configuration of the board.

#### Parameters
| Parameter  | Description                                             | Sample |
| ---------- | ------------------------------------------------------- | ------ |
| <board_id> | Board id of the requested board, as seen in /api/boards | 1      |

#### Sample Request
```
/api/data/1
```

#### Sample Response
```json
{
    "wifi": {
        "ssid": "<ssid>"
    },
    "pins": {
        "steering": <steering-pin>,
        "hall": <hall-pin>,
        "tie": <tie-pin>,
        "odometer": <odometer-pin>
    },
    "config": {
        "readInterval": <read-interval>
    }
}

```

### [POST] /api/update
Sets the configuration of the board.

*TODO*
