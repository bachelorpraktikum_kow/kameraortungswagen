"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

# Flask imports
from flask import Blueprint
from flask import request
from flask import jsonify

# Project imports
from api.ApiRounds import ApiRounds
from api.ApiData import ApiData
from api.ApiCSV import ApiCSV
from api.ApiConfiguration import ApiConfiguration
from api.ApiBoards import ApiBoards
from api.ApiUpdate import ApiUpdate
from api.ApiSteering import ApiSteering

from database.SQLiteDatabase import SQLiteDatabase


api_blueprint = Blueprint('api', __name__, template_folder='templates')

@api_blueprint.route('/api/data/<int:board_id>/')
@api_blueprint.route('/api/data/<int:board_id>/<int:count>/')
def api_data(board_id, count = 1):
    """
    This function is a GET route handler for the '/api/data/int:board_id/int:count' endpoint.
    It takes two arguments, an int 'board_id' passed in through the URL and an optional int 'count' (default: 1) passed in through the URL.

    It prints a JSON response to this request method call which is the processed data from the sensor_data in the database.

    :param board_id: Board id of the request passed through the URL
    :type board_id: int
    :param count: Number of data packets to be returned from the database (default: 1)
    :type count: int
    :return: str
    :rtype: JSON-Encoded sensor data from the database
    """
    db = SQLiteDatabase()
    api = ApiData(db)
    data = api.request((board_id, count))
    return jsonify(data)


@api_blueprint.route('/api/rounds/<int:board_id>/')
@api_blueprint.route('/api/rounds/<int:board_id>/<int:rounds>/')
def api_rounds(board_id, rounds = 5):
    """
    This function is a GET route handler for the '/api/rounds/int:board_id/int:rounds' endpoint.
    It takes two arguments, an int 'board_id' passed in through the URL and an optional int 'rounds' (default: 5) passed in through the URL.

    It outputs a JSON encoded response to this request method call which is the processed data from the sensor_data in the database containing all entries of the last 'rounds' rounds.

    :param board_id: Board id of the request passed through the URL
    :type board_id: int
    :param rounds: Number of rounds to be returned from the database (default: 5)
    :type rounds: int
    :return: str
    :rtype: JSON-Encoded sensor data from the database
    """
    db = SQLiteDatabase()
    api = ApiRounds(db)
    data = api.request((board_id, rounds))
    return jsonify(data)

@api_blueprint.route('/api/csv/<int:board_id>/')
@api_blueprint.route('/api/csv/<int:board_id>/<int:rounds>/')
def api_csv(board_id, rounds = 5):
    """
    This function is a GET route handler for the '/api/csv/int:board_id/int:rounds' endpoint.
    It takes two arguments, an int 'board_id' passed in through the URL and an optional int 'rounds' (default: 5) passed in through the URL.

    It downloads a CSV file to this request method call which is the processed data from the sensor_data in the database.

    :param board_id: Board id of the request passed through the URL
    :type board_id: int
    :param rounds: Number of rounds to be returned from the database (default: 5)
    :type rounds: int
    :return: str
    :rtype: CSV download response
    """
    db = SQLiteDatabase()
    api = ApiCSV(db)
    data = api.request((board_id, rounds))
    return data

@api_blueprint.route('/api/configuration/<int:board_id>/')
def api_configuration(board_id):
    """
    This function is a GET route handler for the '/api/configuration/int:board_id' endpoint.
    It takes one argument, an int 'board_id' passed in through the URL.

    It prints a JSON response to this request method call which is the processed data from the configuration in the database.

    :param board_id: Board id of the request passed through the URL
    :type board_id: int
    :return: str
    :rtype: JSON-Encoded configuration data from the database
    """
    db = SQLiteDatabase()
    api = ApiConfiguration(db)
    data = api.request((board_id,))
    return jsonify(data)

@api_blueprint.route('/api/update/<int:board_id>/', methods=["POST"])
def api_update(board_id):
    """
    This function is a POST route handler for the '/api/update/int:board_id' endpoint.
    It takes one argument, an int 'board_id' passed in through the URL as well a JSON encoded POST-request body.

    :param board_id: Board id of the request passed through the URL
    :type board_id: int
    :return: str
    :rtype: JSON-Encoded configuration data from the database
    """
    content = request.json

    db = SQLiteDatabase()
    api = ApiUpdate(db)
    data = api.request([board_id, content])
    return jsonify(data)

@api_blueprint.route('/api/steering/<int:board_id>/', methods=["GET", "POST"])
def api_steering(board_id):
    """
    This function is a POST route handler for the '/api/steering/int:board_id' endpoint.
    It takes one argument, an int 'board_id' passed in through the URL as well a JSON encoded POST-request body.

    :param board_id: Board id of the request passed through the URL
    :type board_id: int
    :return: str
    :rtype: JSON-Encoded configuration data from the database
    """
    if request.method == "GET":
        db = SQLiteDatabase()
        api = ApiSteering(db)
        data = api.request(board_id)
        return jsonify(data) # data is already a JSON response
    
    content = request.json

    db = SQLiteDatabase()
    api = ApiSteering(db)
    data = api.request([board_id, content])
    return jsonify(data) # data is already a JSON response


@api_blueprint.route('/api/boards/')
def api_boards():
    """
    This function is a GET route handler for the '/api/boards' endpoint.
    The response of the request is the processed data about the available boards from the database.

    :return: str
    :rtype: JSON-Encoded boards list from the database
    """
    db = SQLiteDatabase()
    api = ApiBoards(db)
    data = api.request(())
    return jsonify(data)