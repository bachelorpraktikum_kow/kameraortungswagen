"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""
class Api:

    def __init__(self, db) -> None:
        """
        Initializes the API with a database.

        :param db: Database to be used.
        """
        self._db = db

    def request(self, data):
        pass

    @staticmethod
    def dict_factory(rows):
        """
        Converts a sqlite row list to a dict.
        :param rows: sqlite row list
        :returns: A dict with named keys containing the same data.
        """
        d = []
        for row in rows:
            r = dict(zip(row.keys(), row))
            d.append(r)
        return d