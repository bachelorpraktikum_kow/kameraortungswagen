"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

from api.ApiData import ApiData
from database.Database import Database
class ApiRounds(ApiData):

    def __init__(self, db) -> None:
        super().__init__(db)

    def request(self, data):
        """
        Retrieve the sensor_data information of a board from the database.

        :param data: board_id as a tuple of the board to retrieve the sensor_data information from and the number of rounds to be returned.
        :type data: tuple
        :return: A dictionary containing the sensor information of the board, including steering, hall, tie, odometer, odometer_count, wifi strength, local and remote time and active status.
        :rtype: dict
        """
        sensor_data = self.fetch_rounds(data[0], data[1])

        formatted_data = []
        for unformatted_entry in sensor_data:
            formatted_entry = {
                "active": unformatted_entry["active"],
                "steering": unformatted_entry["steering"],
                "odometer": unformatted_entry["odometer"],
                "tie": unformatted_entry["tie"],
                "fixpoint": {
                    "hall": unformatted_entry["hall"],
                    "odometer": unformatted_entry["fixpoint_odometer"] if unformatted_entry["fixpoint_odometer"] >= 0 else "n/a",
                    "tie": unformatted_entry["fixpoint_tie"] if unformatted_entry["fixpoint_tie"] >= 0 else "n/a",
                    "time": unformatted_entry["fixpoint_time"] if unformatted_entry["fixpoint_time"] >= 0 else "n/a"
                },
                "wifi": unformatted_entry["wifi"],
                "battery": unformatted_entry["battery"],
                "rtc": {
                    "board": unformatted_entry["rtc"],
                    "server": unformatted_entry["local_rtc"],
                }
            
            }
            formatted_data.append(formatted_entry)

        return formatted_data

    
    def fetch_rounds(self, board_id, round_count):
        """
        Fetches the sensor_data information of a board from the database.
        
        :param board_id: The id of the board to retrieve the sensor_data information from.
        :type board_id: int
        :param round_count: The number of rounds to be returned.
        :type round_count: int
        :return: A list of dictionaries containing the sensor information of the board, including steering, hall, tie, odometer, odometer_count, wifi strength, local and remote time and active status.
        :rtype: list
        """

        # fetch last hall sensor id
        args = (board_id, round_count)
        last_hall_sensor  = self._db.query("""
        SELECT MIN(s.id) id FROM 
            (
                SELECT sensor_data_id id FROM sensor_data 
                WHERE board_id = ?1 AND sensor_data_hall = '1' 
                ORDER BY sensor_data.sensor_data_id DESC 
                LIMIT ?2
            ) s;
        """, args)
        last_hall_sensor = Database.dict_factory(last_hall_sensor)

        # return all data if no hall sensor was found
        last_hall_sensor_id = -1
        if not last_hall_sensor[0]["id"] == None:
            last_hall_sensor_id = last_hall_sensor[0]["id"]

        timeout = 30

        # fetch data from database
        args = (board_id, last_hall_sensor_id)
        db_result = self._db.query("""
        SELECT 
        sensor_data_id id, 
        sensor_data_steering steering, 
        sensor_data_hall hall, 
        sensor_data_tie tie, 
        sensor_data_tie_count fixpoint_tie, 
        sensor_data_odometer odometer, 
        sensor_data_odometer_count fixpoint_odometer, 
        sensor_data_wifistrength wifi, 
        sensor_data_rtc rtc, 
        sensor_data_battery battery, 
        sensor_data_local_rtc local_rtc,
        (SELECT COUNT(*) > 0 FROM sensor_data WHERE datetime(sensor_data_local_rtc / 1000, 'unixepoch', 'localtime') > datetime('now', '-""" + str(timeout) + """ seconds', 'localtime') AND board_id = ?1) active
        FROM sensor_data 
        WHERE board_id = ?1 AND (sensor_data_id >= ?2 OR ?2 < 0)
        ORDER BY sensor_data.sensor_data_id DESC
        """, args)
        db_result = Database.dict_factory(db_result)

        if len(db_result) == 0:
            return []

        # calculate fixpoint values
        return self.fixpoint(db_result)