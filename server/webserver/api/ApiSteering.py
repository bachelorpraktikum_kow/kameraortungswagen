"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""
import json

from api.Api import Api
from database.Database import Database

from login.UserCore import Role
from login.LoginBlueprint import is_logged_in_as

class ApiSteering(Api):

    def __init__(self, db) -> None:
        super().__init__(db)


    def request(self, data):
        """
        Updates the steering calibration in the database.

        :param data: list of the board_id and the steering calibration or just the board_id
        :type data: list or int
        :return: A dictionary containing a success or error message
        :rtype: dict
        """

        if isinstance(data, int):
            result = self._db.query("SELECT configuration_steering_calibration FROM configuration WHERE board_id = ?", (data,))
            result = Database.dict_factory(result)
            if len(result) == 0:
                return []
            if result[0]["configuration_steering_calibration"] is None:
                return []

            return json.loads(result[0]["configuration_steering_calibration"])

        # Authenticate if configuration packet is privileged 
        if not is_logged_in_as(Role.USER) and not is_logged_in_as(Role.ADMIN):
            return {"success": False, "msg": "Error: Not logged in!"}

        # retrieve current configuration
        board_id = data[0]
        json_calibration = json.dumps(data[1])

        self._db.execute("""
            UPDATE configuration SET configuration_steering_calibration = ? WHERE board_id = ?
        """, (json_calibration, board_id))

        return {"success": True}






        