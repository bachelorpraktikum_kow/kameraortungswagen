"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

from database.Database import Database
from api.Api import Api
class ApiBoards(Api):

    def __init__(self, db) -> None:
        super().__init__(db)


    def request(self, data):
        """
        Retrieve information about all the boards registered in the database.
        The function returns a list of dictionaries, where each dictionary contains information about a single board.

        :param data: unused parameter
        :type data: any
        :return: list of dictionaries containing information about all the boards
        :rtype: list
        """

        result = self._db.query("""
            SELECT board_id id, board_ip ip, board_name name, 
            (SELECT sensor_data_local_rtc FROM sensor_data WHERE sensor_data.board_id = board.board_id ORDER BY sensor_data_local_rtc DESC LIMIT 1) updated, 
            (SELECT COUNT(*) > 0 FROM sensor_data WHERE datetime(sensor_data_local_rtc / 1000, 'unixepoch', 'localtime') > datetime('now', '-30 seconds', 'localtime') AND board.board_id = sensor_data.board_id) active
            FROM board
            """, ())
        boards = Database.dict_factory(result)
        if len(boards) == 0:
            return []
        return Database.dict_factory(result)