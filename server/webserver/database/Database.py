"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

class Database:

    def __init__(self) -> None:
        """
        Creates a connection to a database by establishing a connection to the database using the path to the database file.
        In case of an Error exception, it prints out the error message and exits the program.
        """
        pass

    def execute(self, query, params):
        """
        Executes a query on a database, and commits the changes.

        :param query: string that contains the parameterlized sql query
        :param params: tuple/list of parameters to be passed in the query
        :returns: the cursor object of the executed query, that allows to fetch the results, if there's any.
        """
        pass

    def query(self, query, params):
        """
        Executes a query on a database, and retrieve the results as a list of named tuples.

        :param query: string that contains the parameterlized sql query
        :param params: tuple/list of parameters to be passed in the query
        :returns: It returns the rows as a list of named tuples.
        """
        pass

    @staticmethod
    def dict_factory(rows):
        """
        Converts a sqlite row list to a dict.
        :param rows: sqlite row list
        :returns: A dict with named keys containing the same data.
        """
        d = []
        for row in rows:
            r = dict(zip(row.keys(), row))
            d.append(r)
        return d