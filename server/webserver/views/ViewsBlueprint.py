"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""
# Standard library imports

# Flask imports
from flask import Blueprint
from flask import render_template

# Project imports
from database.Database import Database
from database.SQLiteDatabase import SQLiteDatabase

views_blueprint = Blueprint('views', __name__, template_folder='templates')

@views_blueprint.route("/")
def home():
    """
    This function is a GET route handler for the root '/' endpoint. It renders the landing page for the webserver.

    :return: str
    :rtype: Render of the template with the placeholders set to their values
    """
    return render_template('landing.html')


@views_blueprint.route("/boards/")
def boards():
    """
    This function is a GET route handler for the '/boards' endpoint.
    It renders a HTML template 'boards.html' with the page information and boards data.

    :return: str
    :rtype: Render of the template with the placeholders set to their values
    """
    page = dict()
    page["title"] = "Boards"

    return render_template('boards.html', page=page)

@views_blueprint.route('/dashboard/<int:board_id>/')
def dashboard(board_id):
    """
    This function is a GET route handler for the '/dashboard/<int:board_id>' endpoint.
    It takes a board_id as an argument that is passed in through the URL, sets the page "title" to "Dashboard of " concatenated with the board_id.
    Then it renders the HTML template 'dashboard.html' with the page information.

    :param board_id: Board id of the request passed through the URL
    :type board_id: int
    :return: str
    :rtype: Render of the template with the placeholders set to their values
    """

    db = SQLiteDatabase()
    board_db = db.query("""SELECT board_id id, board_ip ip, board_name name FROM board WHERE board_id = ?""", (board_id,))
    board_db = Database.dict_factory(board_db)[0]

    page = dict()
    page["title"] = "Dashboard of Board \"" + board_db["name"] + "\" | " + str(board_id)
    page["board_name"] = board_db["name"] + " | " + board_db["ip"]
    board = {
        "id": board_id,
        "ip": board_db["ip"],
        "name": board_db["name"]
    }
    return render_template('dashboard.html', page=page, board = board)