"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""
# Standard library imports

# Flask imports
from flask import Flask
from flask_wtf import CSRFProtect

# Project imports
from login.LoginBlueprint import get_current_login, is_logged_in, is_logged_in_as
from login.UserCore import User, Role
from config import SECRET_KEY

# Blueprints
from api.ApiBlueprints import api_blueprint
from stream.StreamBlueprint import stream_blueprint
from views.ViewsBlueprint import views_blueprint
from login.LoginBlueprint import login_blueprint


# Initialize flask app
app = Flask(__name__, static_folder="static")
if SECRET_KEY == 'dev':
    app.logger.warning('Secret key for development used (change to random string in config.py)')

# Initialize csrf protection
app.config['SECRET_KEY'] = SECRET_KEY
csrf = CSRFProtect(app)

# Register Views Blueprint
app.register_blueprint(views_blueprint)

# Register API Blueprint
app.register_blueprint(api_blueprint)

# Register Stream Blueprint
app.register_blueprint(stream_blueprint)

# Register Login Blueprint
login_blueprint.before_request(csrf.protect)
app.register_blueprint(login_blueprint)
app.jinja_env.globals.update(Role = Role)
app.jinja_env.globals.update(get_current_login = get_current_login)
app.jinja_env.globals.update(is_logged_in = is_logged_in)
app.jinja_env.globals.update(is_logged_in_as = is_logged_in_as)
app.jinja_env.globals.update(get_all_users = User.get_all_users)

# run flask server on all interfaces and threaded
if __name__ == '__main__':
    app.run(host="0.0.0.0", threaded=True, debug=True)
