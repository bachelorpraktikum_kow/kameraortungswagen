"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""
# Standard library imports
import os
import time

# Flask imports
from flask import Blueprint
from flask import Response

# Project imports
from database.Database import Database
from database.SQLiteDatabase import SQLiteDatabase


stream_blueprint = Blueprint('stream', __name__, template_folder='templates')

@stream_blueprint.route("/stream/<int:board_id>/<force>/")
@stream_blueprint.route("/stream/<int:board_id>/")
def stream(board_id, force = None):  
    """
    This function is a GET route handler for the '/stream/<board_id>/' endpoint.
    The response of the request is a stream of images for a board with the given id.

    Using '/stream/<board_id>/force' will also return a stream of images for a board with the given id, but ignores the 30s check to validate the timeliness of the image.

    :param board_id: ID of the board for the image
    :type board_id: int
    :param force: string whether to use the 30s check or not.
    :type force: str
    :return: flask.Response
    :rtype: Image-Stream Generator with boundary 'frame'
    """

    db = SQLiteDatabase()

    boards = db.query("""SELECT board_ip ip FROM board WHERE board_id = ? LIMIT 1""", (board_id,))
    boards = Database.dict_factory(boards)

    if len(boards) == 0:
        board_ip = "None"
    else:
        board_ip = boards[0]["ip"]

    # TODO: Send caching header and/or reload image
    return Response(gather_img(board_ip, force == "force"), mimetype='multipart/x-mixed-replace; boundary=frame')


def gather_img(ip, force):
    """
    Creates a generator for a stream of images read from the folder ../image/files/ using the ip of a board. If the image is older than 30s or no image is available from the board, a placeholder image is streamed instead.
    The 30s check can be disabled through the boolean force parameter.

    :param ip: IP of the board for the image
    :type ip: str
    :param force: Boolean whether to use the 30s check or not.
    :type force: bool
    :return: generator
    :rtype: Image-Stream Generator with boundary 'frame'
    """

    while True:
        # TODO: sleeping -> lower framerate
        time.sleep(0.2)

        placeholder = os.path.abspath(os.path.realpath(__file__) + "/../../static/img/placeholder/placeholder.jpg")

        # get frame or placeholder if not exists
        frame_path = os.path.abspath(os.path.realpath(__file__) + "/../../../image/files/" + ip + ".jpg")
        if not os.path.exists(frame_path):
            frame_path = placeholder
        

        frame = None
        try:
            # check if file is older than 30s and set placeholder, except if force is set
            if not force and time.time() - os.path.getmtime(frame_path) > 30.0:
                frame_path = placeholder
            # output the image under frame_path
            with open(frame_path, mode="rb") as imagefile:
                frame = imagefile.read()
        except:
            # on error, display placeholder
            with open(placeholder, mode="rb") as imagefile:
                frame = imagefile.read()
        

        yield (b'--frame\r\nContent-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')