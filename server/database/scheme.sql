-- Table: board
CREATE TABLE board (
    board_id   INTEGER PRIMARY KEY AUTOINCREMENT
                       UNIQUE
                       NOT NULL,
    board_ip   TEXT    NOT NULL,
    board_name TEXT    NOT NULL
);


-- Table: configuration
CREATE TABLE configuration (
    configuration_id                   INTEGER PRIMARY KEY AUTOINCREMENT
                                               UNIQUE
                                               NOT NULL,
    configuration_wifi_ssid            STRING,
    configuration_read_interval        INTEGER,
    configuration_flashlight           INTEGER,
    configuration_camera               INTEGER,
    configuration_camera_quality       TEXT,
    configuration_camera_fps           INTEGER,
    configuration_steering_calibration STRING,
    board_id                           INTEGER UNIQUE
                                               NOT NULL,
    FOREIGN KEY (
        board_id
    )
    REFERENCES board (board_id) ON DELETE CASCADE
                                ON UPDATE CASCADE
);


-- Table: sensor_data
CREATE TABLE sensor_data (
    sensor_data_id             INTEGER PRIMARY KEY AUTOINCREMENT
                                       NOT NULL
                                       UNIQUE,
    board_id                   INTEGER,
    sensor_data_steering       REAL,
    sensor_data_hall           REAL,
    sensor_data_tie            REAL,
    sensor_data_tie_count      INTEGER,
    sensor_data_odometer       REAL,
    sensor_data_odometer_count INTEGER,
    sensor_data_wifistrength   REAL,
    sensor_data_rtc            INTEGER,
    sensor_data_battery        INTEGER,
    sensor_data_local_rtc      INTEGER NOT NULL,
    FOREIGN KEY (
        board_id
    )
    REFERENCES board (board_id) ON DELETE CASCADE
                                ON UPDATE CASCADE
);


-- Table: user
CREATE TABLE user (
    user_id       INTEGER PRIMARY KEY AUTOINCREMENT
                          UNIQUE
                          NOT NULL,
    user_name     TEXT    NOT NULL
                          UNIQUE,
    user_password TEXT    NOT NULL,
    user_role     TEXT
);

-- Add default users
INSERT INTO user (user_name, user_password, user_role) VALUES ('admin', '$argon2id$v=19$m=65536,t=3,p=4$rz8WCQqWoRm7pN97SaJdjQ$Kj6ciIJVWG2MQa7v7h+I1Y9zxMdMjzhkG7kO0BNUtIg', 'admin');
INSERT INTO user (user_name, user_password, user_role) VALUES ('user', '$argon2id$v=19$m=65536,t=3,p=4$E7CMNAn97De4lPCOtnxhgA$AtY9z/dWI2pdlDhvJopU7Z3gUnmC4d2XwefWKjNAbvU', 'user');
INSERT INTO user (user_name, user_password, user_role) VALUES ('view', '$argon2id$v=19$m=65536,t=3,p=4$pFXLhoObqlhTLjCOXGEBEQ$Knc7k/yOvbLUE1QLdaGwOFSbRB6eaKgBx+fZnswbSjo', 'user');