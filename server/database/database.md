# Database

## Schema
![Schema](database-schema.png)

## Tables
### board [receiver/webserver]
contains basic board information

| Column       |  Type  | Extra                                | Comment    |
| ------------ | :----: | ------------------------------------ | ---------- |
| `board_id`   |  INT   | PRIMARY KEY, AUTOINCREMENT, NOT NULL | Board ID   |
| `board_ip`   | STRING | NOT NULL                             | Board IP   |
| `board_name` | STRING | NOT NULL                             | Board Name |


### configuration [receiver/webserver]
contains current configuration of board (using health packets)

| Column                         |  Type  | Extra                                | Comment                      |
| ------------------------------ | :----: | ------------------------------------ | ---------------------------- |
| `configuration_id`             |  INT   | PRIMARY KEY, AUTOINCREMENT, NOT NULL | Configuration ID             |
| `configuration_wifi_ssid`      | STRING |                                      | SSID of current Wifi Network |
| `configuration_read_interval`  | STRING |                                      | Read Interval of Board       |
| `configuration_flashlight`     |  INT   |                                      | Brightness of Flashlight     |
| `configuration_camera`         |  INT   |                                      | Toggle if camera on or off   |
| `configuration_camera_quality` | STRING |                                      | Camera Quality               |
| `configuration_camera_fps`     |  INT   |                                      | Camera FPS                   |
| `board_id`                     |  INT   | FOREIGN KEY(`board`)                 | Link to Board ID             |


### sensor_data [receiver/webserver]
Contains sensor data sent via the data packets.

| Column                       |  Type   | Extra                                | Comment                         |
| ---------------------------- | :-----: | ------------------------------------ | ------------------------------- |
| `sensor_data_id`             |   INT   | PRIMARY KEY, AUTOINCREMENT, NOT NULL | Sensor ID                       |
| `board_id`                   |   INT   | FOREIGN KEY(`boardinfo`)             | Link to Board ID                |
| `sensor_data_steering`       |  REAL   |                                      | Sensor data for steering        |
| `sensor_data_hall`           |  REAL   |                                      | Sensor data for hall            |
| `sensor_data_tie`            |  REAL   |                                      | Sensor data for tie             |
| `sensor_data_tie_count`      |  REAL   |                                      | Current tie count in round      |
| `sensor_data_odometer`       |  REAL   |                                      | Sensor data for odometer        |
| `sensor_data_odometer_count` | INTEGER |                                      | Current odometer count in round |
| `sensor_data_wifistrength`   |  REAL   |                                      | Wifi Strength                   |
| `sensor_data_rtc`            |   INT   |                                      | Clock of ESP at sent            |
| `sensor_data_battery`        |   INT   |                                      | Battery level of the board      |
| `sensor_data_local_rtc`      |   INT   | NOT NULL                             | Clock of Receiver at receive    |

### user [webserver]
contains user data for the webserver

| Column          |  Type  | Extra                      | Comment                 |
| --------------- | :----: | -------------------------- | ----------------------- |
| `user_id`       |  INT   | PRIMARY KEY, AUTOINCREMENT | User ID                 |
| `user_name`     | STRING | NOT NULL                   | Username                |
| `user_password` | STRING | NOT NULL                   | User password           |
| `user_role`     | STRING |                            | User Role <admin, user> |

## Conventions
The table must be named of what it contains, its column names must follow this schema: `tablename_columnname`. Foreign keys keep their original name of the other table.

# SQL Commands
See `schemes.sql` file.
