"""
This file is part of the project "Kameraortungswagen" licensed under GPLv3.
It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
"""

# Setup script for the server, which sets the CSRF/JWT secret for the webserver

# Standard library imports
import secrets, re, os

print("[#] Setting up CSRF / JWT secret for the webserver...")

# Generate a random string of 32 characters
secret = secrets.token_hex(32)

print("[#] Generated secret: " + secret)

config_file = os.path.abspath(os.path.realpath(__file__)  + "/../webserver/config.py")
print("[#] Config file: " + config_file)

# read the file
content = ""
with open(config_file, "r") as f:
    # read the file
    content = f.read()

# replace the old token with the new one using a regex
regex = r"SECRET_KEY = 'dev'"
subst = "SECRET_KEY = '" + secret + "'"

result = re.sub(regex, subst, content, 0, re.MULTILINE)

if result == content:
    print("[+] Secret already set, skipping...")

# write secret to the config file
with open(config_file, "w") as f:
    f.write(result)

print("[+] Secret set successfully!")
