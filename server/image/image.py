import socket
import os

UDP_IP = "0.0.0.0"
UDP_PORT = 4243

# create udp socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# bind socket to UDP_IP and UDP_PORT
sock.bind((UDP_IP, UDP_PORT))

# wait and receive packets

print("[+] Started image receiver on port " + str(UDP_PORT) + "...")

while True:
    data, addr = sock.recvfrom(50000)
    ip = addr[0]
    try:
        if not os.path.exists("./files/"):
            os.makedirs("./files/")
        with open("./files/" + ip + ".jpg", 'wb') as f:
            f.write(data)
    except:
        pass