#!/bin/sh
# Description: This script starts the server
python3 -u setup.py 
python3 -u receiver/receiver.py &
python3 -u image/image.py &
cd webserver/ && gunicorn -w 4 --worker-class=gevent --worker-connections=1000 --bind 0.0.0.0:80 server:app