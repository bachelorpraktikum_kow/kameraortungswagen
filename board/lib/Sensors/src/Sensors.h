/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Sensors.h
 * @authors Joschka Hemming, Philipp Macher
 * @date 24/02/2023
 * 
 * This file contains a declaration of the Sensors class and all its methods used in the project. 
 * Please refer to the class for more information.
 */

#pragma once

/**
 * @brief A struct for storing sensor readings.
 * 
 * This struct is used as a container for all sensor readings.
 */
struct readings_t {
    int steering_angle;
    int hall_sensor;
    int odometer_count;
    int odometer_speed;
    int tie_sensor_count;
    int tie_sensor_speed;
    int battery_voltage;
    timeval time;
    int8_t wifi_strength;
};

/**
 * @class Sensors
 * 
 * @brief The Sensors class manages reading all sensor data and sending it to the main task
 * 
 * This class is used to manage the sensor readings. It starts a task that retrieves the sensor readings.
 * It also handles the hall sensor interrupt and the ads1115 interrupt.
 * All information is sent to the main task in the interval specified in the settings.
 */
class Sensors {
    static bool forced_round;

    /**
     * @brief Retrieves measurements from sensors and sends them to the main task
     * 
     * This method is called by the timer interrupt. It retrieves the current sensor data and sends it to the main task.
     * 
     * @param arg Arguments for the task. Should be a pointer to a QueueHandle_t.
     */
    static bool get_measurements(void* arg);

    /**
     * @brief Retrieves the current readings from the sensors via ads1115
     * 
     * This method is called by the timer interrupt. It starts a conversion on the ads1115 and then notifies the main task.
     * 
     * @param arg Arguments for the task. Should be the curernt task handle.
     */
    static void get_ads_result(void* arg);

    /**
     * @brief Interrupt handler for the hall sensor
     * 
     * This method is called by the hall sensor interrupt. It sets the landmark_reached flag to true.
     * 
     * @param arg Arguments for the task. Should be a pointer to a bool.
     */
    static void hall_sensor_handler(void* arg);

public:

    /**
     * @brief Starts the sensor task
     * 
     * This method is called as a FREERTOS task. It starts the sensor readings as a loop.
     * It notifies the main task in the interval specified in the settings and delivers the
     * current sensor readings.
     * 
     * @param arg Argument for the task. Required by FREERTOS.
     */
    static void start(void* arg);

    /**
     * @brief Forces a round to be sent to the server in the next iteration
     * 
     * This is used to force a round to be sent to the server when the server. 
     * It sets a flag in the sensor reads.
     */
    static void force_round();
};
