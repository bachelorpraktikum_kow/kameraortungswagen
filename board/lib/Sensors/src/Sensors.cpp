/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Sensors.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 24/02/2023
 * 
 * This file contains an implementation of the Sensors class and all its methods used in the project. 
 * Please refer to the class for more information.
 */

#include <Arduino.h>
#include "Sensors.h"
#include "driver/timer.h"
#include "driver/adc.h"
#include "driver/pcnt.h"
#include "Logger.h"
#include "Config.h"
#include "Connection.h"
#include "Packets.h"
#include "pins.h"

#include "Wire.h"
#include "SPI.h"
#include "Adafruit_ADS1X15.h"

#define TIMER_GROUP TIMER_GROUP_0
#define TIMER_ID TIMER_0

#define ODOMETER_COUNTER PCNT_UNIT_0
#define TIE_COUNTER PCNT_UNIT_1

#define ADS_ADDRESS 0x48
#define ADS_RDY_PIN GPIO_NUM_12

#define WHEEL_DIAMETER_MM 10
#define WHEEL_HOLE_COUNT 12
#define TIE_WIDTH 3.7           // Measured tie width = 3.7mm

// ADS1115 outputs int16, but no negative values in single ended mode, so 2^15 = 32768 is used
#define MV_PER_ADS_STEP (double) 4096 / 32768

#define TAG "Sensor"

Adafruit_ADS1115 ads_sensor;

using namespace std;

bool Sensors::forced_round = true;


void IRAM_ATTR Sensors::get_ads_result(void* arg) {

    TaskHandle_t* main_task = (TaskHandle_t*) arg;

    vTaskNotifyGiveFromISR(*main_task, NULL);
}

void IRAM_ATTR Sensors::hall_sensor_handler(void* arg) {

    bool* landmark_reached = (bool*) arg;

    *landmark_reached = true;

}

bool IRAM_ATTR Sensors::get_measurements(void* arg) {
    
    QueueHandle_t* queue = (QueueHandle_t*) arg;

    // No matter why this function gets called, it should reset the current timer to 0
    timer_set_counter_value(TIMER_GROUP, TIMER_ID, 0);

    timeval time;

    // Get system time. Without any syncing, it is the time passed since last µC reset
    // gettimeofday returns a timeval struct. tv_sec contains seconds since last restart and tv_usec contains µs since the given second
    // So to get the us since last restart: tv_sec * 1000000 + tv_usec
    gettimeofday(&time, NULL);

    // Send back readings
    BaseType_t high_task_awoken = pdFALSE;
    xQueueSendFromISR(*queue, &time, &high_task_awoken);

    // Return whether we need to yield at the end of ISR
    // Yielding seems to enforce a context switch after leaving ISR.
    // This seems to be needed if no higher priority task has been woken by the new queue element.
    // This is inspired by https://github.com/espressif/esp-idf/blob/v4.3/examples/peripherals/timer_group/main/timer_group_example_main.c
    return high_task_awoken == pdTRUE;
}

void Sensors::start(void* arg) {

    Settings* settings = Config::get_settings();

    // We need a queue to pass measurement data between ISR and sensor task
    QueueHandle_t timer_queue = xQueueCreate(1, sizeof(timeval));

    TaskHandle_t current_task = xTaskGetCurrentTaskHandle();

    bool ads_reachable = true;
    bool landmark_reached = false;

    // Init ADS1115
    TwoWire* i2c_bus = new TwoWire(0);
    i2c_bus->setPins(15, 14);
    ads_reachable = ads_sensor.begin(ADS_ADDRESS, i2c_bus);
    if (!ads_reachable)
        Logger::err(TAG, String("I2C device was not reachable at address ") + String(ADS_ADDRESS) + String("! (decimal)"));

    // Set gain to measure up to 4.096V, 2.2V is our maximal analogue value though
    ads_sensor.setGain(GAIN_ONE);

    timer_config_t config;
    config.divider = 80;       // Now timer counts µs
    config.counter_dir = TIMER_COUNT_UP;
    config.counter_en = TIMER_PAUSE;
    config.alarm_en = TIMER_ALARM_EN;
    config.auto_reload = TIMER_AUTORELOAD_EN;
    // default clock source is APB (should be around 80MHz)

    timer_init(TIMER_GROUP, TIMER_ID, &config);

    // Let timer call get_measurments each time it reaches it's alarm value 
    timer_isr_callback_add(TIMER_GROUP, TIMER_ID, Sensors::get_measurements, &timer_queue, 0);

    // Set timer's alarm value, the value at which it throws an interrupt
    timer_set_alarm_value(TIMER_GROUP, TIMER_ID, settings->sensors_slowest_update_ms*1000);

    // Start timer
    timer_start(TIMER_GROUP, TIMER_ID);

    // GPIO configs for digital sensors
    gpio_config_t io_conf = {};
    io_conf.intr_type = GPIO_INTR_NEGEDGE;          // Negedge interrupt, expected behavior at landmark: 1-0-1
    io_conf.mode = GPIO_MODE_INPUT;                 // set as input mode
    io_conf.pin_bit_mask = 1ULL<<HALL_PIN;      // bit mask of the pins that you want to set
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;   // disable pull-down mode
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;       // disable pull-up mode
    
    // configure hall sensor GPIO with the given settings
    gpio_config(&io_conf);

    // Now configure ADS RDY pin
    io_conf.pin_bit_mask = 1ULL<<ADS_RDY_PIN;
    io_conf.pull_up_en = GPIO_PULLUP_ENABLE;

    gpio_config(&io_conf);

    gpio_install_isr_service(ESP_INTR_FLAG_IRAM);

    gpio_isr_handler_add(ADS_RDY_PIN, Sensors::get_ads_result, &current_task);
    gpio_isr_handler_add(HALL_PIN, Sensors::hall_sensor_handler, &landmark_reached);

    // Configure Counter
    pcnt_config_t pcnt_config = {
        ODOMETER_PIN,
        -1,
        PCNT_CHANNEL_LEVEL_ACTION_KEEP,
        PCNT_CHANNEL_LEVEL_ACTION_KEEP,
        PCNT_CHANNEL_EDGE_ACTION_INCREASE,
        PCNT_CHANNEL_EDGE_ACTION_INCREASE,
        32767,
        -1,
        ODOMETER_COUNTER,
        PCNT_CHANNEL_0
    };

    pcnt_unit_config(&pcnt_config);

    pcnt_config.pulse_gpio_num = TIE_PIN;
    pcnt_config.unit = TIE_COUNTER; 
    pcnt_config.channel = PCNT_CHANNEL_1;

    pcnt_unit_config(&pcnt_config);

    // Wait for data to arrive in queue
    readings_t read;
    int odometer_count_global = 0, tie_count_global = 0;
    while(1) {
        if (xQueueReceive(timer_queue, &read.time, settings->sensors_slowest_update_ms * 2)) {
            
            // Read Counters for tie and odo
            int16_t odometer_pulses, tie_pulses;
            pcnt_get_counter_value(ODOMETER_COUNTER, &odometer_pulses);
            pcnt_get_counter_value(TIE_COUNTER, &tie_pulses);
            
            // Clear counters to start new counting
            pcnt_counter_clear(ODOMETER_COUNTER);
            pcnt_counter_clear(TIE_COUNTER);

            odometer_count_global += odometer_pulses;
            tie_count_global += tie_pulses;

            // Calculate speed
            double odo_track_length = (WHEEL_DIAMETER_MM * M_PI * odometer_pulses) / (WHEEL_HOLE_COUNT * 2);
            double tie_track_length = TIE_WIDTH * tie_pulses;
            int odo_speed = odo_track_length / ((double) settings->sensors_slowest_update_ms / 1000);
            int tie_speed = tie_track_length / ((double) settings->sensors_slowest_update_ms / 1000);

            // If this is the first packet or the server forces a round, set landmark reached to true
            if (forced_round) {
                landmark_reached = true;
                forced_round = false;
            }

            // Check if landmark was reached
            read.hall_sensor = landmark_reached;
            landmark_reached = false;

            // Measure time it takes for ADS readings
            timeval start;
            gettimeofday(&start, NULL);
            if (ads_reachable) {
                // read.steering_angle = ads_sensor.readADC_SingleEnded(0) * mv_per_step;
                ads_sensor.startADCReading(MUX_BY_CHANNEL[0], /*continuous=*/false);
                ulTaskNotifyTake(pdTRUE, 1000 / portTICK_PERIOD_MS);
                read.steering_angle = ads_sensor.getLastConversionResults() * MV_PER_ADS_STEP;

                // Read Battery voltage
                ads_sensor.startADCReading(MUX_BY_CHANNEL[1], false);
                ulTaskNotifyTake(pdTRUE, 1000 / portTICK_PERIOD_MS);
                read.battery_voltage = ads_sensor.getLastConversionResults() * MV_PER_ADS_STEP;
            } else {
                read.steering_angle = 0;
                read.battery_voltage = 0;
            }
            timeval end;
            gettimeofday(&end, NULL);

            Logger::info(TAG, String("reading (s:µs): ") + (end.tv_sec - start.tv_sec) + String(":") + (end.tv_usec - start.tv_usec));

            // Print results to logs
            read.wifi_strength = Connection::get_strength();
            Logger::info(TAG, String("Odometer speed: ") += odo_speed);
            Logger::info(TAG, String("Odometer pulses: ") += odometer_pulses);
            Logger::info(TAG, String("Hall Sensor: ") += read.hall_sensor);
            Logger::info(TAG, String("Tie Sensor speed: ") += tie_speed);
            Logger::info(TAG, String("Tie Sensor pulses: ") += tie_pulses);
            Logger::info(TAG, String("Steering angle: ") += read.steering_angle);
            Logger::info(TAG, String("Battery Voltage: ") += read.battery_voltage);
            Logger::info(TAG, String("WiFi strength: ") += read.wifi_strength);
            Logger::info(TAG, String("Timestamp in s: ") += read.time.tv_sec);
            Logger::info(TAG, String("Timestamp in µs: ") += read.time.tv_usec);

            // Reset steps if landmark reached
            if (read.hall_sensor) {
                odometer_count_global = 0;
                tie_count_global = 0;
            }

            // Send values to further processing
            read.odometer_speed = odo_speed;
            read.odometer_count = odometer_count_global;
            read.tie_sensor_speed = tie_speed;
            read.tie_sensor_count = tie_count_global;

            // Send packet
            Packets::send_data_packet(read);
        } else {
            Logger::warn(TAG, String("Haven't got sensor reads in ") + (double) (settings->sensors_slowest_update_ms*2)/1000 + String(" seconds!"));
        }
    }
}

void Sensors::force_round() {
    Sensors::forced_round = true;
}