/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Camera.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 19/01/2023
 * 
 * This file contains an implementation of the Camera class and all its methods used in the project. 
 * Please refer to the class for more information.
 */


#include "Camera.h"
#include "Config.h"
#include "Logger.h"
#include "Packets.h"
#include "BTSerial.h"

#define TAG "Camera"

#define PWDN_GPIO_NUM 32
#define RESET_GPIO_NUM -1
#define XCLK_GPIO_NUM 0
#define SIOD_GPIO_NUM 26
#define SIOC_GPIO_NUM 27
#define Y9_GPIO_NUM 35
#define Y8_GPIO_NUM 34
#define Y7_GPIO_NUM 39
#define Y6_GPIO_NUM 36
#define Y5_GPIO_NUM 21
#define Y4_GPIO_NUM 19
#define Y3_GPIO_NUM 18
#define Y2_GPIO_NUM 5
#define VSYNC_GPIO_NUM 25
#define HREF_GPIO_NUM 23
#define PCLK_GPIO_NUM 22

bool Camera::initialized = false;
bool Camera::running = false;
bool Camera::update_frame_buffer = false;

framesize_t Camera::translate_framesize(const String& frame_size) {
    if (frame_size == "QQVGA") {
        return FRAMESIZE_QQVGA;
    } else if (frame_size == "QCIF") {
        return FRAMESIZE_QCIF;
    } else if (frame_size == "HQVGA") {
        return FRAMESIZE_HQVGA;
    } else if (frame_size == "QVGA") {
        return FRAMESIZE_QVGA;
    } else if (frame_size == "CIF") {
        return FRAMESIZE_CIF;
    } else if (frame_size == "VGA") {
        return FRAMESIZE_VGA;
    } else if (frame_size == "SVGA") {
        return FRAMESIZE_SVGA;
    } else if (frame_size == "XGA") {
        return FRAMESIZE_XGA;
    } else if (frame_size == "SXGA") {
        return FRAMESIZE_SXGA;
    } else if (frame_size == "UXGA") {
        return FRAMESIZE_UXGA;
    } else {
        Logger::warn(TAG, "Received an invalid frame size! Returning QQVGA...");
        return FRAMESIZE_QQVGA;
    }
}


bool Camera::init() {
    Logger::info(TAG, "Initializing...");
    camera_config_t config;

    Settings* settings = Config::get_settings();

    if (settings->camera_quality == "") {
        settings->camera_quality == "QQVGA";
        Config::set_settings(settings);
        Logger::err(TAG, "Camera quality was empty string!");
    }

    config.ledc_channel = LEDC_CHANNEL_5;
    config.ledc_timer = LEDC_TIMER_0;
    config.pin_d0 = Y2_GPIO_NUM;
    config.pin_d1 = Y3_GPIO_NUM;
    config.pin_d2 = Y4_GPIO_NUM;
    config.pin_d3 = Y5_GPIO_NUM;
    config.pin_d4 = Y6_GPIO_NUM;
    config.pin_d5 = Y7_GPIO_NUM;
    config.pin_d6 = Y8_GPIO_NUM;
    config.pin_d7 = Y9_GPIO_NUM;
    config.pin_xclk = XCLK_GPIO_NUM;
    config.pin_pclk = PCLK_GPIO_NUM;
    config.pin_vsync = VSYNC_GPIO_NUM;
    config.pin_href = HREF_GPIO_NUM;
    config.pin_sccb_sda = SIOD_GPIO_NUM;
    config.pin_sccb_scl = SIOC_GPIO_NUM;
    config.pin_pwdn = PWDN_GPIO_NUM;
    config.pin_reset = RESET_GPIO_NUM;
    config.xclk_freq_hz = 20000000;
    config.pixel_format = PIXFORMAT_JPEG;
    config.frame_size = translate_framesize(Config::get_settings()->camera_quality);
    config.jpeg_quality = 10;
    config.fb_count = 1;

    // Initialize camera
    esp_err_t init_err = esp_camera_init(&config);
    if (init_err != ESP_OK) {
        Logger::err(TAG, "Cannot initialize!");
        return false;
    }

    // Get sensor to apply settings
    sensor_t *sensor = esp_camera_sensor_get();
    if (sensor == NULL) {
        Logger::warn(TAG, "Problem reading sensor settings!");
        return false;
    }

    // Gain mode to auto
    sensor->set_gain_ctrl(sensor, 1);
    // Exposure mode to on
    sensor->set_exposure_ctrl(sensor, 1);
    // White Balance mode to auto
    sensor->set_awb_gain(sensor, 1);

    Logger::info(TAG, "Finished initializing.");
    initialized = true;

    return true;
}


void Camera::send_image() {
    // Check if camera has been initialized
    if (!initialized) {
        Logger::warn(TAG, "Send image failed: Not inialized yet!");
        return;
    }

    // Get frame buffer
    camera_fb_t *fb = esp_camera_fb_get();

    // Send camera packet
    Packets::send_camera_packet((char*) fb->buf, fb->len);

    // Release frame buffer
    esp_camera_fb_return(fb);
}

void Camera::update_framesize() {
    Camera::update_frame_buffer = true;
}

void Camera::set_frame_size(const String& frame_size) {
    // Check if camera has been initialized
    if (!initialized) {
        return;
    }

    // Get camera sensor and translate string to enum
    sensor_t* sensor = esp_camera_sensor_get();
    framesize_t framesize = translate_framesize(frame_size);

    // Apply framesize and check for success
    if (sensor->set_framesize(sensor, framesize)) {
        Logger::warn(TAG, "Failed to set frame size!");
    } else {
        Logger::info(TAG, "Set frame_size to " + String(frame_size) + ".");
    }
}

void Camera::start(void* arg) {
    // Check if camera is already running
    if (Camera::running) {
        Logger::warn(TAG, "Already running!");
        return;
    }

    // Initialize camera if not initialzed already
    if (!Camera::initialized) {
        if(!init()) {
            Logger::err(TAG, "Cannot start!");
            vTaskDelete(NULL);
            return;
        }
    }

    // Start loop to send camera image
    Logger::info(TAG, "Starting...");
    running = true;
    while (Config::get_settings()->camera_enabled && running) {
        // send image and wait to achieve set fps
        send_image();

        // Check if frame size has to be changed
        if (update_frame_buffer) {
            set_frame_size(Config::get_settings()->camera_quality);
            update_frame_buffer = false;
        }

        // Protect again zero-division
        int fps = Config::get_settings()->camera_fps;
        vTaskDelay((1000 / (fps == 0 ? 1 : fps)) / portTICK_PERIOD_MS);
    }
    running = false;
    Logger::info(TAG, "Stopped");
    vTaskDelete(NULL);
}

bool Camera::is_running() {
    return running;
}

bool Camera::is_initialized() {
    return initialized;
}