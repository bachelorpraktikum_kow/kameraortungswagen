/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Camera.h
 * @authors Joschka Hemming, Philipp Macher
 * @date 19/01/2023
 * 
 * This file contains a declaration of the Camera class and all its methods used in the project. 
 * Please refer to the class for more information.
 */

#pragma once
#include <Arduino.h>
#include <esp_camera.h>

/**
 * @class Camera
 * 
 * @brief The Camera class provides a set of functions for interacting with a camera module.
 * 
 * This class allows for the initialization, configuration, and control of a camera module.
 * It provides functions for setting the frame size and frame rate of the camera, as well as sending images
 * from the camera as packets. Additionally, it provides a start function which starts a loop to continuously
 * capture and send images from the camera.
 */
class Camera {
private:
    static bool initialized;
    static bool running;
    static bool update_frame_buffer;

    /**
     * @brief Translates a frame size string to a framesize_t enum.
     * 
     * Translates the string representation of a frame size to an enumerated type.
     *
     * @param frame_size A string representing the frame size.
     * @return The corresponding framesize_t enum. If an invalid frame size is received, a warning is logged and FRAMESIZE_QQVGA is returned.
     */
    static framesize_t translate_framesize(const String& frame_size);

    /**
     * @brief Set the frame size of the camera
     * 
     * This function sets the frame size of the camera by taking an input string
     * and matching it with a predefined frame size enum. If the input string is not a valid frame size,
     * the function will return without doing anything.
     * 
     * @param frame_size A string representing the desired frame size (e.g. "QVGA", "VGA", etc.).
     */
    static void set_frame_size(const String& frame_size);

public:
    /**
     * @brief Initialize the camera module
     * 
     * This function sets up the camera module with the specified configuration
     * such as the pin configuration, image format, image size, and image quality.
     * It also applies any necessary settings such as gain, exposure, and white balance.
     * 
     * @return if the camera was successfully initalized
     */
    static bool init();

    /**
     * @brief Send image data from the camera
     * 
     * This function retrieves the current frame buffer from the camera,
     * sends it as a packet to the specified port, and releases the frame buffer.
     */
    static void send_image();

    /**
     * @brief Tell camera to update the frame buffer
     * 
     * This function sets the update_frame_buffer flag to true.
     */
    static void update_framesize();

    /**
     * @brief Start the camera task
     * 
     * This function starts a loop that retrieves the current frame buffer from the camera,
     * sends it as a packet, releases the frame buffer, and wait for a duration calculated based on the set fps.
     * If the camera is not initialized yet, it will initialize it before starting the loop.
     * If the setting config->camera_enabled is false, it will continue to the next iteration
     */
    static void start(void* arg);

    /**
     * @brief Check if the camera is running
     * 
     * This function checks if the camera task is running.
     * 
     * @return if the camera task is running
     */
    static bool is_running();

    /**
     * @brief Stop the camera task
     * 
     * This function stops the camera task by setting the running flag to false.
     */
    static bool is_initialized();
};