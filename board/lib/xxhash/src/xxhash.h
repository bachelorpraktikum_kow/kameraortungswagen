/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file xxhash.h
 * @authors Joschka Hemming, Philipp Macher, OpenAI
 * @date 31/12/2022
 * 
 * Based on the XXH32 hash function implementation by OpenAI's ChatGPT.
 * 
 * This file contains an implementaion of the XXH32 hash function, which was developed by Yann Collet
 * and is available as part of the open source XXHash library.
 * https://github.com/Cyan4973/xxHash
 */


#pragma once
#include <stdint.h>

#define PRIME_1 0x9E3779B1U
#define PRIME_2 0x85EBCA77U
#define PRIME_3 0xC2B2AE3DU
#define PRIME_4 0x27D4EB2FU
#define PRIME_5 0x165667B1U

#define SEED 0

typedef unsigned int size_t;
typedef uint32_t hash_t;

/**
 * @brief Calculates the xxHash32 checksum of a block of data.
 * 
 * This function calculates the xxHash32 checksum of a block of data. The xxHash32 checksum is a 32-bit hash value
 * that is calculated using the xxHash algorithm. The function takes a pointer to the data and the length of the data
 * block as input. The function returns the calculated checksum as a hash_t value.
 * 
 * @param data A pointer to the data block.
 * @param len The length of the data block.
 * @return The calculated checksum.
 */
hash_t XXHASH32(const void* data, size_t len) {
    const uint8_t* p = (const uint8_t*) data;
    const uint8_t* const bEnd = p + len;
    uint32_t h32;

    if (len >= 16) {
        const uint8_t* const limit = bEnd - 16;
        uint32_t v1 = SEED + PRIME_1 + PRIME_2;
        uint32_t v2 = SEED + PRIME_2;
        uint32_t v3 = SEED + 0;
        uint32_t v4 = SEED - PRIME_1;

        do {
            v1 += *(const uint32_t*) p * PRIME_2;
            v1 = (v1 << 13) | (v1 >> 19);
            v1 *= PRIME_1;
            p += 4;
            v2 += *(const uint32_t*) p * PRIME_2;
            v2 = (v2 << 13) | (v2 >> 19);
            v2 *= PRIME_1;
            p += 4;
            v3 += *(const uint32_t*) p * PRIME_2;
            v3 = (v3 << 13) | (v3 >> 19);
            v3 *= PRIME_1;
            p += 4;
            v4 += *(const uint32_t*) p * PRIME_2;
            v4 = (v4 << 13) | (v4 >> 19);
            v4 *= PRIME_1;
            p += 4;
        } while (p <= limit);

        h32 = ((v1 << 1) + (v1 >> 31)) + ((v2 << 7) + (v2 >> 25)) + ((v3 << 12) + (v3 >> 20)) + ((v4 << 18) + (v4 >> 14));
    } else {
        h32 = SEED + PRIME_5;
    }

    h32 += (uint32_t) len;

    while (p + 4 <= bEnd) {
        h32 += *(const uint32_t*) p * PRIME_3;
        h32 = (h32 << 17) | (h32 >> 15);
        h32 *= PRIME_4;
        p += 4;
    }

    while (p < bEnd) {
        h32 += *p * PRIME_5;
        h32 = (h32 << 11) | (h32 >> 21);
        h32 *= PRIME_1;
        p++;
    }

    h32 ^= h32 >> 15;
    h32 *= PRIME_2;
    h32 ^= h32 >> 13;
    h32 *= PRIME_3;
    h32 ^= h32 >> 16;

    return h32;
}
