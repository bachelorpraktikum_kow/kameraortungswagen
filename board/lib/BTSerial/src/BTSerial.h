/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file BTSerial.h
 * @authors Joschka Hemming, Philipp Macher
 * @date 10/03/2023
 * 
 * This file contains a declaration of the BTSerial class and all its methods used in the project. 
 * Please refer to the class for more information.
 */


#ifndef BT_SERIAL_H
#define BT_SERIAL_H

#include <Arduino.h>

const static char* WELCOME_MESSAGE = "Hi!\n"
    "To use BT Serial to set settings, please provide key/value pairs in the format \"{{ key }}={{ value }}\". {{ key }} is case-insensitive\n"
    "Following settings can be set:\n"
    "name, update_freqm, flashlight, ssid, password, ip\n"
    "If you need to restart the ESP, use \"restart\"";

/**
 * @class BTSerial
 * 
 * @brief The BTSerial class provides a set of functions for managing bluetooth.
 * 
 * This class allows for managing of bluetooth connectivity. It provides functions for starting, stopping bluetooth as
 * well as receiveing and sending data.
 */
class BTSerial {
private:
    
    static bool running;

    /**
     * @brief Handler for incoming data via BT serial.
     * 
     * This method is called when data is received via BT serial. It processes and handles the received data.
     * This includes altering the settings accordingly and restarting if requested.
     * 
     * @param buffer the received data
     * @param size the size of the received data
     */
    static void receiveHandler(const uint8_t *buffer, size_t size);

public:

    /**
     * @brief Start function for bluetooth connectivity
     * 
     * This function is called to start the bluetooth connectivity.
     * It starts the bluetooth serial and adds a handler to execute when data is received via BT serial.
     * After the defined timeout of no connection, the bluetooth serial is stopped and the function returns.
     */
    static void start();

    /**
     * @brief Sends a message via BT serial
     * 
     * This method sends a message via BT serial. It checks whether there is a connection first.
     * 
     * @param message the message to send
     */
    static void send(String* message);

    /**
     * @brief Checks whether there is a connection via BT serial
     * 
     * This method checks whether there is a connection via BT serial.
     * 
     * @return true if there is a connection, false otherwise
     */
    static bool connected();

    /**
     * @brief Checks whether the BT serial task is running
     * 
     * This method checks whether the BT serial task is running.
     * 
     * @return true if bluetooth is running, false otherwise
     */
    static bool is_running();
};

#endif