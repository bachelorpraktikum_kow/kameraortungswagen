/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file BTSerial.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 10/03/2023
 * 
 * This file contains an implementation of the BTSerial class and all its methods used in the project. 
 * Please refer to the class for more information.
 */

#include "Arduino.h"
#include "BluetoothSerial.h"
#include "BTSerial.h"
#include "Logger.h"
#include "Config.h"
#include "Flashlight.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please change the menuonfig
#endif

#if !defined(CONFIG_BT_SPP_ENABLED)
#error Serial Bluetooth not available or not enabled. It is only available for the ESP32 chip.
#endif

#define TAG "BT Serial"
#define BT_TIMEOUT_S 20

BluetoothSerial SerialBT;
bool started = false;
bool BTSerial::running = true;


void BTSerial::start() {
    SerialBT.begin(Config::get_settings()->device_name);
    Logger::info(TAG, "BT Serial started!");

    // Add a handler to execute when data is received via BT serial
    SerialBT.onData(receiveHandler);

    // Wait a few milliseconds to give BT time to start up before advertising it's readiness
    vTaskDelay(100/portTICK_PERIOD_MS);

    started = true;

    // This block of code works in the following way:
    // In each iteration, it checks for edges on the connection status:
    // -> If there is a positive edge (0 -> 1), it displays a welcome message
    // -> If there is a negative edge (1 -> 0), it starts counting a timeout to stop BT
    // Initial value for the connection status is true, because we want a negative edge happening at the start
    // I anticipated that it is not possible to connect to the device before entering this loop
    bool connection_status_old, connection_status = true;
    bool timeout_running = false;
    timeval timeout_end;
    while (1) {
        connection_status_old = connection_status;
        connection_status = BTSerial::connected();
        
        // No edge -> Check if timeout is running and finished
        // It is -> Stop BT
        // It is not -> Nothing to do
        if (connection_status == connection_status_old) {

            // Check for running timeout
            if (timeout_running) {
                timeval current_time;
                gettimeofday(&current_time, NULL);

                // Check it end of timeout is reached
                if (current_time.tv_sec > timeout_end.tv_sec) {
                    started = false;
                    SerialBT.end();
                    Logger::info(TAG, "BT Serial stopped because of incativity");
                    break;
                }
            }
            // Block for one second, then continue
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            continue;
        }

        // Pos edge
        if (connection_status) {
            timeout_running = false;
            SerialBT.println(WELCOME_MESSAGE);
        }
        // Neg edge
        else {
            timeout_running = true;
            gettimeofday(&timeout_end, NULL);
            timeout_end.tv_sec += BT_TIMEOUT_S;
        }

        // Block for one second
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    running = false;
}

void BTSerial::receiveHandler(const uint8_t *buffer, size_t size) {
    char res[size];
    memcpy(&res, buffer, sizeof(res));
    String msg = String(res);

    Logger::info(TAG, "Received message...");

    // split message into key-value pair based on "="
    int lineFeed_index = msg.indexOf((char) 10);
    int carriageReturn_index = msg.indexOf((char) 13);
    int end_index = lineFeed_index < carriageReturn_index ? lineFeed_index : carriageReturn_index;

    int index = msg.indexOf("=");
    bool has_value = true;
    if (index == -1) {
        has_value = false;
        index = end_index;
    }
    String key = msg.substring(0, index);
    String value = msg.substring(index + 1, end_index);

    // Especially useful for mobile users
    key.toLowerCase();

    // Check restart
    if (key == "restart") {
        Logger::info(TAG, "Restarting...");
        ESP.restart();
        return;
    }

    // Alter settings based on key(-value) pair received
    Settings* settings = Config::get_settings();
    if (key == "ip") {
        if (has_value) {
            settings->wifi_receiver_ip = value;
            Logger::info(TAG, "Set receiver IP");
        } else {
            Logger::info(TAG, "Receiver IP: " + settings->wifi_receiver_ip);
        }
    } else if (key == "update_freq") {
        if (has_value) {
            settings->sensors_slowest_update_ms = value.toInt();
            Logger::info(TAG, "Set update frequency");
        } else {
            Logger::info(TAG, "Update frequency: " + String(settings->sensors_slowest_update_ms));
        }		
    } else if (key == "ssid") {
        if (has_value) {
            settings->wifi_ssid = value;
            Logger::info(TAG, "Set wifi ssid");
        } else {
            Logger::info(TAG, "SSID: " + settings->wifi_ssid);
        }
    } else if (key == "password") {
        if (has_value) {
            settings->wifi_password = value;
            Logger::info(TAG, "Set wifi password");
        } else {
            Logger::info(TAG, "Password: " + settings->wifi_password);
        }        
    } else if (key == "name") {
        if (has_value) {
            settings->device_name = value;
            Logger::info(TAG, "Set device name");
        } else {
            Logger::info(TAG, "Device name: " + settings->device_name);
        }
    } else if (key == "camera") {
        if (has_value) {
            if (value == "false") {
                settings->camera_enabled = false;
                Logger::info(TAG, "Turned camera off");
            } else if (value == "true") {
                settings->camera_enabled = true;
                Logger::info(TAG, "Turned camera on");
            } else {
                Logger::warn(TAG, "Could not recognize given input!");
            }
        } else {
            Logger::info(TAG, "Camera enabled: " + String(settings->camera_enabled));
        }
    } else if (key == "flashlight") {
        if (has_value) {
            Flashlight::set_status(value.toInt());
            Logger::info(TAG, "Set flashlight to " + String(Flashlight::get_status()));
        } else {
            Logger::info(TAG, "Flashlight is set to: " + String(Flashlight::get_status()));
        }
    } else {
        Logger::warn(TAG, "Could not recognize given input!");
        Logger::info(TAG, key + ": " + value);
        return;
    }

    // Save settings to EEPROM
    String serialized_settings = Config::serialize(settings);
    
    // Set settings in config
    Config::set_settings(settings);
    Config::store_to_EEPROM(serialized_settings);
}

void BTSerial::send(String* message) {
    if (!BTSerial::connected()) {
        Logger::warn(TAG, "Trying to send a BT messge although there is no connection yet!");
    } else {
        SerialBT.println(*message);
    }
}

bool BTSerial::connected() {
    if (!started) {
        return false;
    }
    return SerialBT.connected(0);
}

bool BTSerial::is_running() {
    return running;
}