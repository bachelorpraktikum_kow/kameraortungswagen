/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file DebugLed.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 12/02/2023
 * 
 * This file contains an implementation of the DebugLed class and all its methods used in the project. 
 * Please refer to the class for more information.
 */


#include "DebugLed.h"
#include "Logger.h"
#include "Camera.h"
#include <Arduino.h>

#define TAG "DebugLed"

// |-- Private --|

int DebugLed::gpio = 33;
bool DebugLed::running = false;
int DebugLed::interval = 500;


// |-- Public --|

void DebugLed::init(void* arg) {
    Logger::info(TAG, "Setting up...");
    ledcSetup(2, 60, 8);
    ledcAttachPin(gpio, 2);
    ledcWrite(2, 256);
    Logger::info(TAG, "Finished setting up.");
}

void DebugLed::start(void* arg) {
    Logger::info(TAG, "Starting...");
    running = true;
    int status = 0;
    while (!Camera::is_initialized() && running) {
        set_status(status);
        status = status == 0 ? 10 : 0;
        vTaskDelay(interval / portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

void DebugLed::stop() {
    running = false;
    Logger::info(TAG, "Stopping...");
}

void DebugLed::set_status(int level) {
    ledcWrite(2, 256 - ((256 / 10) * level));
}

void DebugLed::set_interval(int interval) {
    DebugLed::interval = interval;
}