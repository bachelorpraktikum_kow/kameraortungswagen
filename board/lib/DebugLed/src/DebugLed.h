/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file DebugLed.h
 * @authors Joschka Hemming, Philipp Macher
 * @date 09/02/2023
 * 
 * This file contains a declaration of the DebugLed class and all its methods used in the project. 
 * Please refer to the class for more information.
 */

#pragma once


/**
 * @class DebugLed
 * 
 * @brief The DebugLed class provides a set of functions for interacting with a led module.
 * 
 * This class allows for the initialization, configuration, and control of the onboard led.
 */ 
class DebugLed {
private:
    static int gpio;
    static bool running;
    static int interval;

public:
    /**
     * @brief Initialize the led module
     * 
     * This function sets up the led with the specified configuration such as the
     * channel and frequency and attaches it to the specified gpio pin.
     * 
     * @param arg parameters
     */
    static void init(void* arg);

    /**
     * @brief Start the led module
     * 
     * This function starts the led module by creating a new task for the led.
     * 
     * @param arg parameters
     */
    static void start(void* arg);

    /**
     * @brief Stop the led module
     * 
     * This function stops the led module by setting the running flag to false.
     */
    static void stop();

    /**
     * @brief Set the led light level
     * 
     * This function sets the level of the led by taking an input integer level.
     * 
     * @param level An integer representing the desired level of the led light. range 0 - 100
     */
    static void set_status(int level);

    /**
     * @brief Set the led interval
     * 
     * This function sets the interval of the led by taking an input integer interval.
     * 
     * @param interval An integer representing the desired interval of the led light. range 0 - 100
     */    
    static void set_interval(int interval);
};