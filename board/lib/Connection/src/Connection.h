/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Connection.h
 * @authors Joschka Hemming, Philipp Macher
 * @date 11/01/2023
 * 
 * This file contains a declaration of the Connection class and all its methods used in the project. 
 * Please refer to the class for more information.
 */


#pragma once

#include <Arduino.h>
#include <WiFi.h>

class AsyncUDP;
class AsyncUDPPacket;

/**
 * @class Connection
 * 
 * @brief Class responsible for WiFi connection and communication using UDP.
 * 
 * The Connection class is responsible for managing the WiFi connection of the device and providing
 * methods for sending and receiving packets of data using the UDP protocol. The class also provides
 * methods for getting the strength of the WiFi connection and the device's local IP address.
 */
class Connection {
private:

    /**
     * Local variable for receiving data via datagram-socket.
     * Will bind to UDP_PORT.
     */
    static AsyncUDP recv_udp;

    /**
     * Local variable for sending data via datagram-socket.
     */
    static AsyncUDP send_udp;

    /**
     * @brief Handles a received UDP packet.
     * 
     * This function is used to process an incoming UDP packet received by the device. It logs the IP address and port number
     * of the sender, as well as the data of the packet. The data is passed to another function for further processing. The function
     * also flushes the UDP receive buffer to clear any remaining data.
     * 
     * @param packet The received UDP packet as an AsyncUDPPacket object
     */
    static void handle_packet(AsyncUDPPacket& packet);

    /**
     * @brief Event handler for when the device successfully connects to WiFi.
     * 
     * Logs the IP address, starts the UDP service on port UDP_PORT, and sends a login packet to the receiver.
     * 
     * @param event WiFi event
     * @param info Additional information about the event
     */
    static void got_ip_event(WiFiEvent_t event, WiFiEventInfo_t info);

public:
    /**
     * @brief Initializes the WiFi connection and starts the UDP service.
     * 
     * This function is used to establish a WiFi connection using the SSID and password specified in
     * the settings object. It also starts the UDP service on the specified port number.
     * 
     * @param arg A void pointer to any arguments needed by the function.
     */
    static void init(void* arg);

    /**
     * @brief Gets the strength of the WiFi connection.
     * 
     * This function returns the strength of the WiFi connection as a value between 0 and 100, where 0 is the weakest
     * and 100 is the strongest. If the WiFi is not connected, the function returns -1.
     * 
     * @return The strength of the WiFi connection as an integer between 0 and 100, or -1 if the WiFi is not connected.
     */
    static int8_t get_strength();
    
    /**
     * @brief Gets the local IP address of the WiFi connection. 
     * 
     * This function returns the local IP address of the WiFi connection as a string. If the WiFi is not connected, 
     * the function returns an empty string. 
     * 
     * @return The local IP address of the WiFi connection as a string, or an empty string if the WiFi is not connected.
     */
    static String get_ip();

    /**
     * @brief Send a packet to the configured receiver IP and port.
     * 
     * If the device is not connected to a WiFi network, the method will return without sending the packet.
     * The packet is sent using the AsyncUDP library.
     * 
     * @param packet The packet to be sent.
     * @param len The length of the packet.
     * @param port The port to send the packet to.
     */
    static void send_packet(char* packet, int len, uint16_t port);
};