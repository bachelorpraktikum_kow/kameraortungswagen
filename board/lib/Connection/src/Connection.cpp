/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Connection.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 11/01/2023
 * 
 * This file contains an implementation of the Connection class and all its methods used in the project.
 * Please refer to the class for more information.
 */


#include "Connection.h"
#include "Logger.h"
#include "Config.h"
#include "Packets.h"
#include <AsyncUDP.h>
#include <esp_wifi.h>

#define TAG "Wifi"


// |-- Private --|

AsyncUDP Connection::recv_udp;

AsyncUDP Connection::send_udp;

void Connection::handle_packet(AsyncUDPPacket& packet) {
    String remoteIp = packet.remoteIP().toString();
    String remotePort = String(packet.remotePort());
    Logger::info(TAG, "Received packet from " + remoteIp + ":" + remotePort);
    
    Packets::handle_packet((char*) packet.data(), (int) packet.length());
    recv_udp.flush();
}

void Connection::got_ip_event(WiFiEvent_t event, WiFiEventInfo_t info) {
    String ip_addr = WiFi.localIP().toString();
    Logger::info(TAG, "Successfully connected to wifi: " + ip_addr);

    // Start UDP service
    bool success = recv_udp.listen(UDP_PORT);
    if (success) {
        Logger::info(TAG, "Starting service on port " + String(UDP_PORT) + "...");
        recv_udp.onPacket(handle_packet);
    } else {
        Logger::warn(TAG, "Could not start UDP-service on port " + String(UDP_PORT) + "!");
    }

    // Send Login packet to receiver.
    Packets::send_health_packet();
}


// |-- Public --|

void Connection::init(void* arg) {
    Logger::info(TAG, "Setting up...");

    // Disconnect from any earlier connections
    WiFi.disconnect(true, true);

    Settings* settings = Config::get_settings();

    // Set hostname for recognition
    WiFi.hostname(settings->device_name);

    // Set wifi mode to station
    WiFi.mode(WIFI_STA);

    if (settings->wifi_ssid.isEmpty()) {
        Logger::warn(TAG, "No ssid entered! Stopping wifi...");
        return;
    }

    // Start wifi connection
    WiFi.begin(settings->wifi_ssid.c_str(), settings->wifi_password.c_str());
    Logger::info(TAG, "Connecting to wifi...");

    // Start event for handling successful connection
    WiFi.onEvent(got_ip_event, WiFiEvent_t::ARDUINO_EVENT_WIFI_STA_GOT_IP);
}

int8_t Connection::get_strength()  {
    return WiFi.isConnected() ? WiFi.RSSI() : -1;
}

String Connection::get_ip()  {
    return WiFi.isConnected() ? WiFi.localIP().toString() : "";
}

void Connection::send_packet(char* packet, int len, uint16_t port) {
    if(!WiFi.isConnected()) {
        return;
    }

    Settings* settings = Config::get_settings();

    // Parse receiver ip
    IPAddress* ipaddr = new IPAddress();
    ipaddr->fromString(settings->wifi_receiver_ip);

    // Send packet
    AsyncUDPMessage* message = new AsyncUDPMessage();
    message->write((uint8_t*) packet, len);
    size_t size = send_udp.sendTo(*message, *ipaddr, port, (tcpip_adapter_if_t) 0);

    // Check if packet was sent
    if (size != len) {
        Logger::warn(TAG, "Failed to send packet! (" + String(size) + "/" + String(len) + ")");
    }

    // Delete created objects
    delete ipaddr;
    delete message;
}
