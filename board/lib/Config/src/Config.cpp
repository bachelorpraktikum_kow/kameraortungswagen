/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Config.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 08/01/2023
 * 
 * This file contains an implementation of the Config class and all its methods used in the project. 
 * Please refer to the class for more information.
 */


#include "Config.h"
#include "Logger.h"
#include <EEPROM.h>
#include <cJSON.h>

#define TAG "Config"

// |-- Private --|

Settings* Config::settings;


// |-- Public -- |

void Config::set_settings(Settings* settings) {
    if(Config::settings != settings) {
        delete Config::settings;
    }

    Config::settings = settings;
}

Settings* Config::get_settings() {
    return Config::settings;
}


void Config::restore_defaults() {
    Settings* settings = new Settings();
    Config::set_settings(settings);
    store_to_EEPROM(serialize(settings));
}


void Config::store_to_EEPROM(const String& str) {
    // Using two bytes for string length the maximum length is defined by EEPROM_SIZE but that is limited to 65535 bytes.
    if (str.length() > EEPROM_SIZE - 2) {
        Logger::err(TAG, "Trying to write config with length over " + String(EEPROM_SIZE - 2) + " characters!");
        return;
    }

    // Split string length number into two 8 bit parts
    int length = str.length();
    uint8_t upper = length >> 8;
    uint8_t lower = length;

    // Write 16 bit number to EEPROM
    EEPROM.write(0, upper);
    EEPROM.write(1, lower);

    for (int i = 0; i < length; i++) {
        EEPROM.write(i + 2, str[i]);
    }

    // Save values
    EEPROM.commit();
}

String Config::retrieve_from_EEPROM() {
    // Read length as two 8 bit parts
    uint8_t upper = EEPROM.read(0);
    uint8_t lower = EEPROM.read(1);

    // If the value at address 0 and 1 is 255, there is no config present at this device yet.
    // -> We provide a default config and use it
    if (upper == 255 && lower == 255) {
        Logger::warn(TAG, "Cannot find config! Resetting to default values...");
        restore_defaults();
        upper = EEPROM.read(0);
        lower = EEPROM.read(1);
    }

    // Combine both parts to build length
    int length = ((int) upper << 8) + (int) lower;

    // Read EEPROM and build char array
    char json[length];
    for (int i = 0; i < length; i++) {
        json[i] = EEPROM.read(i + 2);
    }
    return String(json);
}


String Config::serialize(Settings* settings)  {
    cJSON* json = cJSON_CreateObject();

    // Device
    cJSON_AddStringToObject(json, "0", settings->device_name.c_str());

    // Sensors
    cJSON_AddNumberToObject(json, "1", settings->sensors_slowest_update_ms);

    // Wifi
    cJSON_AddStringToObject(json, "2", settings->wifi_ssid.c_str());
    cJSON_AddStringToObject(json, "3", settings->wifi_password.c_str());
    cJSON_AddStringToObject(json, "4", settings->wifi_receiver_ip.c_str());

    // Camera
    cJSON_AddBoolToObject(json, "5", settings->camera_enabled);
    cJSON_AddStringToObject(json, "6", settings->camera_quality.c_str());
    cJSON_AddNumberToObject(json, "7", settings->camera_fps);

    String str = cJSON_PrintUnformatted(json);

    // Delete cjson object
    cJSON_Delete(json);

    return str;
}

Settings* Config::deserialize(const String& json_string) {

    Settings* settings = new Settings();

    cJSON* json = cJSON_Parse(json_string.c_str());

    // Device
    settings->device_name = String(cJSON_GetObjectItem(json, "0")->valuestring);

    // Sensors
    settings->sensors_slowest_update_ms = cJSON_GetObjectItem(json, "1")->valueint;

    // Wifi
    settings->wifi_ssid = String(cJSON_GetObjectItem(json, "2")->valuestring);
    settings->wifi_password = String(cJSON_GetObjectItem(json, "3")->valuestring);
    settings->wifi_receiver_ip = String(cJSON_GetObjectItem(json, "4")->valuestring);

    // Camera
    settings->camera_enabled = cJSON_IsTrue(cJSON_GetObjectItem(json, "5"));
    settings->camera_quality = String(cJSON_GetObjectItem(json, "6")->valuestring);
    settings->camera_fps = cJSON_GetObjectItem(json, "7")->valueint;

    cJSON_Delete(json);

    return settings;
}

void Config::load_config_from_eeprom() {
    String config = Config::retrieve_from_EEPROM();
    Settings* settings = Config::deserialize(config);
    Config::set_settings(settings);
}
