/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */

/**
 * @file Config.h
 * @authors Joschka Hemming, Philipp Macher
 * @date 08/01/2023
 * 
 * This file contains a declaration of the Config class and all its methods used in the project. 
 * Please refer to the class for more information.
 */


#pragma once

#include <Arduino.h>

// Define constant settings
#define BLUETOOTH_TIMEOUT 30
#define UDP_PORT 4242
#define UDP_CAMERA_PORT 4243
#define EEPROM_SIZE 1024


/**
 * @brief A struct for storing device settings.
 * 
 * This struct is used for all settings of the device.
 * It is fistly used as an access but also used to store the settings in EEPROM via serialization.
 */
struct Settings {
    // Device
    String device_name { "Kameraortungswagen" };        // #0

    // Sensors

    int sensors_slowest_update_ms { 5000 };             // #1

    // Connection
    String wifi_ssid { "" };                            // #2
    String wifi_password { "" };                        // #3
    String wifi_receiver_ip { "" };                     // #4

    // Camera
    bool camera_enabled = { true };                     // #5
    String camera_quality = { "QQVGA" };                // #6
    int camera_fps = { 5 };                             // #7
};

/**
 * @class Config
 * 
 * @brief A class for storing and managing device settings.
 * 
 * This class provides methods for storing and retrieving device settings from EEPROM,
 * restoring default settings, serializing and deserializing settings as JSON strings,
 * and setting and getting the current settings.
 */
class Config {
private:
    /**
     * Local variable for currently used and applied settings.
     * This can and will deviate from EEPROM.
    */
    static Settings* settings;

public:
    /**
     * @brief Sets the current settings to the given element
     * 
     * This method takes the given settings parameter and sets the local
     * settings variable with it.
     * 
     * @param settings Settings to apply
     */
    static void set_settings(Settings* settings);

    /**
     * @brief Retrieves settings from local variable 
     * 
     * @return Settings* address of generated settings struct
     */
    static Settings* get_settings();

    /**
     * @brief Restores the default settings for the board.
     * 
     * This method creates a new settings-instance which contains
     * default value, stores it to EEPROM and sets it as the 
     * current settings.
     */
    static void restore_defaults();

    /**
     * This method stores the given string into the local EEPROM. This saves it for use even after
     * resetting the board.
     * 
     * @brief stores a given string to EEPROM
     * 
     * @param str the string to save
    */
    static void store_to_EEPROM(const String& str);

    /**
     * @brief Retrieves a string from EEPROM.
     * 
     * This method determines the length of the string on the first two bytes of EEPROM.
     * Then reads the string from EEPROM using this length. If the flash is not written,
     * the method writes the default config to EEPROM before reading.
     *
     * @return The string retrieved from EEPROM.
     */
    static String retrieve_from_EEPROM();


    /**
     * @brief Serializes a Settings object into a JSON string.
     * 
     * This method serializes the given Settings object by creating a new JSON object.
     * Each field is assigned a number with which it will be saved. The numbers are need
     * to be the same when reading.
     * 
     * @param settings Settings object to be serialized.
     * @return The serialized JSON string.
     */
    static String serialize(Settings* settings);

    /**
     * @brief Deserializes a JSON string into a Settings object.
     * 
     * This method takes the given string, parses it as a json using cJSON. Each field
     * is accessed by the number (see serialize function) to extract the value. The value
     * is saved into a settings object. Finally, it returns a pointer to the created
     * Settings object.
     * 
     * @param json_string The JSON string to be deserialized.
     * @return Pointer to the deserialized Settings object.
     */
    static Settings* deserialize(const String& str);


    /**
     * @brief Load configuration from the EEPROM memory
     * 
     * This function retrieves the configuration data that is stored in the EEPROM memory,
     * deserializes it into a Settings object, and sets it as the current configuration.
     * 
     * @note This function should be called at system startup to ensure the system is configured correctly.
     */
    static void load_config_from_eeprom();
};