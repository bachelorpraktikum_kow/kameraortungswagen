/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Logger.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 08/01/2023
 * 
 * This file contains an implementation of the Logger class and all its methods used in the project. 
 * Please refer to the class for more information.
 */


#include "Logger.h"
#include "Config.h"
#include "BTSerial.h"
#include "DebugLed.h"

void Logger::info(const String& tag, const String& message) {
    String* log = new String();
    *log += "[";
    *log += tag;
    *log += "/INFO] ";
    *log += message;

    Serial.println(*log);
    if (BTSerial::connected()) {
        BTSerial::send(log);
    }
    delete log;
}

void Logger::warn(const String& tag, const String& message) {
    String* log = new String();
    *log += "[";
    *log += tag;
    *log += "/WARN] ";
    *log += message;

    Serial.println(*log);
    if (BTSerial::connected()) {
        BTSerial::send(log);
    }
    delete log;
}

void Logger::err(const String& tag, const String& message) {
    String* log = new String();
    *log += "------------------------------\n";
    *log += "[";
    *log += tag;
    *log += "/ERROR] ";
    *log += message;
    *log += "\n";
    *log += "------------------------------";

    DebugLed::set_interval(100);

    Serial.println(*log);
    if (BTSerial::connected()) {
        BTSerial::send(log);
    }
    delete log;
}
