/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Logger.h
 * @authors Joschka Hemming, Philipp Macher
 * @date 08/01/2023
 * 
 * This file contains a declaration of the Logger class and all its methods used in the project. 
 * Please refer to the class for more information.
 */


#pragma once

#include <Arduino.h>

/**
 * @class Logger
 * 
 * @brief A utility class for logging messages.
 * 
 * The Logger class provides functions for logging messages to the serial output and (optionally) over Bluetooth.
 * The functions are provided for logging info, warning, and error messages, each with a different log level. The
 * log level is combined in the log message with a tag, which is a short string identifying the source of the log
 * message.
 */
class Logger {
private:

public:
    /**
     * @brief Logs an info message. 
     * 
     * This function logs an info message with a specified tag. The message is logged to the serial output 
     * and (optionally) over Bluetooth if it is enabled in the settings. 
     *  
     * @param tag The tag to be associated with the message. 
     * @param message The message to be logged.
     */
    static void info(const String& tag, const String& message);

    /**
     * @brief Logs a warning message. 
     * 
     * This function logs a warning message with a specified tag. The message is logged to the serial output
     * and (optionally) over Bluetooth if it is enabled in the settings.
     * 
     * @param tag The tag to be associated with the message.
     * @param message The message to be logged.
     */
    static void warn(const String& tag, const String& message);

    /**
     * @brief Logs an error message.
     * 
     * This function logs an error message with a specified tag. The message is logged to the serial output
     * and (optionally) over Bluetooth if it is enabled in the settings. The error message is also surrounded
     * by a line of dashes to make it more noticeable.
     * 
     * @param tag The tag to be associated with the message.
     * @param message The message to be logged.
     */
    static void err(const String& tag, const String& message);
};