/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Packets.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 08/01/2023
 * 
 * This file contains an implementaion of the Packets class and all its methods used in the project. 
 * Please refer to the class for more information.
 */

#include "Packets.h"
#include "xxhash.h"
#include "Config.h"
#include "Camera.h"
#include "Connection.h"
#include "Logger.h"
#include "Sensors.h"
#include "Flashlight.h"
#include <cJSON.h>
#include <time.h>

#define TAG "Packets"


unsigned long long Packets::reset_time = 0;
unsigned long long Packets::server_time = 0;

char* Packets::get_packettype(char* json) {
    // Parse the JSON string
    cJSON* root = cJSON_Parse(json);

    // Get the value of the "type" field as a string
    cJSON* type = cJSON_GetObjectItem(root, "type");

    cJSON_Delete(root);
    return type->valuestring;
}

char* Packets::prepare_data_packet(readings_t readings) {
    Settings* settings = Config::get_settings();

    // Calculate the time since the last reset
    unsigned long long time = readings.time.tv_sec * 1000 + readings.time.tv_usec / 1000;
    time += server_time;
    time -= reset_time;

    cJSON* sensorsJson = cJSON_CreateObject();
    cJSON_AddNumberToObject(sensorsJson, "0", readings.steering_angle); // Steering
    cJSON_AddNumberToObject(sensorsJson, "1", readings.hall_sensor); // Hall
    cJSON_AddNumberToObject(sensorsJson, "2", readings.tie_sensor_speed); // Tie
    cJSON_AddNumberToObject(sensorsJson, "3", readings.tie_sensor_count); // Tie-Count
    cJSON_AddNumberToObject(sensorsJson, "4", readings.odometer_speed); // Odometer
    cJSON_AddNumberToObject(sensorsJson, "5", readings.odometer_count); // Odometer-Count
    cJSON_AddNumberToObject(sensorsJson, "6", readings.wifi_strength); // Wifi-Strength
    cJSON_AddNumberToObject(sensorsJson, "7", time); // Rtc-time
    cJSON_AddNumberToObject(sensorsJson, "8", readings.battery_voltage); // Battery


    cJSON* json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "0", Connection::get_ip().c_str());
    cJSON_AddStringToObject(json, "1", settings->device_name.c_str());
    cJSON_AddStringToObject(json, "2", "data");
    cJSON_AddItemToObject(json, "3", sensorsJson);


    char* json_msg = cJSON_PrintUnformatted(json);
    int json_length = strlen(json_msg);

    uint32_t xxhash = XXHASH32(json_msg, json_length);
    char hash_msg[9];
    sprintf(hash_msg, "%08x", xxhash);
    int hash_length = strlen(hash_msg);

    char* packet = new char[hash_length + 1 + json_length + 1];
    sprintf(packet, "%s:%s", hash_msg, json_msg);

    delete[] json_msg;
    cJSON_Delete(json);

    return packet;
}

char* Packets::prepare_health_packet() {
    Settings* settings = Config::get_settings();

    cJSON* wifiJson = cJSON_CreateObject();
    cJSON_AddStringToObject(wifiJson, "0", settings->wifi_ssid.c_str());

    cJSON* configJson = cJSON_CreateObject();
    cJSON_AddNumberToObject(configJson, "0", settings->sensors_slowest_update_ms);
    cJSON_AddNumberToObject(configJson, "1", Flashlight::get_status());
    cJSON_AddNumberToObject(configJson, "2", settings->camera_enabled);
    cJSON_AddStringToObject(configJson, "3", settings->camera_quality.c_str());
    cJSON_AddNumberToObject(configJson, "4", settings->camera_fps);

    cJSON* json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "0", Connection::get_ip().c_str());
    cJSON_AddStringToObject(json, "1", settings->device_name.c_str());
    cJSON_AddStringToObject(json, "2", "health");
    cJSON_AddItemToObject(json, "3", wifiJson);
    cJSON_AddItemToObject(json, "4", configJson);


    char* json_msg = cJSON_PrintUnformatted(json);
    int json_length = strlen(json_msg);

    uint32_t xxhash = XXHASH32(json_msg, json_length);
    char hash_msg[9];
    sprintf(hash_msg, "%08x", xxhash);
    int hash_length = strlen(hash_msg);

    char* packet = new char[hash_length + 1 + json_length + 1];
    sprintf(packet, "%s:%s", hash_msg, json_msg);

    delete[] json_msg;
    cJSON_Delete(json);

    return packet;
}

void Packets::parse_configuration_packet(char* hash, int hash_length, char* json, int json_length) {
    
    Settings* settings = Config::get_settings();

    // Check integrity of the packet
    uint32_t xxhash = XXHASH32(json, json_length-1);
    char hash_msg[9];
    sprintf(hash_msg, "%08x", xxhash);

    if (strncmp(hash, hash_msg, hash_length) != 0) {
        Logger::info(TAG, "Packet integrity check failed");
        return;
    }

    // Parse the JSON string
    cJSON* root = cJSON_Parse(json);

    // Get the value of the "config" field as an object
    cJSON* config = cJSON_GetObjectItem(root, "config");

    if (cJSON_HasObjectItem(config, "readInterval")) {
        cJSON* readInterval = cJSON_GetObjectItem(config, "readInterval");

        // Check for valid input values
        if (readInterval->valueint == 0) {
            settings->sensors_slowest_update_ms = 1;
        } else {
            settings->sensors_slowest_update_ms = readInterval->valueint;
        }
    }

    if (cJSON_HasObjectItem(config, "flashlight")) {
        cJSON* flashlight = cJSON_GetObjectItem(config, "flashlight");
        Logger::info(TAG, "Received flashlight status: " + String(flashlight->valueint));
        Flashlight::set_status(flashlight->valueint);
    }

    if (cJSON_HasObjectItem(config, "camera")) {
        cJSON* camera = cJSON_GetObjectItem(config, "camera");
        Logger::info(TAG, "Received camera status: " + String(camera->valueint));
        bool previous_state = settings->camera_enabled;
        settings->camera_enabled = camera->valueint;

        // Check if the camera should be started
        bool turnOn = !previous_state && settings->camera_enabled;
        bool faultyState = !Camera::is_running() && settings->camera_enabled;

        if (turnOn || faultyState) {
            // Check if camera is initialized -> Not means bluetooth is still running!
            if (Camera::is_initialized()) {
                // Check is camera is not already running
                if (!Camera::is_running()) {
                    Logger::info(TAG, "Starting camera...");
                    xTaskCreate(Camera::start, "Camera", 4096, NULL, tskIDLE_PRIORITY, NULL);
                } else {
                    Logger::info(TAG, "Camera is already running, skipping start...");
                }
            } else {
                Logger::info(TAG, "Camera is not initialized yet, skipping start...");
            }
        }
    }

    if (cJSON_HasObjectItem(config, "cameraQuality")) {
        cJSON* camera_quality = cJSON_GetObjectItem(config, "cameraQuality");
        if (strcmp(settings->camera_quality.c_str(), camera_quality->valuestring) != 0) {
            settings->camera_quality = camera_quality->valuestring;
            Logger::info(TAG, "Received camera quality: " + settings->camera_quality);
            Camera::update_framesize();
        }
    }

    if (cJSON_HasObjectItem(config, "cameraFps")) {
        cJSON* camera_fps = cJSON_GetObjectItem(config, "cameraFps");
        Logger::info(TAG, "Received camera fps: " + String(camera_fps->valueint));
        settings->camera_fps = camera_fps->valueint;
    }

    if (cJSON_HasObjectItem(config, "round")) {
        cJSON* round = cJSON_GetObjectItem(config, "round");
        Logger::info(TAG, "Received force round: " + String(round->valueint));
        if (round->valueint == 1) {
            Sensors::force_round();
        }
    }

    if (cJSON_HasObjectItem(config, "time")) {
        cJSON* time = cJSON_GetObjectItem(config, "time");
        String time_string = time->valuestring;

        Logger::info(TAG, "Received time: " + time_string);

        for(int i = 0; i < time_string.length(); i++ ) {
            server_time += (time_string[i] - 48) * pow(10, time_string.length() - i - 1);
        }
        timeval timev;
        gettimeofday(&timev, NULL);
        reset_time = timev.tv_sec * 1000 + timev.tv_usec / 1000;
    }

    // Save settings
    String serialized_settings = Config::serialize(settings);
    Config::store_to_EEPROM(serialized_settings);

    // Send health packet as answer
    Packets::send_health_packet();

    // Check for restart
    if (cJSON_HasObjectItem(config, "restart")) {
        cJSON* restart = cJSON_GetObjectItem(config, "restart");
        if(restart->valueint == 1) {
            ESP.restart();
        }
    }

    // Delete the root object
    cJSON_Delete(root);
}

void Packets::send_health_packet() {
    char* packet = prepare_health_packet();
    Connection::send_packet(packet, strlen(packet), UDP_PORT);
    delete[] packet;
}

void Packets::send_data_packet(readings_t readings) {
    char* packet = prepare_data_packet(readings);
    Connection::send_packet(packet, strlen(packet), UDP_PORT);
    delete[] packet;
}

void Packets::send_camera_packet(char* packet, int len) {
    Connection::send_packet(packet, len, UDP_CAMERA_PORT);
}

void Packets::handle_packet(char* packet, int length) {

    // Extract JSON object
    char* c = strchr(packet, ':');
    int index = c != NULL ? c - packet : -1;
    if (index == -1) {
        Logger::warn(TAG, "Cannot parse received packet.");
        return;
    }

    // separate json
    int json_length = length - index;
    char json[json_length];
    snprintf(json, json_length, packet + index + 1);

    // Separate prepend for integrity or authentification checks
    char prepend[index + 2];
    snprintf(prepend, index + 1, packet);

    char* type = get_packettype(json);

    if (strcmp(type, "configuration") == 0) {
        Logger::info(TAG, "Handling configuration packet...");
        // Parse content of packet as configration packet
        Packets::parse_configuration_packet(prepend, index + 2, json, json_length);
    }
}