/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Flashlight.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 12/01/2023
 * 
 * This file contains an implementation of the Flashlight class and all its methods used in the project. 
 * Please refer to the class for more information.
 */


#include "Flashlight.h"
#include "Logger.h"
#include <Arduino.h>

#define TAG "Flashlight"

// |-- Private --|

int Flashlight::gpio = 4;


// |-- Public --|

/**
 * @brief Initialize the flash light module
 * 
 * This function sets up the flash light module with the specified configuration such as the
 * channel and frequency and attaches it to the specified gpio pin.
 * 
 * @param arg pointer to the gpio pin number
 */
void Flashlight::init(void* arg) {
    Logger::info(TAG, "Setting up...");
    ledcSetup(1, 60, 8);
    ledcAttachPin(gpio, 1);
    Logger::info(TAG, "Finished setting up.");
}

void Flashlight::set_status(int level) {
    ledcWrite(1, (256 / 10) * level);
}

int Flashlight::get_status() {
    return round((float) ledcRead(1) * 10 / 256);
}