/**
 * This file is part of the project "Kameraortungswagen" licensed under GPLv3.
 * It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
 * Patrick Reidelbach and Daniel Simon.
 * On behalf of and supervised by the
 * Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
 */


/**
 * @file Flashlight.h
 * @authors Joschka Hemming, Philipp Macher
 * @date 12/01/2023
 * 
 * This file contains a declaration of the Flashlight class and all its methods used in the project. 
 * Please refer to the class for more information.
 */

#pragma once


/**
 * @class Flashlight
 * 
 * @brief The Flashlight class provides a set of functions for interacting with a flash light module.
 * 
 * This class allows for the initialization, configuration, and control of a flash light module.
 * It provides functions for setting the level of flash light.
 */ 
class Flashlight {
private:
    static int gpio;

public:
    /**
     * @brief Initialize the flash light module
     * 
     * This function sets up the flash light module with the specified configuration such as the
     * channel and frequency and attaches it to the specified gpio pin.
     * 
     * @param arg pointer to the gpio pin number
     */
    static void init(void* arg);

    /**
     * @brief Set the flash light level
     * 
     * This function sets the level of the flash light by taking an input integer level.
     * 
     * @param level An integer representing the desired level of the flash light. range 0-100
     */
    static void set_status(int level);

    /**
     * @brief Get the flash light level
     * 
     * This function returns the current level of the flash light.
     * 
     * @return An integer representing the current level of the flash light. range 0-10
     */
    static int get_status();
};