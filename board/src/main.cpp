/**
* This file is part of the project "Kameraortungswagen" licensed under GPLv3.
* It is developed by Joschka Hemming, Philipp Macher, Lucas Marschall,
Patrick Reidelbach and Daniel Simon.
* On behalf of and supervised by the
Institut für Bahnsysteme und Bahntechnik - Technische Universität Darmstadt
*/

/**
 * @file main.cpp
 * @authors Joschka Hemming, Philipp Macher
 * @date 08/01/2023
 * 
 * This file contains the entry point to the project.
 */


#include "Config.h"
#include "Logger.h"
#include "Connection.h"
#include "BTSerial.h"
#include "Sensors.h"
#include "Camera.h"
#include "Flashlight.h"
#include "Packets.h"
#include "DebugLed.h"
#include <Arduino.h>
#include <EEPROM.h>

#define TAG "Setup"

// 2048B stack size worked, while 1536B did not.
#define SENSORS_STACK_SIZE 4096

/**
 * @brief The setup function is called once when the Arduino sketch starts.
 * 
 * This function is used to initialize variables, pin modes, and other setup tasks.
 * It is called once before the loop function begins.
 */
void setup() {
    // Setup serial
    Serial.begin(115200);

    Logger::info(TAG, "Setting up...");

    // | --- Inits --- |

    // Initalize DebugLed
    DebugLed::init(NULL);
    xTaskCreate(DebugLed::start, "DebugLed", 2048, NULL, 10, NULL);

    // Initalize flashlight
    Flashlight::init(NULL);

    // Setup config
    Logger::info(TAG, "Initially loading config...");

    // Setup EEPROM
    EEPROM.begin(EEPROM_SIZE);

    // Load config from EEPROM
    Config::load_config_from_eeprom();
    Logger::info(TAG, "Device name: " + Config::get_settings()->device_name);

    // Initialize Connection
    Connection::init(NULL);


    // | --- Tasks --- |

    Logger::info(TAG, "Creating tasks...");

    // Start Sensor task
    xTaskCreate(Sensors::start, "Sensors", SENSORS_STACK_SIZE, NULL, 9, NULL);

    // Start Bluetooth Serial
    BTSerial::start();

    // Start Camera after bluetooth has finished
    Camera::start(NULL);
}

/**
 * @brief The loop function is called repeatedly in an infinite loop. 
 * 
 * This function is used to perform tasks that need to be repeated, such as reading sensors, 
 * controlling actuators, and communicating with other devices. It is called repeatedly 
 * after the setup function has completed.
 */
void loop() {
    // As the loop task is not used we can delete it.
    vTaskDelete(NULL);
}
