#include <Arduino.h>
#include <math.h>

const int pot_pin = 14;
const int odo_pin = 12;
const int tie_pin = 13;

const double wheel_diameter = 10.2;
const int holes = 12;
const double tie_length = 5;

// n holes -> 2n edges
const double mm_per_odometer_edge = (M_PI * wheel_diameter) / (holes * 2);

unsigned long last_odometer = 0UL;
unsigned long last_tie = 0UL;

bool odometer_state = 0;
bool tie_state = 0;

void setup() {
  pinMode(odo_pin, OUTPUT);
  pinMode(tie_pin, OUTPUT);
}

void loop() {
  // Input Range is 0 - 4095, output range is 0 - 170
  int speed = analogRead(pot_pin) * 170 / 4095;

  int micros_per_odometer_edge = mm_per_odometer_edge / speed * 1000000;
  int micros_per_tie_edge = tie_length / speed * 1000000;

  // Detect timer overflow, will happen around every 70 minutes
  // That's not 100% accurate, but I think a few micros of error might be okay every 70 minutes
  if (micros() < last_odometer || micros() < last_tie) {
    last_odometer = 0;
    last_tie = 0;
  }

  // This way of distance measuring is not highly accurate, but it should be close enough.
  // In theory, it is possible to trick the system trough VERY quick accelerating or decelerating.
  // For better values, we would have to integrate speed over time to get distance, but this would be way more complicated
  // And maybe even slower and though not that much more accurate
  if (micros() - last_odometer >= micros_per_odometer_edge) {
    last_odometer = micros();
    odometer_state = !odometer_state;
    digitalWrite(odo_pin, odometer_state);
  }

  if (micros() - last_tie >= micros_per_tie_edge) {
    last_tie = micros();
    tie_state = !tie_state;
    digitalWrite(tie_pin, tie_state);
  }
}