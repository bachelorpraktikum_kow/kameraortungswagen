# Communication Protocol v.1.2
> Kameraortungswagen, ESP-32 <-> Receiver/Web-Server

This file layouts the communication protocol between the ESP-32 and the receiver/web-server for the `Kameraortungswagen`.

## Overview
![](KOW_Protocol.png)

## Communication
The ESP board communicates with the receiver using `UDP` on port `4242` for json packets and `4243` for the images.

1. On startup or change of the configuration of the receiver, the ESP sends a [Health Packet](#health-packet-e2r) to the (now) configured receiver.
2. On startup the esp sends a [Data Packet](#data-packet-e2r) with the hall sensor set to `1`.
3. [Data Packets](#data-packet-e2r) are send by the ESP-32 to the server periodically.
4. If the data packet has a timestamp that is way behind real time, the receiver will send a [Configuration Packet](#configuration-packet-r2e) only containing the `time` key to the ESP-32.
5. The receiver/web-server can send [Configuration Packets](#configuration-packet-r2ea) to change the configuration of the ESP-32. The ESP-32 responds with a [Health Packet](#health-packet-e2r). 

### Integrity
Since we are using UDP, a fast and small checksum is calculated for each packet.

The protocol uses [xxhash](https://github.com/Cyan4973/xxHash) for these integrity checksums, since they are the fasted to calculate.
The checksum is calculated using xxhash32 as a hexdigest. The hash is prepended to the json with the seperator `:` to avoid json structure errors.

#### Example
```
xxhash:{"json":"", ...}
```

## Packets
Packets are encoded as json. They include the ESPs `name` and its `ip`. The hash for integrity checks is prepended to these packets.

Only the *Image Packet* is not encoded in json, it is just encoded as a `jpeg` image. The image is sent over a different port to not hinder any text packets.

| Short | Name            | Description                                |
| :---: | --------------- | ------------------------------------------ |
|  E2R  | ESP to Receiver | Communication from the ESP to the receiver |
|  R2E  | Receiver to ESP | Communication from the receiver to the ESP |

### Health Packet [E2R]
#### Packet
```json
xxhash:{
    "ip": "<ip>",
    "name": "<esp-name>",
    "type": "health",
    "wifi": {
        "ssid": "<ssid>"
    },
    "config": {
        "readInterval": <read-interval>,
        "flashlight": <flashlight-brightness:0-10>,
        "camera": <camera:off/on>,
        "cameraQuality": <quality:[UXGA, SXGA, XGA, SVGA, VGA, CIF, QQVGA]>,
        "cameraFps": <fps:[1,5,10]>
    }
}
```

#### Compressed
```json
xxhash:{
    "0": "<ip>",
    "1": "<esp-name>",
    "2": "health",
    "3": {
        "0": "<ssid>"
    },
    "4": {
        "0": <read-interval>,
        "1": <flashlight-brightness:0-10>,
        "2": <camera:off/on>,
        "3": <quality:[UXGA, SXGA, XGA, SVGA, VGA, CIF, QQVGA]>,
        "4": <fps:[1,5,10]>
    }
}
```

#### Description
| Key             | Values                                          | Description                           |
| --------------- | ----------------------------------------------- | ------------------------------------- |
| `ssid`          | string                                          | SSID of currently connected WiFi      |
| `readInterval`  | int                                             | Sensor read interval in milli-seconds |
| `flashlight`    | int:[0-10]                                      | Flashlight brightness (`0` being off) |
| `camera`        | int:[0/1]                                       | Camera on/off toggle (`0` being off)  |
| `cameraQuality` | string:[UXGA, SXGA, XGA, SVGA, VGA, CIF, QQVGA] | Quality setting for camera            |
| `cameraFps`     | int:[1,5,10]                                    | Maximum FPS for camera                |

### Data Packet [E2R]
```json
xxhash:{
    "ip": "<ip>",
    "name": "<esp-name>",
    "type": "data",
    "sensors": {
        "steering": <steering-value>,
        "hall": <hall-value>,
        "tie": <tie-value>,
        "tieCount": <tie-count-value>,
        "odometer": <odometer-value>,
        "odometerCount": <odometer-count-value>,
        "wifi": <wifi-strength>,
        "rtc": <rtc-value>,
        "battery": <battery-value>
    }
}
```

#### Compressed
```json
xxhash:{
    "0": "<ip>",
    "1": "<esp-name>",
    "2": "data",
    "3": {
        "0": <steering-value>,
        "1": <hall-value>,
        "2": <tie-value>,
        "3": <tie-count-value>,
        "4": <odometer-value>,
        "5": <odometer-count-value>,
        "6": <wifi-strength>,
        "7": <rtc-value>,
        "8": <battery-value>
    }
}
```

#### Description
| Key               | Values        | Description                                                                                                 |
| ----------------- | ------------- | ----------------------------------------------------------------------------------------------------------- |
| `steering`        | int           | Current steering angle in mV                                                                                |
| `hall`            | int:[0/1]     | Current read of hall sensor *(if landmark/fixpoint is passed or esp is started, set to `1` for one packet)* |
| `tie`             | float         | Current speed of tie-sensor in mm/s                                                                         |
| `tieCounter`      | int           | Tie count since last round *(last fixpoint)*                                                                |
| `odometer`        | float         | Current speed of odometer-sensor in mm/s                                                                    |
| `odometerCounter` | int           | Odometer count since last round *(last fixpoint)*                                                           |
| `wifi`            | int:[0- -100] | Current WiFi Strength                                                                                       |
| `rtc`             | long          | Current board time in milli-seconds *(real time if synced or time since start if not)*                      |
| `battery`         | int           | Current Battery level in mV                                                                                 |


### Configuration Packet [R2E]
#### Packet
```json
xxhash:{
    "type": "configuration",
    "config": {
        "readInterval": <read-interval:int>,
        "flashlight": <flashlight-brightness:0-10>,
        "camera": <camera:0/1>,
        "cameraQuality": <quality:[UXGA, SXGA, XGA, SVGA, VGA, CIF, QQVGA]>,
        "cameraFps": <fps:[1,5,10]>,
        "round": <round:0/1>,
        "restart": <restart:0/1>,
        "time": <time:long>
    }
}
```

#### Description
The keys in the configuration packet are optional. If a key is not present, the value will not be changed. This is especially useful for the `time` key, which can be used to sync the ESP's RTC with the receiver's RTC (receiver only sends time in configuration packet)

| Key             | Values                                          | Description                           |
| --------------- | ----------------------------------------------- | ------------------------------------- |
| `readInterval`  | int                                             | Sensor read interval in milli-seconds |
| `flashlight`    | int:[0-10]                                      | Flashlight brightness (`0` being off) |
| `camera`        | int:[0/1]                                       | Camera on/off toggle (`0` being off)  |
| `cameraQuality` | string:[UXGA, SXGA, XGA, SVGA, VGA, CIF, QQVGA] | Quality setting for camera            |
| `cameraFps`     | int:[1,5,10]                                    | Maximum FPS for camera                |
| `round`         | int:[0,1]                                       | Start new round if set to `1`         |
| `restart`       | int:[0,1]                                       | Restart ESP if set to `1 `            |
| `time`          | long                                            | Time in milliseconds                  |

### Image Packet [E2R]
Raw-Image Data to port 4243